# ZSH:
- перенести все функции из zshrc в fpath/bin/*
- остальные конфиги в zshrc.d

- https://wiki.archlinux.org/title/Zsh#Colors
   и остальное, например esc-последовательность для установки заголовка таба

- сделать truecolor-цвета в теме и в LS_COLORS, чтобы работало в konsole-direct и tmux-direct
- подумать про плагины


# TMUX:
- пересортировать опции
- подумать про плагины
- truecolor цветовая схема

# NVIM:
- truecolor цветовая схема
- получше отсортировать конфиги определений плагинов для lazy и конфиги самих плагинов


# *-direct:
https://github.com/wez/wezterm/issues/4528
https://github.com/tmux/tmux/issues/2370

# truecolor:
https://github.com/termstandard/colors/blob/master/README.md
https://github.com/SkyN9ne/dircolors-gen
