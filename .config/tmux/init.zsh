#!/bin/zsh -f

trap exit HUP TERM KILL INT

mypid=${$}
oldpid=$(timeout 0.1 tmux showenv -g TMUX_INIT_DAEMON_PID 2>/dev/null | cut -d= -f2)
# oldpid=${oldpid:-${mypid}}

zmodload zsh/zselect

jobs -Z "Tmux RPS updater"
# set process full name ^
printf "Tmux.Worker" >! /proc/${mypid}/comm
# set process shortname ^

if [[ "${oldpid}" != "${mypid}" ]] {
	if [[ -n "${oldpid}" ]] {
		kill -HUP "${oldpid}";
	}
	tmux setenv -g TMUX_INIT_DAEMON_PID ${mypid}
}

# set_status() definition {{{
set_status() {
	# vars {{{
	local ret;
	local cl="\xe2\x8c\x9a";
	local ut="\xe2\x87\x91";
	local rb="\xee\x82\xb0";
	local rt="\xee\x82\xb1";
	local lb="\xee\x82\xb2";
	local lt="\xee\x82\xb3";
	local nil="\0";
	# }}}

# nproc {{{
	local function nproc() {
		(command nproc || grep -c processor /proc/cpuinfo || print 1) 2>/dev/null
	}
# }}}

	# color_avg() {{{
	local function color_avg() {
		local proc="$(nproc)"
		local crit="${proc}";
		local flt="0.0"; # kludge to not break vim syntax hl
		local warn="$(((proc+flt)/2))";
		local base="#[fg=_PH_]";
		local la;
		local out=();
		out+=("#[fg=colour241]${lt}");
		for la in ${@}; do
			if [[ ${la} -ge ${crit} ]]; then
				out+=("${base/_PH_/colour196,blink}${la}#[noblink]");
			elif [[ ${la} -ge ${warn} ]]; then
				out+=("${base/_PH_/colour226,underscore}${la}#[nounderscore]");
			else
				out+=("${base/_PH_/colour2}${la}");
			fi;
		done;
		print "${out[@]}";
	};
	# }}}

	# filling s {{{
    # upt="\xe2\x9c\x88" # ✈
    upt="\xf0\x9f\xa1\x85" # 🡅
	local s=("#[fg=colour233] #[bg=colour233]#[fg=colour31] ${upt} #[fg=colour247]")
	s+=("$((${(f)"$(</proc/uptime)"%%.*}/86400))")
	s+=($(color_avg ${(ps: :)$(</proc/loadavg)[1,3]}))
	# }}}

	ret=$(tmux display -p "$(print ${s})")

	tmux setenv -g TMUX_RPS "${ret}"
}
# }}}

timeout=$(((${timeout:-$(tmux show -g -v -q status-interval)}-0.8)*100))

for t (~/.config/tmux/inc/tmux*.conf(N)) {
	# TODO: version detection magic?
	#		terminal colors detection magic?
	tmux source "${t}";
}

while [[ -n $(tmux ls) ]] {
	( [[ -d "/proc/${PPID}" ]] || [[ "${${(%):-/proc/${PPID}/exe}:A:t}" =~ "tmux*" ]] ) || exit 0

	# Count of additional sessions.
	# This is made for not patching tmux sources
	# to distinguish if it is single session on the server, or not
	# (to show or hide session name in the window title)
	tmux setenv -g TMUX_ADDITIONAL_SESSIONS "$((${(wf)#$(tmux list-sessions 2>/dev/null)}-1))"

	# Uptime + LoadAvg
	set_status

	# Custom subshell "sleep" implementation
#	: $(jobs -Z "Infinite loop sleeper (restarts every ${timeout:-2}s)";printf "Tmux.Inf.Loop.Sleeper" >! /proc/${$}/comm;read -u 1 -s -t ${timeout:-2}; true)
	# ZSH's internal (well, in-module) replacement
	zselect -t ${${timeout:-200}%.};true
}
