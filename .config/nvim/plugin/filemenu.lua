-- luacheck: globals vim

-- local f = require"lib.funcs"
local o = vim.o

o.wildmenu = true
vim.cmd[[set wcm=<Tab>]]

-- <Меню изменения кодировки чтения из файла (F8)> {{{
vim.cmd[[
  menu Encoding.Read.CP1251   :e ++enc=cp1251<CR>
  menu Encoding.Read.CP866    :e ++enc=cp866<CR>
  menu Encoding.Read.KOI8-R   :e ++enc=koi8-r<CR>
  menu Encoding.Read.UTF-8    :e ++enc=utf-8<CR>
  map <F8> :emenu Encoding.Read.<TAB>
]]
-- }}}
-- <Меню изменения кодировки записи в файл (Ctrl-F8)> {{{
vim.cmd[[
  menu Encoding.Write.CP1251    :set fenc=cp1251<CR>
  menu Encoding.Write.CP866     :set fenc=cp866<CR>
  menu Encoding.Write.KOI8-R    :set fenc=koi8-r<CR>
  menu Encoding.Write.UTF-8     :set fenc=utf-8<CR>
  map <C-S-F8> :emenu Encoding.Write.<TAB>
]]
-- }}}
-- <Меню изменения конца строки у файла (Shift-F8)> {{{
vim.cmd[[
  menu File.Format.DOS    :set fileformat=dos<CR>
  menu File.Format.MAC    :set fileformat=mac<CR>
  menu File.Format.UNIX   :set fileformat=unix<CR>
  map <S-F8> :emenu File.Format.<TAB>
]]
-- }}}

