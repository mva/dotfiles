-- luacheck: globals vim req

local f = require"lib.funcs"
-- local has = vim.fn.has
local exists = vim.fn.exists
-- local empty = vim.fn.empty
local au = f.au
local o = vim.o
local g = vim.g
-- local b = vim.b
local bo = vim.bo
local wo = vim.wo
-- local env = vim.env

if not (g.checkpager_loaded) then
  g.checkpager_loaded = 1

  local function check_pager_mode() -- {{{
    --		if exists(":Man") == 2 {{{
    --			"set statusline=%<MAN:\ %f%h%m%r%=format=%{&fileformat}\ file=%{&fileencoding}\ enc=%{&encoding}\ %b\ 0x%B\ %l,%v\ %P
    --			"set statusline=MAN:\ %t\ %l,%v\ %P
    --		endif -- }}}

    if bo.filetype == "man" or (g.loaded_less and g.loaded_less == 1) then -- {{{
      -- we're in vimpager / less.sh / man mode
      --			set whichwrap=b,s,<,>,[,] " Перемещать курсор на следующую строку при нажатии на клавиши вправо-влево и пр.
      --			set laststatus=3
      -- set ruler
      -- {{{ bo
      wo.foldmethod = 'manual' -- не сворачивать
      wo.foldlevel = 99
      wo.list = false     -- не подсвечивать пробелы
      bo.swapfile = false
      wo.cursorline = false -- не подсвечивать текущую строку
      wo.number = false   -- не нумервать строки
      wo.scrolloff = 1
      o.scrolljump = 0
      wo.colorcolumn = ""
      wo.wrap = true
      o.modifiable = true
      bo.modifiable = false
      bo.readonly = true
      --if has('autocmd')
      --	au VimEnter * augroup nomodif
      --	au VimEnter * au BufReadPost,BufNewFile * let &l:ma = 0 | let &l:ro = 1
      --	au VimEnter * augroup END
      --endif
      -- }}}

      -- Disable unuseable keys
      local buf = vim.api.nvim_get_current_buf()
      local function bufmap(l, r)
        vim.api.nvim_buf_set_keymap(buf, "", l, r, {})
      end

      bufmap("<Insert>", "<Esc>")
      bufmap("<Delete>", "<Esc>")
      bufmap("=", "<Esc>")
      bufmap("i", "<Esc>")
      bufmap("o", "<Esc>")
      bufmap("a", "<Esc>")
      bufmap("s", "<Esc>")
      bufmap("x", "<Esc>")
      bufmap("c", "<Esc>")
      bufmap("r", "<Esc>")
      bufmap("<S-R>", "<Esc>")
      bufmap("v", "<Esc>")
      bufmap("<S-V>", "<Esc>")
      bufmap("<C-V>", "<Esc>")
      bufmap("d", "<Esc>")
      bufmap("u", "<Esc>")
      bufmap("p", "<Esc>")
      bufmap("z", "<Esc>")
      bufmap("<S-f>", "<Esc>")

      -- {{{ commented block
      --			if &wrap
      --				noremap <buffer> <SID>L L0:redraw<CR>:echo<CR>
      --				normal! L0
      --			else
      --				noremap <buffer> <SID>L Lg0:redraw<CR>:echo<CR>
      --				normal! Lg0
      --			endif
      -- }}}

      --[=[{{{
			if (g.loaded_less and g.loaded_less == 1) then -- {{{
				o.modifiable = true -- somewhy it ignores the one that is set below, so, that's why this is in separate 'if'
				-- vim.cmd[[source $MYVIMRC]] -- kludge to re-colorize statusline
				bo.modifiable=false
				bo.readonly=true
        -- Re-read file and page forward "tail -f"
        -- map <buffer> F :e<CR>G<SID>L:sleep 1<CR>F
			end -- }}}
--}}}]=]

      if exists":Man" == 2 and bo.filetype == "man" then -- {{{
        vim.cmd[[runtime! less.vim]]
      end                                             -- }}}

      -- {{{ Unused code
      --		" Scroll one line forward
      --			noremap <buffer> <script> <CR> <C-E><SID>L
      --			map <buffer> <C-N> <CR>
      --			map <buffer> e <CR>
      --			map <buffer> <C-E> <CR>
      --			map <buffer> j <CR>
      --			map <buffer> <C-J> <CR>
      --			map <buffer> <Down> <CR>
      --
      --		" Scroll one page backward
      --			noremap <buffer> <script> b <C-B><SID>L
      --			map <buffer> <C-B> b
      --			map <buffer> <PageUp> b
      --			map <buffer> <kPageUp> b
      --			map <buffer> <S-Up> b
      --			map <buffer> w b
      --			map <buffer> <Esc>v b
      --
      --			noremap <buffer> <script> k <C-Y><SID>L
      --			map <buffer> y k
      --			map <buffer> <C-Y> k
      --			map <buffer> <C-P> k
      --			map <buffer> <C-K> k
      --			map <buffer> <Up> k
      --
      --		" Redraw
      --			noremap <buffer> <script> r <C-L><SID>L
      --			noremap <buffer> <script> <C-R> <C-L><SID>L
      --			noremap <buffer> <script> R <C-L><SID>L
      --
      --		" Start of file
      --			noremap <buffer> <script> g gg<SID>L
      --			map <buffer> < g
      --			map <buffer> <Esc>< g
      --			map <buffer> <Home> g
      --			map <buffer> <kHome> g
      --
      --		" End of file
      --			noremap <buffer> <script> G G<SID>L
      --			map <buffer> > G
      --			map <buffer> <Esc>> G
      --			map <buffer> <End> G
      --			map <buffer> <kEnd> G
      --
      --		" Search
      --			noremap <buffer> <script> / H$:call <SID>Forward()<CR>/
      --			if &wrap
      --				noremap <buffer> <script> ? H0:call <SID>Backward()<CR>?
      --			else
      --				noremap <buffer> <script> ? Hg0:call <SID>Backward()<CR>?
      --			endif
      --
      --			fun! s:Forward()
      --			" Searching forward
      --				noremap <buffer> <script> n H$nzt<SID>L
      --				if &wrap
      --					noremap <buffer> <script> N H0Nzt<SID>L
      --				else
      --					noremap <buffer> <script> N Hg0Nzt<SID>L
      --				endif
      --				cnoremap <buffer> <silent> <script> <CR> <CR>:cunmap <lt>CR><CR>zt<SID>L
      --			endfun
      --
      --			fun! s:Backward()
      --			" Searching backward
      --				if &wrap
      --					noremap <buffer> <script> n H0nzt<SID>L
      --				else
      --					noremap <buffer> <script> n Hg0nzt<SID>L
      --				endif
      --				noremap <buffer> <script> N H$Nzt<SID>L
      --				cnoremap <buffer> <silent> <script> <CR> <CR>:cunmap <lt>CR><CR>zt<SID>L
      --			endfun
      --			}}}
    end -- /if }}}
  end -- /fun }}}

  au("VimEnter", "*", check_pager_mode)
end
