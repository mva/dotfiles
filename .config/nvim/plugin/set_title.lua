-- luacheck: globals vim

-- local term = os.getenv("TERM") --vim.go.term
local hn

-- XXX Fix a vim bug: Only t_te, not t_op, gets sent when leaving an alt screen
-- vim.go.t_te=vim.go.t_te..vim.go.t_op
-- TODO: ^^^ check if not needed anymore

-- Set the to- and from-status-line sequences to match the xterm titlebar
-- manipulation begin/end sequences for any terminal where
-- a) We don't know for a fact that these sequences would be wrong, and
-- b) the sequences were not already set in terminfo.
-- NOTE: This would be nice to fix in terminfo, instead...
--[[
if term:match"linux" or term:match"cons" or term:match"vt" and not vim.go.t_ts and not vim.go.t_fs then
  vim.go.t_ts="\\<ESC>]2;"
  vim.go.t_fs="\\<C-G>"
end
]]
if not (vim.env.TMUX) and vim.env.SSH_TTY then
  hn = vim.fn.substitute(vim.fn.hostname(), "\\([^.]*\\)\\(\\..*\\)", "\\1", "")
  if hn then
    hn = hn .. ":"
  end
else
  hn = ""
end

-- [[ Titlebar ]]
vim.go.title = true -- Turn on titlebar support
-- vim.go.titleold = ""
vim.go.titlestring = hn .. '%{&ft=~"^man"?"man":&ro?"view":"vim"}:%{expand("%:t")}'
-- ..":".expand("%")."]"

if not (vim.go.titleold) then
  vim.go.titleold = hn .. (vim.env.SHELL or "sh")
end
--let &titlestring = hostname() . '> ' . '%{expand("%:p:~:h")}'
--                \ . ' || %{&ft=~"^man"?"man":&ro?"view":"vim"} %f %m'
--
