-- luacheck: globals vim
vim.filetype.add({
  extension = {
    ebuild = "ebuild",
    gpg = "gpg",
    pgp = "gpg",
    asc = "gpg",
    ['tdesktop-theme'] = "tdesktop-theme",
  },
  -- filename = {
  --     ["Foofile"] = "fooscript",
  -- },
  -- pattern = {
  --     ["~/%.config/foo/.*"] = "fooscript",
  -- }
})
