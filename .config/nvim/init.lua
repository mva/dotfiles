-- luacheck: globals vim T

-- [[ Starting from stuff that shouldn't cast any notification (mangling over variables) ]]
local g = vim.g
local o = vim.opt
--local cmd = vim.cmd

-- Set mapleader here, before any plugins, to not miss any leader mappings because of switching
g.mapleader = [[ ]]
g.maplocalleader = [[ ]]

-- Skip some remote provider loading
g.loaded_python3_provider = 0
g.loaded_node_provider = 0
g.loaded_perl_provider = 0
g.loaded_ruby_provider = 0

-- Do not source the default filetype.vim as NVim had migrated to filetype.lua,
-- and also we'll load filetype.nvim later (and it's much more faster)
-- and also we'll load polyglot then, which is slow as fuck anyway.
--g.do_filetype_lua = 1
--g.did_load_filetypes = 0

o.termguicolors = true -- kludge for packer_compiled and early-preloading of nvim-notify

require("lib.globals")

_G.string = require"lib.string" -- luacheck: ignore
_G.table = require"lib.table" or table -- luacheck: ignore
_G.T = table.create or function(t) -- luacheck: ignore
  return t
end -- luacheck: ignore

_G.NVIM_HACKS = {}
local NH = vim.env.NVIM_HACKS or ""
for _, v in pairs(NH:split(",")) do --- @diagnostic disable-line: undefined-field
  _G.NVIM_HACKS[v] = true
end

vim.api.nvim_set_hl(0, "NotifyBackground", { bg = "NONE" })

pcall(require, "custom-pre")

require"lazylazy.pm" -- run Lazy (Package Manager)
require"lazylazy" -- preload options, keymaps and autocommands

pcall(require, "custom-post")

-- cmd[[start]] -- auto-enter Insert mode (actually, mostly useless)
