-- luacheck: globals stds std
stds.nvim = {
  globals = {
    "req",
    "vim",
    "T",
  }
}
std = "max+luajit+nvim"
