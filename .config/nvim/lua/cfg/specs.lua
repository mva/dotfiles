-- luacheck: globals vim

local s = require"specs"

s.setup{
  show_jumps       = true,
  min_jump         = 0,
  popup            = {
    delay_ms = 0,     -- delay before popup displays
    inc_ms = 10,      -- time increments used for fade/resize effects
    blend = 10,       -- starting blend, between 0-100 (fully transparent), see :h winblend
    width = 100,
    winhl = "Search",
    -- fader = s.linear_fader,
    fader = s.pulse_fader,
    -- fader = s.exp_fader,
    resizer = s.shrink_resizer,
    -- resizer = s.slide_resizer,
  },
  ignore_filetypes = {},
  ignore_buftypes  = {
    -- nofile = true,
  },
}

s.hl_toggle = function()
  vim.wo.cursorcolumn = not (vim.wo.cursorcolumn)
  vim.wo.cursorline = not (vim.wo.cursorline)
end

s.hl_blink = function()
  s.hl_toggle()
end

nnoremap("<c-k>", function()
  s.show_specs()
  s.hl_toggle()
end)
nnoremap("n", function()
  vim.fn.feedkeys("n")
  print("moo2")
  s.show_specs()
  vim.fn.feedkeys("<cr>")
end)
nnoremap("N", function()
  vim.fn.feedkeys("N")
  print("moo")
  s.show_specs()
  vim.fn.feedkeys("<cr>")
end)
-- nnoremap("N", 'N<cmd>lua require"cfg.specs".show_specs()<cr>')

-- vim.api.nvim_set_hl(0,"CursorLine",{link = "CursorLineNr"})
vim.api.nvim_set_hl(0, "CursorLine", { link = "CursorColumn" })
