-- luacheck: globals vim T

--[[
local parser_configs = require"nvim-treesitter.parsers".get_parser_configs()
parser_configs.norg = { -- {{{
  install_info = { -- {{{
    url = "https://github.com/nvim-neorg/tree-sitter-norg",
    files = { "src/parser.c", "src/scanner.cc" },
    branch = "main"
  }, -- }}}
} -- }}}
--]]


local ensure

if _G.NVIM_HACKS["install:ts"] then
  ensure = true
end

local ensure_installed = ensure and {
  "c",
  "cpp",

  "rust",
  "toml",

  "comment",
  "dockerfile",
  "http",

  "html",
  "javascript",
  "css",

  "bash",
  "lua",
  "go",
  "gomod",
  "json",
  "json5",
  "jsonc",

  "yaml",
  "python",
  "vim",
  "vimdoc",

  "norg",
  "org",

  "latex",

  "make",

  "query",
  "regex",

  "gitcommit",

  "markdown",
  "markdown_inline"
}

local parsers_path = ("%s/ts_parsers"):format(vim.fn.stdpath("data"))
vim.opt.runtimepath:append(parsers_path)

require"nvim-treesitter.configs".setup{
  parser_install_dir = parsers_path,
  ensure_installed = ensure_installed,
  sync_install = false,
  auto_install = true,
  ignore_install = {},
  rainbow = {
    enable = false,
    extended_mode = true,
    max_file_lines = 1000,
    colors = {
      '#B52A5B',
      '#FF4971',
      '#bd93f9',
      '#E9729D',
      '#F18FB0',
      '#8897F4',
      '#b488bf'
    },
  },
  highlight = {
    enable = true, --- 🤷
    disable = function(lang, bufnr)
      local disabled_parsers = {
        html = true,
        -- lua = true,
        diff = true,
      }
      if disabled_parsers[lang] then --and vim.api.nvim_buf_line_count(bufnr) > 500 then
        return true
      end
      local max_filesize = 100 * 1024 -- 100 KB
      local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(bufnr))
      if ok and stats and stats.size > max_filesize then
        return true
      end
      for _, line in ipairs(vim.api.nvim_buf_get_lines(0, 0, 3, false)) do
        if #line > 100 then
          return true
        end
      end
      return false
    end,
    additional_vim_regex_highlighting = false,
  },
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "gnn",
      node_incremental = "gnn",
      scope_incremental = "gns",
      node_decremental = "gnp",
    },
  },
  indent = {
    enable = true,
    disable = function(lang, _)
      local ft_dis = {
        -- lua = true, -- giving a chance
      }
      return not (not (ft_dis[lang]))
    end,
  }, -- problems with indentation in lua. TODO: investigate and try to fix
  query_linter = {
    enable = true,
    use_virtual_text = true,
    lint_events = { "BufWrite", "CursorHold" }, -- "CursorHoldI", },
  },
  textsubjects = {
    enable = true,
    keymaps = { [','] = 'textsubjects-smart', }
  },
  autopairs = { -- not needed because of more powerful external plugin using ts
    enable = false,
    -- disable = function(lang, bufnr) end,
  },
  textobjects = {
    select = {
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = {
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["il"] = "@loop.outer",
        ["al"] = "@loop.outer",
        ["icd"] = "@conditional.inner",
        ["acd"] = "@conditional.outer",
        ["acm"] = "@comment.outer",
        ["ast"] = "@statement.outer",
        ["isc"] = "@scopename.inner",
        ["iB"] = "@block.inner",
        ["aB"] = "@block.outer",
        ["p"] = "@parameter.inner",
      },
    },
    move = {
      enable = true,
      set_jumps = true, -- Whether to set jumps in the jumplist
      goto_next_start = {
        ["gnf"] = "@function.outer",
        ["gnif"] = "@function.inner",
        ["gnp"] = "@parameter.inner",
        ["gnc"] = "@call.outer",
        ["gnic"] = "@call.inner",
      },
      goto_next_end = {
        ["gnF"] = "@function.outer",
        ["gniF"] = "@function.inner",
        ["gnP"] = "@parameter.inner",
        ["gnC"] = "@call.outer",
        ["gniC"] = "@call.inner",
      },
      goto_previous_start = {
        ["gpf"] = "@function.outer",
        ["gpif"] = "@function.inner",
        ["gpp"] = "@parameter.inner",
        ["gpc"] = "@call.outer",
        ["gpic"] = "@call.inner",
      },
      goto_previous_end = {
        ["gpF"] = "@function.outer",
        ["gpiF"] = "@function.inner",
        ["gpP"] = "@parameter.inner",
        ["gpC"] = "@call.outer",
        ["gpiC"] = "@call.inner",
      },
    },
    swap = {
      enable = true,
      swap_next = { ["<leader>xp"] = "@parameter.inner" },
      swap_previous = { ["<leader>xP"] = "@parameter.inner" }
    },
    lsp_interop = {
      enable = true,
      border = 'none',
      peek_definition_code = {
        ["<leader>pf"] = "@function.outer",
        ["<leader>pc"] = "@class.outer"
      }
    },
  },
  tree_docs = { enable = false, }, -- Currently experimental, doesn't work well
  endwise = { enable = true, },
  autotag = { enable = true, },
  matchup = {
    enable = true, -- mandatory, false will disable the whole extension
    -- disable = { "c", "ruby" },  -- optional, list of language that will be disabled
    disable_virtual_text = false,
    include_match_words = true,
  },
  refactor = {
    highlight_definitions = {
      enable = true,
      -- Set to false if you have an `updatetime` of ~100.
      clear_on_cursor_move = true,
    },
    highlight_current_scope = {
      enable = false, -- folke/twilight.nvim
    },
    smart_rename = {
      enable = true,
      -- Assign keymaps to false to disable them, e.g. `smart_rename = false`.
      keymaps = {
        smart_rename = "grr",
      },
    },
    navigation = {
      enable = true,
      -- Assign keymaps to false to disable them, e.g. `goto_definition = false`.
      keymaps = {
        goto_definition = "gnd",
        list_definitions = "gnD",
        list_definitions_toc = "gO",
        goto_next_usage = "<a-]>",
        goto_previous_usage = "<a-[>",
      },
    },
  },
  modules = {},
}
