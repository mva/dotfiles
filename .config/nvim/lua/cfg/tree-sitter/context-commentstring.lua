-- luacheck: globals vim

vim.g.skip_ts_context_commentstring_module = true
require('ts_context_commentstring').setup{
  enable = true,
  config = {
    css = '// %s',
    javascript = {
      __default = '// %s',
      jsx_element = '{/* %s */}',
      jsx_fragment = '{/* %s */}',
      jsx_attribute = '// %s',
      comment = '// %s'
    },
    crontab = "# %s",
    dockerfile = "# %s",
  },
  enable_autocmd = false,
}
