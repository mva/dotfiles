-- luacheck: globals vim req

local hi = vim.api.nvim_set_hl

hi(0, "IndentBlanklineIndent1", { fg = "#E06C75", nocombine = 1 })
hi(0, "IndentBlanklineIndent2", { fg = "#E5C07B", nocombine = 1 })
hi(0, "IndentBlanklineIndent3", { fg = "#98C379", nocombine = 1 })
hi(0, "IndentBlanklineIndent4", { fg = "#56B6C2", nocombine = 1 })
hi(0, "IndentBlanklineIndent5", { fg = "#61AFEF", nocombine = 1 })
hi(0, "IndentBlanklineIndent6", { fg = "#C678DD", nocombine = 1 })

require"indent_blankline".setup{
  filetype_exclude = {
    "help", "terminal", "dashboard", "packer", "lspinfo", "TelescopePrompt",
    "TelescopeResults", "startify", "dotooagenda", "log",
    "fugitive", "gitcommit", "packer", "vimwiki", "markdown", "txt",
    "vista", "norg", "todoist", "NvimTree", "peekaboo", "git",
    "TelescopePrompt", "undotree", "flutterToolsOutline", "lsp-installer", "",
    "mason.nvim", "mason", "neo-tree", "man", "startuptime", "lazy",
    "noice", "alpha", "Trouble"
  },
  buftype_exclude = {
    "terminal", "norg", "TelescopePrompt", "Startify",
    "noice",
  },
  indentLine_enabled = 1,
  char = "▏",
  -- show_trailing_blankline_indent = true, -- false,
  show_trailing_blankline_indent = false,
  space_char_blankline = " ",
  show_end_of_line = true,
  show_first_indent_level = true,    -- false,
  show_current_context = true,
  show_current_context_start = true, -- false,
  max_indent_increase = 1,
  indent_level = 99,
  char_highlight_list = {
    "IndentBlanklineIndent1", "IndentBlanklineIndent2",
    "IndentBlanklineIndent3", "IndentBlanklineIndent4",
    "IndentBlanklineIndent5", "IndentBlanklineIndent6",
  },
}
