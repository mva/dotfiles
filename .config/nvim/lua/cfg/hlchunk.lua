local disable_all_for_ft = {
  "help", "terminal", "dashboard", "packer", "lspinfo", "TelescopePrompt",
  "TelescopeResults", "startify", "dotooagenda", "log",
  "fugitive", "gitcommit", "packer", "vimwiki", "markdown", "txt",
  "vista", "norg", "todoist", "NvimTree", "peekaboo", "git",
  "TelescopePrompt", "undotree", "flutterToolsOutline", "lsp-installer", "",
  "mason.nvim", "mason", "neo-tree", "man", "startuptime", "lazy",
  "noice", "alpha", "Trouble",
}

require("hlchunk").setup({
  chunk = {
    enable = true,
    exclude_filetypes = disable_all_for_ft,
    -- notify = true,
    style = {
      "#56B6C2", -- norm
      -- "#61AFEF",
      "#FF0000", -- error
    },
  },
  indent = {
    enable = true,
    exclude_filetypes = disable_all_for_ft,
    -- notify = true,
    style = {
      -- "#FF0000",
      -- "#FF7F00",
      -- "#FFFF00",
      -- "#00FF00",
      -- "#00FFFF",
      -- "#0000FF",
      -- "#8B00FF",
      "#E06C75",
      "#E5C07B",
      "#98C379",
      "#56B6C2",
      "#61AFEF",
      "#C678DD",
      -- "#555555",
    },
    chars = { "│", "¦", "┆", "┊", },
    use_treesitter = true,
  },
  --[[ blank = {
    enable = false,
    exclude_filetypes = disable_all_for_ft,
    notify = true,
    style = {
      -- { bg = "#434437" },
      -- { bg = "#2f4440" },
      -- { bg = "#433054" },
      -- { bg = "#284251" },
      "#E06C75",
      "#E5C07B",
      "#98C379",
      "#56B6C2",
      "#61AFEF",
      "#C678DD",
    },
    chars = { " ", "․", "⁚", "⁖", "⁘", "⁙", },
  }, ]]
})
