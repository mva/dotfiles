-- luacheck: globals vim T

local Dropbar = {
  condition = function(self)
    local dropbar = require"dropbar"
    self.data = vim.tbl_get(dropbar.bars or {}, vim.api.nvim_get_current_buf(), vim.api.nvim_get_current_win())
    return self.data
  end,
  static = { dropbar_on_click_string = 'v:lua.dropbar.on_click_callbacks.buf%s.win%s.fn%s' },
  init = function(self)
    local components = self.data.components
    local children = {}
    for i, c in ipairs(components) do
      local child = {
        {
          provider = c.icon,
          hl = c.icon_hl
        },
        {
          hl = c.name_hl,
          provider = c.name,
        },
        on_click = {
          callback = self.dropbar_on_click_string:format(self.data.buf, self.data.win, i),
          name = "heirline_dropbar",
        },
      }
      if i < #components then
        local sep = self.data.separator
        table.insert(child, {
          provider = sep.icon,
          hl = sep.icon_hl,
          on_click = {
            callback = self.dropbar_on_click_string:format(self.data.buf, self.data.win, i + 1)
          }
        })
      end
      table.insert(children, child)
    end
    self.child = self:new(children, 1)
  end,
  provider = function(self) return self.child:eval() end,
}

return Dropbar
