-- luacheck: globals vim T

local utils = require"cfg.heirline.utils" --- @diagnostic disable-line: different-requires
local conditions = require"cfg.heirline.conditions" --- @diagnostic disable-line: different-requires
local icons = require"cfg.icons"
local mvaline_colors = require"cfg.heirline.colors.mva"
local special = require"cfg.heirline.blocks.special"


local _M = T{}

_M.FileReadOnly = {
  provider = (" %s "):format(icons.fileinfo.ro),
  hl = mvaline_colors["isRO"],
  condition = function(self)
    local nr = self.bufnr or 0
    if
        conditions.buffer_not_empty(nr)
        and not conditions.is_special(nr)
        and (not vim.bo[nr].modifiable or vim.bo[nr].readonly)
    then
      return true
    end
  end,
}
_M.FileIcon = {
  init = function(self)
    self.icon, self.iconhl = utils.get_file_icon{
      bufnr = self.bufnr or 0,
      bufname = self.filename,
    }
    -- self.bg = mvaline_colors.FileName.bg
  end,
  provider = function(self)
    return (" %s"):format(self.icon)
  end,
  hl = function(self)
    return { bg = self.bg, fg = self.iconhl }
  end,
  condition = function(self)
    local nr = self.bufnr or 0
    if conditions.is_man(nr) or conditions.is_help(nr) then
      return true
    elseif conditions.is_special(nr) then
      return false
    else
      return true
    end
  end,
}
_M.FileName = {
  provider = function(self)
    return (" %s "):format(utils.get_filename{ bufname = self.file_fullname, bufnr = self.bufnr })
  end,
  condition = function(self)
    local nr = self.bufnr or 0
    if conditions.is_man(nr) or conditions.is_help(nr) then
      return true
    elseif conditions.is_special(nr) then
      return false
    else
      return true
    end
  end,
  hl = mvaline_colors["FileName"],
  update = {
    "WinNew",
    "WinClosed",
    "BufEnter",
    "BufNew",
  },
}
_M.TerminalName = {
  provider = function()
    local tname, _ = vim.api.nvim_buf_get_name(0):gsub(".*:", "")
    return " " .. tname
  end,
  hl = {
    fg = utils.get_highlight("Title").fg,
    bold = true
  },
}
_M.FileSaved = {
  provider = function(self)
    local nr = self.bufnr or 0
    local icon
    if vim.bo[nr].modified then
      icon = icons.fileinfo.unsaved
    else
      icon = icons.fileinfo.saved
    end
    return ("%s "):format(icon)
  end,
  condition = function(self)
    local nr = self.bufnr or 0
    return conditions.buffer_not_empty(nr)
        and not conditions.is_special(nr)
  end,
  hl = function(self)
    local nr = self.bufnr or 0
    if vim.bo[nr].modified then
      return mvaline_colors["Modified"]
    else
      return mvaline_colors["Saved"]
    end
  end,
}
_M.FileBlockEndSep = { -- TODO: think about removing as unused anymore
  provider = function()
    return (" %s"):format(icons.sep.left_heavy)
  end,
  update = "ModeChanged",
  hl = mvaline_colors["FileNameArrow"],
}

_M.GitSep = {
  provider = function()
    return ("%s "):format(icons.sep.left_light)
  end,
  hl = mvaline_colors["FileName"],
  condition = conditions.is_git_repo,
  update = "ModeChanged",
}
_M.GitIcon = {
  provider = function()
    return ("%s "):format(icons.vcs.git)
  end,
  condition = conditions.is_git_repo,
  hl = mvaline_colors["VCSI_git"],
}
_M.GitBranch = {
  provider = function(self)
    local ret = self.status_dict.head --  " "
    return (" %s"):format(ret):gsub(("(%s).+"):format(("."):rep(vim.g.statusline_branch_length_limit or 15)), "%1...")
  end,                                -- 'GitBranch',
  condition = conditions.is_git_repo,
  hl = mvaline_colors["VCSI_branch"],
}

_M.DiffAdd = {
  provider = function(self)
    local count = self.status_dict.added or 0
    return count > 0 and ("%s%d "):format(self.icon.added, count)
  end,
  condition = function(self)
    return self.has_changes
  end,
  -- static = { icon = ("%s "):format(icons.diff.add) },
  hl = "diffAdd",
}
_M.DiffModified = {
  provider = function(self)
    local count = self.status_dict.changed or 0
    return count > 0 and ("%s%d "):format(self.icon.changed, count)
  end,
  condition = function(self)
    return self.has_changes
  end,
  hl = "diffChange",
}
_M.DiffRemove = {
  provider = function(self)
    local count = self.status_dict.removed or 0
    return count > 0 and ("%s%d "):format(self.icon.removed, count)
  end,
  condition = function(self)
    return self.has_changes
  end,
  hl = "diffDelete",
}

_M.FileType = {
  provider = function(self)
    local nr = self.bufnr or 0
    local ft = vim.bo[nr].filetype
    if #ft == 0 then
      ft = "none"
    end
    return (" %s "):format(ft)
  end,
  hl = mvaline_colors["FileName"],
  -- hl = { fg = utils.get_highlight("Type").fg, bold = true },
}

_M.FileFormat = {
  provider = function(self)
    local nr = self.bufnr or 0
    local icon = icons.nl[vim.bo[nr].fileformat] or ""
    return (" %s "):format(icon)
  end,
  hl = mvaline_colors["Scroll_10"],
}

_M.FileEncoding = {
  provider = function()
    local enc = (vim.bo.fenc ~= "" and vim.bo.fenc) or vim.o.enc -- :h 'enc'
    return enc ~= "utf-8" and enc:upper()
  end,
}

_M.FileSize = {
  provider = function()
    -- stackoverflow, compute human readable file size
    local suffix = { "b", "k", "M", "G", "T", "P", "E" }
    local fsize = vim.fn.getfsize(vim.api.nvim_buf_get_name(0))
    fsize = (fsize < 0 and 0) or fsize
    if fsize < 1024 then
      return fsize .. suffix[1]
    end
    local i = math.floor((math.log(fsize) / math.log(1024)))
    return string.format("%.2g%s", fsize / math.pow(1024, i), suffix[i + 1])
  end,
}

_M.FileLastModified = {
  -- did you know? Vim is full of functions!
  provider = function()
    local ftime = vim.fn.getftime(vim.api.nvim_buf_get_name(0))
    return (ftime > 0) and os.date("%c", ftime)
  end,
}


_M.GitBlk = {
  init = function(self)
    self.status_dict = vim.b.gitsigns_status_dict or {} ---@diagnostic disable-line: undefined-field
    self.has_changes = self.status_dict.added ~= 0 or self.status_dict.removed ~= 0 or self.status_dict.changed ~= 0
  end,
  condition = conditions.is_git_repo,
  _M.GitSep,
  -- special.Space,
  _M.GitIcon,
  _M.GitBranch,
}

_M.DiffBlk = {
  init = function(self)
    self.status_dict = vim.b.gitsigns_status_dict or {} ---@diagnostic disable-line: undefined-field
    self.has_changes = self.status_dict.added ~= 0 or self.status_dict.removed ~= 0 or self.status_dict.changed ~= 0
  end,
  static = {
    icon = icons.git,
    -- icon = {
    --   added = ("%s "):format(icons.git.added),
    --   changed = ("%s "):format(icons.git.changed),
    --   removed = ("%s "):format(icons.git.removed),
    -- },
  },
  condition = conditions.is_git_repo,
  -- condition = function(self) return conditions.is_git_repo() and self.has_changes end,
  _M.GitSep,
  -- special.Space,
  _M.DiffAdd,
  _M.DiffModified,
  _M.DiffRemove,
}

_M.FileBlk = {
  init = function(self)
    -- local bid = vim.api.nvim_get_current_buf()
    self.file_fullname = vim.api.nvim_buf_get_name(self.bufnr or 0)
  end,
  _M.FileReadOnly,
  _M.FileIcon,
  _M.FileName,
  _M.FileSaved,
  special.Space, -- TODO: remove when either konsole start process " " as double-width, or I change (un)save icon

  -- _M.FileBlockEndSep, -- not used anymore
  -- special.Split, -- anyway set in main statusline
  -- _M.GitBlk, -- moved to main stattusline as external block
  -- _M.FileBlockEndSep, -- not used anymore
}




return _M
