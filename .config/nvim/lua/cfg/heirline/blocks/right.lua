-- luacheck: globals vim T

local mvaline_colors = require"cfg.heirline.colors.mva"
-- local conditions = require"cfg.heirline.conditions"
local icons = require"cfg.icons"


local _M = T{}

_M.RightBlockSep = {
  provider = function()
    return ("%s"):format(icons.sep.right_heavy)
  end,
  hl = mvaline_colors["FileNameArrow"],
}

_M.RightInnerSep = {
  provider = icons.sep.right_light,
  hl = mvaline_colors["FileName"],
}

_M.RightBlk = {
  _M.RightBlockSep,
  require"cfg.heirline.blocks.diagnostic.dap".DAPMessages, --- @diagnostic disable-line: different-requires
  require"cfg.heirline.blocks.diagnostic.lsp".LspStatus,
  require"cfg.heirline.blocks.diagnostic".DiagBlk,
  _M.RightInnerSep,
  require"cfg.heirline.blocks.file".FileType,
  _M.RightInnerSep,
  require"cfg.heirline.blocks.file".FileFormat,
  require"cfg.heirline.blocks.special".Split,
  require"cfg.heirline.blocks.position".CursorBlk,
  update = { "CursorMoved", "CursorMovedI", "LspAttach", "LspDetach" },
}


return _M
