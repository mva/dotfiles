-- luacheck: globals vim

-- local conditions = require"cfg.heirline.conditions"
local mvaline_colors = require"cfg.heirline.colors.mva"
local icons = require"cfg.icons"

local _M = {}


_M.PC = {
  provider = " %p%% ",
}

--[[
_M.Ruler = {
  -- %l = current line number
  -- %L = number of lines in the buffer
  -- %c = column number
  -- %P = percentage through file of displayed window
  provider = "%7(%l/%3L%):%2c %P",
}
]]

_M.ScrollBar = {
  static = {
    -- sbar = { '▁', '▂', '▃', '▄', '▅', '▆', '▇', '█' }
    -- Another variant, because the more choice the better.
    sbar = { '🭶', '🭷', '🭸', '🭹', '🭺', '🭻' }
  },
  provider = function(self)
    local pc = self.pc > 0 and self.pc or 1
    local i = math.floor((pc - 1) / 100 * #self.sbar) + 1
    return string.rep(self.sbar[i], 2)
  end,
}

_M.ScrollBlock = {
  _M.PC,
  _M.ScrollBar,
  init = function(self)
    local curr_line = vim.api.nvim_win_get_cursor(0)[1]
    local lines = vim.api.nvim_buf_line_count(0)
    if curr_line == 1 then
      self.pc = 0
    elseif curr_line == lines then
      self.pc = 100
    else
      self.pc = math.modf((curr_line / lines) * 100)
    end
  end,
  hl = function(self)
    local ret
    if self.pc < 10 then
      ret = "Scroll_10"
    elseif self.pc < 30 then
      ret = "Scroll_30"
    elseif self.pc < 50 then
      ret = "Scroll_50"
    elseif self.pc < 70 then
      ret = "Scroll_70"
    elseif self.pc < 90 then
      ret = "Scroll_90"
    else
      ret = "Scroll_100"
    end
    return {
      fg = mvaline_colors[ret].fg,
      bg = mvaline_colors["CurPos"].bg
    }
  end,
  update = { "CursorMoved", "CursorMovedI" },
}

_M.CurBlockSep = {
  provider = icons.sep.right_heavy,
  hl = mvaline_colors["CurArrow"],
}

_M.CurIntSep = {
  provider = icons.sep.right_light,
  hl = {
    fg = mvaline_colors["FileName"].fg,
    bg = mvaline_colors["CurArrow"].fg,
  }
}

_M.LN = {
  provider = (" %s%%l "):format(icons.position.lnum),
  hl = mvaline_colors["CurPos"],
  update = { "CursorMoved", "CursorMovedI" },
}

_M.CN = {
  init = function(self)
    self.pos = vim.fn.charcol"."
  end,
  hl = function(self)
    local ret
    if self.pos < 35 then
      ret = "LinePos_1"
    elseif self.pos < 73 then
      ret = "LinePos_30"
    elseif self.pos < 77 then
      ret = "LinePos_70"
    else
      ret = "LinePos_80"
    end
    return mvaline_colors[ret]
  end,
  -- provider = (" %s%%c "):format(icons.position.cnum),
  -- %c uses col instead of charcol, so column numbering goes irrelevant with multibyte characters in line
  provider = function(self) return (" %s%d "):format(icons.position.cnum, self.pos) end,
  update = { "CursorMoved", "CursorMovedI" },
}

_M.CursorBlk = {
  _M.CurBlockSep,
  _M.CN,
  _M.CurIntSep,
  _M.LN,
  _M.CurIntSep,
  _M.ScrollBlock,
}


return _M
