-- luacheck: globals vim T

local conditions = require"cfg.heirline.conditions" --- @diagnostic disable-line: different-requires
local icons = require"cfg.icons"
local mvaline_colors = require"cfg.heirline.colors.mva"

local _M = T{}

_M.VimMode = {
  provider = function(self)
    local m = self.mode
    local mt = self.modes
    local ml = mt[m] and mt[m].n or m

    return ("%%8( %s %%)"):format(ml) -- %8(...%) == minimal length = 8 chars, %% = escaing %
  end,
  hl = function(self)
    return mvaline_colors[self.modes[self.mode].cg or "NormalMode"]
  end,
  update = {
    "ModeChanged",
    -- "CursorMoved",
  },
  condition = function()
    return (vim.o.bt ~= "nofile")
  end,
}
_M.PagerMode = {
  provider = function()
    if conditions.is_man() then
      return "%3( MAN %)"
    elseif conditions.is_help() then
      return "%4( HELP %)"
      -- elseif conditions.is_less() then
      --   return "%5( PAGER %)"
    elseif vim.o.bt == "nofile" then
      return (" %s "):format(vim.o.ft:upper())
    else
      return " DUNNO LOL "
    end
  end,
  hl = function(self)
    return mvaline_colors[self.modes[self.mode].cg or "NormalMode"]
    -- return mvaline_colors["NormalMode"] -- maybe Insert or Replace? :D
  end,
  condition = function()
    -- return (is_man() or is_help() or is_less() or vim.o.bt == "nofile")
    return (vim.o.bt == "nofile" or vim.o.bt == "help")
  end,
}
_M.ModeSep = {
  -- provider = ("%s "):format(icons.sep.left_heavy),
  provider = icons.sep.left_heavy,
  hl = function(self)
    local hl_g = self.modes[self.mode].cg or "NormalMode"
    return mvaline_colors[("%sArrow"):format(hl_g)]
  end,
  -- condition = function() return not vim.go.paste end,
  update = {
    "ModeChanged",
  },
}
_M.VisualMode = {
  provider = function()
    return " x " -- TODO: block sizes
  end,
  condition = function(self)
    return self.mode:match"^[vV]"
  end,
  hl = mvaline_colors["VisualSelMode"],
  update = {
    "ModeChanged",
  },
}
_M.VisualModeArrow = {
  -- provider = ("%s "):format(icons.sep.left_heavy),
  provider = icons.sep.left_heavy,
  condition = function(self)
    return self.mode:match"^[vV]"
  end,
  hl = function() -- self
    -- return mvaline_colors[("VisualSelModeArrow%s"):format((vim.go.paste and "Paste" or ""))]
    return mvaline_colors["VisualSelModeArrow"]
  end,
  update = {
    "ModeChanged",
  },
}
_M.PasteMode = {
  provider = " PASTE ",
  condition = function()
    if not (conditions.is_special()) then
      return vim.go.paste
    end
  end,
  hl = mvaline_colors["PasteMode"],
  update = {
    "ModeChanged",
  },
}

--[=[
--[[ local MyModeSepPaste = {
--   -- provider = ("%s "):format(icons.sep.left_heavy),
--   provider = icons.sep.left_heavy,
--   hl = function (self)
--     local hl_g = self.modes[self.mode].cg or "NormalMode"
--     return mvaline_colors[("%sArrowPaste"):format(hl_g)]
--   end,
--   update = {
--     "ModeChanged",
--   },
--   condition = function() return vim.go.paste end,
-- } ]]
--[[ local PasteModeArrow = {
--   -- provider = ("%s "):format(icons.sep.left_heavy),
--   provider = icons.sep.left_heavy,
--   condition = function() return vim.go.paste end,
--   hl = mvaline_colors['PasteArrow'],
--   update = {
--     "ModeChanged",
--   },
-- } ]]
--[[ -- local MyPasteBlk = {
--   MyModeSepPaste, PasteMode, PasteModeArrow,
-- } ]]
]=]

_M.ModeBlk = {
  init = function(self)
    self.mode = vim.fn.mode(1) -- :h mode()
    if not self.once then
      vim.api.nvim_create_autocmd("ModeChanged", {
        pattern = "*:*o",
        command = "redrawstatus",
      })
      self.once = true
    end
  end,
  static = {
    modes = require"cfg.heirline.modes",
  },
  _M.PasteMode,
  _M.PagerMode,
  _M.VimMode,
  _M.ModeSep,
  _M.VisualMode,
  _M.VisualModeArrow,
}

_M.SpecialModeBlk = {
  init = function(self)
    self.mode = "i"
    if not self.once then
      vim.api.nvim_create_autocmd("ModeChanged", {
        pattern = "*:*o",
        command = "redrawstatus",
      })
      self.once = true
    end
  end,
  static = {
    modes = require"cfg.heirline.modes",
  },
  _M.PagerMode,
  _M.ModeSep,
}

return _M
