-- luacheck: globals vim T

local _M = T{}

_M.Align = { provider = "%=" }
_M.Space = { provider = " " }
_M.Split = { provider = "%<" }


return _M

