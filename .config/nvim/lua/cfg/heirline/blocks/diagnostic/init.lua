-- luacheck: globals vim T

local utils = require"heirline.utils"
local conditions = require"cfg.heirline.conditions" --- @diagnostic disable-line: different-requires
local diags = conditions.diags
local icons = require"cfg.icons"
local mvaline_colors = require"cfg.heirline.colors.mva"

local _M = T{}

_M.TrailingWhiteSpace = {
  provider = function()
    local bw = vim.fn.search("\\s$", "b", vim.fn.line"w0")  -- TODO: vim.api + additional lines
    local fw = vim.fn.search("\\s$", "", vim.fn.line"w$")
    if bw ~= 0 or fw ~= 0 then
      return "  "
    end
  end,
  hl = { fg = "red", bold = true },
}

_M.WorkDir = {
  provider = function()
    local icon = (vim.fn.haslocaldir(0) == 1 and "l" or "g") .. " " .. " "
    local cwd = vim.fn.getcwd(0)
    cwd = vim.fn.fnamemodify(cwd, ":~")
    if not conditions.width_percent_below(#cwd, 0.25) then
      cwd = vim.fn.pathshorten(cwd)
    end
    local trail = cwd:sub(-1) == "/" and "" or "/"
    return icon .. cwd .. trail
  end,
  hl = { fg = "blue", bold = true },
}

_M.DiagnosticHint = {
  init = function(self)
    self.lvl = "Hint"
    self.c = #diags(self.lvl) or 0
  end,
  -- condition = function(self) return self.c>0 end,
  provider = function(self)
    return self.c > 0 and (" %s %d "):format(icons.diagnostics[self.lvl], self.c) -- u'f071'
  end,
  hl = {
    bg = mvaline_colors["LSP_Active"].bg,
    fg = utils.get_highlight("DiagnosticHint").fg,
  },
}
_M.DiagnosticInfo = {
  init = function(self)
    self.lvl = "Info"
    self.c = #diags(self.lvl) or 0
  end,
  -- condition = function(self) return self.c>0 end,
  provider = function(self)
    return self.c > 0 and (" %s %d "):format(icons.diagnostics[self.lvl], self.c) -- u'f071'
  end,
  hl = {
    bg = mvaline_colors["LSP_Active"].bg,
    fg = utils.get_highlight("DiagnosticInfo").fg,
  },
}
_M.DiagnosticWarn = {
  init = function(self)
    self.lvl = "Warn"
    self.c = #diags(self.lvl) or 0
  end,
  -- condition = function(self) return self.c>0 end,
  provider = function(self)
    return self.c > 0 and (" %s %d "):format(icons.diagnostics[self.lvl], self.c) -- u'f071'
  end,
  hl = {
    bg = mvaline_colors["LSP_Active"].bg,
    fg = utils.get_highlight("DiagnosticWarn").fg,
  },
}
_M.DiagnosticError = {
  init = function(self)
    self.lvl = "Error"
    self.c = #diags(self.lvl) or 0
  end,
  -- condition = function(self) return self.c>0 end,
  provider = function(self)
    return self.c > 0 and (" %s %d "):format(icons.diagnostics[self.lvl], self.c) -- u'f071'
  end,
  hl = {
    bg = mvaline_colors["LSP_Active"].bg,
    fg = utils.get_highlight("DiagnosticError").fg,
  },
}

--[[{{{
DiagArrow = {
  provider = function()
    if #diags() > 0 then
      return (" %s "):format(icons.sep.right_heavy)
    end
  end,
  hl = 'ModeSep',
}
}}}]]


--[=[
Delimiters are subject to the Diagnostics component condition; if you'd like them to be always visible,
just wrap 'em around the component or use the surround() utility!

Diagnostics = { { provider = "![" }, Diagnostics, { provider = "]" } }
-- or
Diagnostics = utils.surround({"![", "]"}, nil, Diagnostics)
--]=]

_M.DiagBlk = {
  condition = conditions.has_diagnostics,
  update = { "DiagnosticChanged", "BufEnter" },
  _M.DiagnosticHint,
  _M.DiagnosticInfo,
  _M.DiagnosticWarn,
  _M.DiagnosticError,
}



return _M
