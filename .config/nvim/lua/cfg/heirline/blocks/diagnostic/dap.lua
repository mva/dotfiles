-- luacheck: globals vim T

local _M = T{}

_M.DAPMessages = {
  condition = function()
    -- local has = require"lib.lazyutil".has
    local dap_loaded = not(not(package.loaded["dap"])) --- @diagnostic disable-line: codestyle-check
    --has'nvim-dap' -- TODO: check if it is loaded, not just exists
    if dap_loaded then
      local session = require"dap".session()
      return session ~= nil
    end
  end,
  provider = function()
    local status = require"dap".status()
    if not status or status == '' then
      return ''
    end
    return "  " .. status
  end,
  hl = "Debug",
  -- see Click-it! section for clickable actions
}

return _M
