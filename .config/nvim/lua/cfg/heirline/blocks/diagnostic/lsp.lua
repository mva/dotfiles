-- luacheck: globals vim T

local conditions = require"cfg.heirline.conditions" --- @diagnostic disable-line: different-requires
local icons = require"cfg.icons"
local mvaline_colors = require"cfg.heirline.colors.mva"

local _M = T{}
_M.LspStatus = {
  -- provider = " [LSP]",
  --[[ provider  = function()
        local names = {}
        for i, server in pairs(vim.lsp.get_active_clients()) do
            table.insert(names, server.name)
        end
        return " [" .. table.concat(names, " ") .. "]"
    end, ]]
  provider = function()
    return ("  %s  "):format(icons.lsp.status)
  end,
  hl = function()
    -- local connected = not vim.tbl_isempty(vim.lsp.get_clients{ bufnr = 0 })
    local connected = not vim.tbl_isempty(vim.lsp.get_clients())
    local ret
    if connected then
      ret = "LSP_Active"
    else
      ret = "LSP_Inactive"
    end
    return mvaline_colors[ret]
  end,
  update = { "LspAttach", "LspDetach" },
  condition = conditions.lsp_attached,
}
--[[ local LSPMessages = {
    provider = require("lsp-status").status,
    hl = { fg = "gray" },
} ]]


return _M
