-- luacheck: globals vim T

local _M = require"heirline.utils"

local conditions = require"heirline.conditions" --- @diagnostic disable-line: different-requires

---Get the names of all current listed buffers
---@return table
function _M.get_current_filenames()
  local listed_buffers = vim.tbl_filter(
    function(bufnr) return vim.bo[bufnr].buflisted and vim.api.nvim_buf_is_loaded(bufnr) end,
    vim.api.nvim_list_bufs()
  )

  return vim.tbl_map(vim.api.nvim_buf_get_name, listed_buffers)
end

---Get unique name for the current buffer
---@param filename string
---@param shorten boolean
---@return string
function _M.get_unique_filename(filename, shorten)
  local filenames = vim.tbl_filter(
    function(filename_other) return filename_other ~= filename end,
    _M.get_current_filenames()
  )

  if shorten then
    filename = vim.fn.pathshorten(filename)
    filenames = vim.tbl_map(vim.fn.pathshorten, filenames)
  end

  -- Reverse filenames in order to compare their names
  filename = string.reverse(filename)
  filenames = vim.tbl_map(string.reverse, filenames)

  local index

  -- For every other filename, compare it with the name of the current file char-by-char to
  -- find the minimum index `i` where the i-th character is different for the two filenames
  -- After doing it for every filename, get the maximum value of `i`
  if next(filenames) then
    index = math.max(table.unpack(vim.tbl_map(function(filename_other)
      for i = 1, #filename do
        -- Compare i-th character of both names until they aren't equal
        if filename:sub(i, i) ~= filename_other:sub(i, i) then return i end
      end
      return 1
    end, filenames)))
  else
    index = 1
  end

  -- Iterate backwards (since filename is reversed) until a "/" is found
  -- in order to show a valid file path
  while index <= #filename do
    if filename:sub(index, index) == "/" then
      index = index - 1
      break
    end

    index = index + 1
  end

  return string.reverse(string.sub(filename, 1, index))
end

---Trim a filename
---@param filename string
---@param char_limit number
---@param truncate? string
---@return string
function _M.trim_filename(filename, char_limit, truncate)
  truncate = truncate or " "

  -- Ensure that with the truncation icon, we don't go over the char limit
  if (#filename + #truncate) > char_limit then char_limit = char_limit - #truncate end

  if #filename > char_limit then filename = string.sub(filename, 1, char_limit) .. truncate end

  return filename
end

---Format a filename
---@param filename string
---@param char_limit? number
---@return string
function _M.format_filename(filename, char_limit)
  filename = _M.get_unique_filename(filename, false)

  char_limit = char_limit or 18
  local pad = math.ceil((char_limit - #filename) / 2)
  return string.rep(" ", pad) .. _M.trim_filename(filename, char_limit) .. string.rep(" ", pad)
end

function _M.get_filename(meta)
  local fname
  if type(meta) ~= "table" then meta = {} end
  local nr = meta.bufnr or 0

  fname =
      meta.bufname or
      vim.b[nr].filename or
      vim.api.nvim_buf_get_name(nr) or
      vim.fn.expand"%"

  if conditions.is_man(nr) then
    local head = vim.fn.getbufoneline(nr, 1) or ""
    local s_head = head:match"^[^ ]+"
    local w_head = s_head or fname or ""
    local l_head = w_head:lower()
    fname = l_head:gsub("^([^%(]+)%(([^%)]+)%)", "%1.%2")
  elseif conditions.is_help(nr) then
    fname = vim.fn.fnamemodify(fname, ":t:r")
    -- elseif vim.bo.bt == "nofile" then -- "nofile" actually not always means "empty buffer"
    --   fname = "<empty>"
  else
    if not fname:match"^%." then
      fname = vim.fn.fnamemodify(fname, ":.")
    end
  end
  if not fname or #fname == 0 then
    fname = "<empty>"
  end
  local shorten = meta.shorten or false
  if conditions.width_percent_below(#fname, 0.25) and meta.shorten then --- NOTE: 🤔
    shorten = true
  end

  if not meta.no_uniq then
    fname = _M.get_unique_filename(fname, shorten)
  end

  if meta.no_ext and not conditions.is_special(nr) then
    fname = vim.fn.fnamemodify(fname, ":r")
  end
  return fname
end

function _M.get_file_icon(meta)
  local ret = {}
  local devicons = require"nvim-web-devicons" --- @diagnostic disable-line: different-requires
  -- local fname, ext = vim.fn.expand"%:t", vim.fn.expand"%:e"
  local fname = _M.get_filename{
    no_uniq = true,
    bufnr = meta.bufnr,
    bufname = meta.bufname,
  }
  local ext = fname:match"^.+%.(.+)$" or ""
  ret.icon, ret.iconhl = devicons.get_icon_color(fname, ext, { default = true })
  if ret.icon == nil then
    ret.icon = ""
  end
  return ret.icon, ret.iconhl
end

function _M.get_listed_bufs()
  return vim.tbl_filter(function(bufnr)
    return vim.api.nvim_buf_get_option(bufnr, "buflisted")
  end, vim.api.nvim_list_bufs())
end

return _M
