-- luacheck: globals vim T

local _M = require"heirline.conditions"

function _M.diagnostic_exists()
  return vim.tbl_isempty(vim.lsp.get_clients{ bufnr = 0 })
end
function _M.diags(severity)
  return vim.diagnostic.get(0, { severity = severity })
end

function _M.is_man(bufnr)
  if not bufnr then bufnr = 0 end
  if vim.bo[bufnr].filetype == "man" and vim.fn.exists":Man" == 2 then
    if not vim.b[bufnr].man_keymaps_set then
      _G.map("K", "<cmd>Man<cr>", { buffer = bufnr, silent = true })
      _G.map("<enter>", "<cmd>Man<cr>", { buffer = bufnr, silent = true })
      vim.b[bufnr].man_keymaps_set = true
      vim.bo[bufnr].buflisted = true
    end
    return true
  else
    return false
  end
end

function _M.is_help(bufnr)
  if not bufnr then bufnr = 0 end
  if vim.bo[bufnr].filetype == "help" then
    if not vim.b[bufnr].help_keymaps_set then
      _G.map("<enter>", "K<cr>", { buffer = bufnr, silent = true })
      vim.b[bufnr].help_keymaps_set = true
      vim.bo[bufnr].buflisted = true
    end
    return true
  end
end

function _M.buffer_not_empty(bufnr)
  local nr = bufnr or 0
  local fn = vim.api.nvim_buf_get_name(nr) or vim.fn.expand"%"
  if #vim.fn.fnamemodify(fn, ":t") == 0 then
    return false
  elseif vim.api.nvim_buf_line_count(nr) == 0 then -- reports 1 line even in empty (nofile) buffer 🤷
    return false
  else
    return true
  end
end

function _M.is_special(bufnr)
  local specials = {
    buftype = {
      "nofile",
      "prompt",
      "help",
      "quickfix",
      "terminal",
    },
    filetype = {
      "^aerial$",
      "^alpha$",
      "^chatgpt$",
      "dashboard",
      "^DressingInput$",
      "^frecency$",
      "fugitive",
      "^git.*",
      "harpoon",
      "help",
      "^lazy$",
      "lspinfo",
      "man",
      "neo-tree",
      "^netrw$",
      "oil",
      "^TelescopePrompt$",
      "toggleterm",
      "Trouble",
      "^undotree$",
    },
  }
  return _M.buffer_matches({
    buftype  = specials.buftype,
    filetype = specials.filetype,
  }, bufnr)
end

return _M
