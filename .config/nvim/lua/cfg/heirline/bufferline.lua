-- luacheck: globals vim T

-- local _M = {}

local utils = require"cfg.heirline.utils" --- @diagnostic disable-line: different-requires
local conditions = require"heirline.conditions" --- @diagnostic disable-line: different-requires
local mvaline_colors = require"cfg.heirline.colors.mva"
local file = require"cfg.heirline.blocks.file"
local special = require"cfg.heirline.blocks.special"
local icons = require"cfg.icons"

-- Navigate to buffers with keystrokes
local TablinePicker = {
  condition = function(self) return self._show_picker end,
  special.Space,
  {
    init = function(self)
      local bufname = vim.api.nvim_buf_get_name(self.bufnr)
      bufname = vim.fn.fnamemodify(bufname, ":t")
      local label = bufname:sub(1, 1)
      local i = 2
      while self._picker_labels[label] do
        if i > #bufname then break end
        label = bufname:sub(i, i)
        i = i + 1
      end
      self._picker_labels[label] = self.bufnr
      self.label = label
    end,
    provider = function(self) return self.label .. ": " end,
    hl = { fg = mvaline_colors["InsertModeArrow"].fg, bold = true, italic = true },
  }
}

local TablineBufnr = { --- @diagnostic disable-line: unused-local -- luacheck: ignore
  condition = function(self) return not self._show_picker end,
  provider = function(self) return tostring(self.bufnr) .. ": " end,
  hl = function(self) return { fg = self.is_active and "purple" or "gray", italic = true } end,
}

local TablineFileName = {
  provider = function(self)
    local nr = self.bufnr or 0
    local filename
    filename = utils.get_filename{
      bufname = self.filename,
      bufnr = self.bufnr,
      no_ext = not (conditions.is_man(nr) or conditions.is_help(nr)),
    }
    return filename
  end,
  hl = function(self)
    return {
      fg = self.is_active and mvaline_colors["FileName"].fg or mvaline_colors["Panel"].fg,
      bold = self.is_active or self.is_visible,
    }
  end,
}

local TablineFileArrowPre = {
  {
    condition = function(self)
      return self.is_active or self.is_visible
    end,
    provider = icons.sep.left_heavy,
    hl = {
      bg = mvaline_colors["CurArrow"].fg,
      fg = mvaline_colors["FileName"].bg,
    },
  },
  {
    condition = function(self)
      return not (self.is_active or self.is_visible)
    end,
    special.Space,
  }
}

local TablineFileArrowPost = {
  provider = function(self)
    local icon
    if self.is_active or self.is_visible then
      icon = icons.sep.left_heavy
    else
      icon = icons.sep.left_light
    end
    return ("%s"):format(icon)
  end,
  hl = {
    fg = mvaline_colors["CurArrow"].fg,
    bg = mvaline_colors["FileName"].bg,
  },
}

local TablineFileFlags = {
  {
    condition = function(self) return vim.api.nvim_get_option_value("modified", { buf = self.bufnr, }) end,
    provider = icons.tabs.noclose,
    update = {
      "ModeChanged",
      "BufWritePost",
      "FileWritePost",
      "CursorHold",
      "CursorHoldI",
      "WinNew",
      "WinClosed",
      "BufEnter",
      "BufNew",
    },
    -- hl = {
    --   fg = mvaline_colors["Modified"].fg, -- NOTE: fg colors are irrelevant for colored emojis 🤷
    -- }
  },
  -- {
  --   condition = function(self)
  --     return not vim.api.nvim_buf_get_option(self.bufnr, "modifiable")
  --       or vim.api.nvim_buf_get_option(self.bufnr, "readonly")
  --   end,
  --   provider = function(self)
  --     if vim.api.nvim_buf_get_option(self.bufnr, "buftype") == "terminal" then
  --       return "  "
  --     else
  --       return " "
  --     end
  --   end,
  --   hl = mvaline_colors["isRO"],
  -- },
}

local TablineCloseTabButton = {
  condition = function(self) return not vim.api.nvim_get_option_value("modified", { buf = self.bufnr, }) end,
  update = {
    "ModeChanged",
    "BufWritePost",
    "FileWritePost",
    "CursorHold",
    "CursorHoldI",
    "WinNew",
    "WinClosed",
    "BufEnter",
    "BufNew",
  },
  special.Space,
  {
    provider = icons.tabs.close,
    hl = {
      fg = mvaline_colors["isRO"].fg,
    },
    on_click = {
      callback = function(_, minwid)
        vim.schedule(function()
          vim.api.nvim_buf_delete(minwid, { force = false })
          vim.cmd.redrawtabline()
        end)
      end,
      minwid = function(self)
        return self.bufnr
      end,
      name = "heirline_tabline_close_buffer_callback",
    },
  },
  special.Space,
}

-- local TablineFileNameBlock
local TablineBufferBlock = {
  init = function(self)
    self.filename = vim.b[self.bufnr].filename or vim.api.nvim_buf_get_name(self.bufnr)
    if not self.keymapped then
      vim.keymap.set("n", "<leader>tp", function()
        local tabline = require("heirline").tabline
        local buflist = tabline._buflist[1]
        buflist._picker_labels = {}
        buflist._show_picker = true
        vim.cmd.redrawtabline()
        local char = vim.fn.getcharstr()
        local bufnr = buflist._picker_labels[char]
        if bufnr then
          vim.api.nvim_win_set_buf(0, bufnr)
        end
        buflist._show_picker = false
        vim.cmd.redrawtabline()
      end, { desc = "Call TabLine Picker" })
      self.keymapped = true
    end
  end,
  hl = function(self)
    if self.is_active or self.is_visible then
      return { bg = mvaline_colors["CurArrow"].fg }
    else
      return mvaline_colors["FileName"]
    end
  end,
  condition = function(self)
    local nr = self.bufnr or 0
    if conditions.is_man(nr) or conditions.is_help(nr) then
      return true
    elseif conditions.is_special(nr) then
      return false
    else
      return true
    end
  end,
  update = {
    "WinNew",
    "WinClosed",
    "BufEnter",
    "BufNew",
    "ModeChanged",
    "BufWritePost",
    "FileWritePost",
    "CursorHold",
    "CursorHoldI",
  },
  on_click = {
    callback = function(_, minwid, _, button)
      if button == "m" then -- close on mouse middle click
        vim.api.nvim_buf_delete(minwid, { force = false })
        -- vim.cmd.redrawtabline()
        -- elseif -- menu on right-click
      else
        vim.api.nvim_win_set_buf(0, minwid)
      end
    end,
    minwid = function(self) return self.bufnr end,
    name = "heirline_tabline_buffer_callback",
  },
  TablineFileArrowPre,
  -- TablineBufnr,
  file.FileIcon,

  special.Space,
  TablinePicker,
  TablineFileName,
  TablineFileFlags,
  TablineCloseTabButton,
  TablineFileArrowPost,
}

local buf_cache = {}

-- setup an autocmd that updates the buflist_cache every time that buffers are added/removed
vim.api.nvim_create_autocmd({ "VimEnter", "UIEnter", "BufAdd", "BufDelete" }, {
  callback = function()
    vim.schedule(function()
      local buffers = utils.get_listed_bufs()
      for i, v in ipairs(buffers) do -- fill up cache table with buffers numbers
        buf_cache[i] = v
      end
      for i = #buffers + 1, #buf_cache do -- wipe stale cache
        buf_cache[i] = nil
      end
    end)
  end,
})


local BufferLine = utils.make_buflist(
  TablineBufferBlock,
  { provider = "  ", hl = { fg = mvaline_colors["FileName"].fg } },
  { provider = "  ", hl = { fg = mvaline_colors["FileName"].fg } },
  -- { provider = "  ", hl = { fg = "gray" } },
  -- { provider = "  ", hl = { fg = "gray" } },
  function()
    return buf_cache -- TODO: support for sorting and placing new buffer after current
  end,
  -- don't need cache here, as we manage it in custom function
  false -- buf_cache
)

local BufferLineOffsetLeft = {
  condition = function(self)
    local wins = vim.api.nvim_tabpage_list_wins(0)
    local win = wins[1]
    local bufnr = vim.api.nvim_win_get_buf(win)
    self.winid = win

    if vim.bo[bufnr].filetype == "neo-tree" then
      self.title = "NeoTree"
      return true
    elseif vim.bo[bufnr].filetype == "aerial" then
      self.title = "Aerial"
      return true
    end
  end,
  provider = function(self)
    local title = self.title
    local width = vim.api.nvim_win_get_width(self.winid)
    local pad = math.ceil((width - #title) / 2)
    return string.rep(" ", pad) .. title .. string.rep(" ", pad)
  end,
  hl = function(self)
    if vim.api.nvim_get_current_win() == self.winid then
      return {
        bg = mvaline_colors["Panel"].bg,
        fg = mvaline_colors["ReplaceMode"].fg,
        bold = true
      }
    else
      return "Panel"
    end
  end,
  update = {
    "WinNew",
    "WinClosed",
    "BufEnter",
    "BufNew",
  },
}
local BufferLineOffsetRight = {
  condition = function(self)
    local wins = vim.api.nvim_tabpage_list_wins(0)
    local win = wins[#wins]
    local bufnr = vim.api.nvim_win_get_buf(win)
    self.winid = win

    if vim.bo[bufnr].filetype == "neo-tree" then
      self.title = "NeoTree"
      return true
    elseif vim.bo[bufnr].filetype == "aerial" then
      self.title = "Aerial"
      return true
    end
    -- print("mooooooooo")
  end,
  provider = function(self)
    local title = self.title
    local width = vim.api.nvim_win_get_width(self.winid)
    local pad = math.ceil((width - #title) / 2)
    return string.rep(" ", pad) .. title .. string.rep(" ", pad)
  end,
  hl = function(self)
    if vim.api.nvim_get_current_win() == self.winid then
      return {
        bg = mvaline_colors["Panel"].bg,
        fg = mvaline_colors["ReplaceMode"].fg,
        bold = true
      }
    else
      return "Panel"
    end
  end,
  update = {
    "WinNew",
    "WinClosed",
    "BufEnter",
    "BufNew",
  },
}

local VimLogo = {
  provider = (" %s "):format(icons.other.vim),
  hl = {
    fg = mvaline_colors["InsertMode"].fg,
    bg = "DarkGreen",
  },
}
local VimLogoArrow = {
  provider = icons.sep.left_heavy,
  hl = {
    fg = "DarkGreen",
    bg = mvaline_colors["FileName"].bg,
  },
}

return {
  hl = mvaline_colors["Panel"],
  BufferLineOffsetLeft,
  VimLogo,
  VimLogoArrow,
  special.Space,
  BufferLine,
  special.Align,
  { provider = icons.sep.right_heavy, hl = { fg = mvaline_colors["Panel"].fg, }, },
  {
    provider = function()
      local loaded_bufs = vim.tbl_filter(
        function(bufnr)
          return vim.api.nvim_buf_is_loaded(bufnr) and vim.bo[bufnr].buflisted
        end,
        vim.api.nvim_list_bufs()
      )
      local listed_bufs = vim.tbl_filter(
        function(bufnr)
          return vim.bo[bufnr].buflisted
        end,
        vim.api.nvim_list_bufs()
      )
      return (" %d (%d) "):format( -- TODO: beauty formatting, logic(?)
        #listed_bufs,
        #loaded_bufs
      )
    end,
    hl = {
      bg = mvaline_colors["Panel"].fg,
      fg = mvaline_colors["InsertMode"].fg,
    },
  },
  BufferLineOffsetRight,
  -- require"cfg.heirline.tabpages",
}
