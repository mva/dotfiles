-- luacheck: globals vim

local heirline = require"heirline"
local conditions = require"cfg.heirline.conditions" --- @diagnostic disable-line: different-requires
-- local utils = require"heirline.utils"

local mode = require"cfg.heirline.blocks.mode"
local special = require"cfg.heirline.blocks.special"
local file = require"cfg.heirline.blocks.file"
local right = require"cfg.heirline.blocks.right"
local mvaline_colors = require"cfg.heirline.colors.mva"

-------------------------------------------------------------------------------------------------------
local DefaultStatusline = {
  condition = function(self)
    return not conditions.is_special(self.bufnr or 0)
  end,
  mode.ModeBlk,
  file.FileBlk,
  special.Split,
  file.GitBlk,
  special.Space,
  file.DiffBlk,
  special.Align,
  right.RightBlk,
}

local InactiveStatusline = {
  condition = conditions.is_not_active,
  file.FileBlk,
  { provider = function() return "moo" end, },
  -- Align,
}

local SpecialStatusline = {
  condition = function(self)
    return conditions.is_special(self.bufnr or 0)
  end,
  mode.SpecialModeBlk,
  file.FileBlk,

  special.Align,
  right.RightBlock,
}

local TerminalStatusline = {
  condition = function()
    return conditions.buffer_matches{ buftype = { "terminal" } }
  end,
  { condition = conditions.is_active, mode.ModeBlk, },
  file.FileBlockEndSep,
  file.TerminalName,
  special.Split,
  special.Align,
}

local StatusLines = {
  -- hl = function()
  --   if conditions.is_active() then
  --     return "StatusLine"
  --   else
  --     return "StatusLineNC"
  --   end
  -- end,
  -- the first statusline with no condition, or which condition returns true is used.
  -- think of it as a switch case with breaks to stop fallthrough.
  fallthrough = false,
  hl = mvaline_colors["Panel"],
  SpecialStatusline,
  TerminalStatusline,
  InactiveStatusline,
  DefaultStatusline,
}

--[[
local statusline = {""}
local winbar = {""}
local tabline = {""}
--]]



-- heirline.load_colors(mvaline_colors)

-- heirline.setup(statusline, winbar, tabline)
heirline.setup{
  statusline = StatusLines,
  statuscolumn = require"cfg.heirline.statuscolumn",
  -- -- winbar = require"cfg.heirline.winbar",
  winbar = require"cfg.heirline.dropbar",
  tabline = require"cfg.heirline.bufferline",
  opts = {
    -- if the callback returns true, the winbar will be disabled for that window
    -- the args parameter corresponds to the table argument passed to autocommand callbacks.
    -- :h nvim_lua_create_autocmd()
    disable_winbar_cb = function(args)
      return conditions.is_special(args.buf)
    end,
  },
}

-- Yep, with heirline we're driving manual!
vim.o.showtabline = 2
vim.cmd([[au FileType * if index(['wipe', 'delete'], &bufhidden) >= 0 | set nobuflisted | endif]])
