-- luacheck: globals nmap

local dap_go = require"dap-go"
dap_go.setup()


nmap("<Leader>td", dap_go.debug_test, { silent = true })
