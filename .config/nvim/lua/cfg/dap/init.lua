-- luacheck: no max line length
-- luacheck: globals vim nmap
-- luacheck: globals nnoremap

local dap = require"dap"
--[[
]]
-- local api = vim.api
-- local keymap_restore = {}
-- dap.listeners.after['event_initialized']['me'] = function()
--   for _, buf in pairs(api.nvim_list_bufs()) do
--     local keymaps = api.nvim_buf_get_keymap(buf, 'n')
--     for _, keymap in pairs(keymaps) do
--       if keymap.lhs == "K" then
--         table.insert(keymap_restore, keymap)
--         api.nvim_buf_del_keymap(buf, 'n', 'K')
--       end
--     end
--   end
--   -- nmap('K', require"dap.ui.widgets".hover(), { silent = true })
-- end

-- dap.listeners.after['event_terminated']['me'] = function()
--   for _, keymap in pairs(keymap_restore) do
--     api.nvim_buf_set_keymap(
--       keymap.buffer,
--       keymap.mode,
--       keymap.lhs,
--       keymap.rhs,
--       { silent = keymap.silent == 1 }
--     )
--   end
--   keymap_restore = {}
-- end

-- dap.configurations.cpp = {
--     {
--       -- If you get an "Operation not permitted" error using this, try disabling YAMA:
--       --  echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
--       name = "Attach to process",
--       type = 'cpp',  -- Adjust this to match your adapter name (`dap.adapters.<name>`)
--       request = 'attach',
--       pid = require('dap.utils').pick_process,
--       args = {},
--     },
-- }
--[[
dap.configurations.python = {
  {
    type = 'python';
    request = 'launch';
    name = "Launch file";
    program = "${file}";
    pythonPath = function()
      return '/usr/bin/python'
    end;
  },
} ]]

nnoremap("<Leader>dB", function() dap.set_breakpoint(vim.fn.input('Breakpoint condition: ')) end,
  { silent = true, desc = "Breakpoint Condition", })
nnoremap("<Leader>db", dap.toggle_breakpoint, { silent = true, desc = "Toggle Breakpoint", })
nnoremap("<Leader>dc", dap.continue, { silent = true, desc = "Continue", })
nnoremap("<Leader>dC", dap.run_to_cursor, { silent = true, desc = "Run to cursor", })
nnoremap("<leader>dg", dap.goto_, { silent = true, desc = "Go to line (no execute)" })
nnoremap("<leader>di", dap.step_into, { silent = true, desc = "Step Into" })
nnoremap("<leader>dj", dap.down, { silent = true, desc = "Down" })
nnoremap("<leader>dk", dap.up, { silent = true, desc = "Up" })
nnoremap("<Leader>dl", dap.run_last, { silent = true, desc = "Run last", })
nnoremap("<Leader>do", dap.step_out, { silent = true, desc = "Step Out", })
nnoremap("<Leader>dO", dap.step_over, { silent = true, desc = "Step Over", })
nnoremap("<Leader>dp", dap.pause, { silent = true, desc = "Pause", })
nnoremap("<Leader>dr", dap.repl.open, { silent = true, desc = "Repl", })
nnoremap("<Leader>ds", dap.session, { silent = true, desc = "Session", })
nnoremap("<Leader>dt", dap.terminate, { silent = true, desc = "Terminate", })
nnoremap("<Leader>dw", require"dap.ui.widgets".hover, { silent = true, desc = "Widgets" })
-- nnoremap("<Leader>dpl", function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end,
--     { silent = true, desc = "Breakpoint log poit message" })

vim.api.nvim_set_hl(0, "DapStoppedLine", { default = true, link = "Visual" })

for name, sign in pairs(require"cfg.icons".dap) do
  sign = type(sign) == "table" and sign or { sign }
  vim.fn.sign_define(
    "Dap" .. name,
    { text = sign[1], texthl = sign[2] or "DiagnosticInfo", linehl = sign[3], numhl = sign[3] }
  )
end

local wk = require"which-key"

wk.add{
  { "<leader>d",  group = "debug" },
  { "<leader>da", group = "adapters" },
}
