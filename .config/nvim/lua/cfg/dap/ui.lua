-- luacheck: globals req
local dapui = require"dapui"

dapui.setup{
  icons = { expanded = "▾", collapsed = "▸" },
  mappings = {
    -- Use a table to apply multiple mappings
    expand = { "<CR>", "<2-LeftMouse>" },
    open = "o",
    remove = "d",
    edit = "e",
    repl = "r",
  },
  layouts = {
    {
      elements = {
        {
          id = "scopes",
          size = 0.25, -- Can be float or integer > 1
        },
        { id = "breakpoints", size = 0.25 },
        { id = "stacks",      size = 0.25 },
        { id = "watches",     size = 00.25 },
      },
      size = 40,
      position = 'left',
    },
    {
      elements = {
        'repl',
        'console',
      },
      size = 10,
      position = 'bottom',
    },
  },
  floating = {
    max_height = nil,  -- These can be integers or a float between 0 and 1.
    max_width = nil,   -- Floats will be treated as percentage of your screen.
    border = "single", -- Border style. Can be "single", "double" or "rounded"
    mappings = {
      close = { "q", "<Esc>" },
    },
  },
  windows = { indent = 1 },
}

-- luacheck: globals vnoremap nmap map
vnoremap("<leader>de", dapui.eval, { desc = "Eval" })
map("<leader>du", dapui.toggle, { desc = "Dap UI" })

-- require("dapui").open()
-- require("dapui").close()
-- require("dapui").toggle()
local dap = require"dap"

dap.listeners.after.event_initialized["dapui_config"] = function()
  dapui.open({})
end
dap.listeners.before.event_terminated["dapui_config"] = function()
  dapui.close({})
end
dap.listeners.before.event_exited["dapui_config"] = function()
  dapui.close({})
end



-- require("dapui").float_element(<element ID>, <optional settings>) -- width: number , height: number , enter: boolean
-- require("dapui").eval(<expression>)

-- vnoremap <M-k> <Cmd>lua require("dapui").eval()<CR>
