local augend = require("dial.augend")
require("dial.config").augends:register_group({
  default = {
    augend.constant.new({ -- Yes/No
      elements = { "Yes", "No" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- yes/no
      elements = { "yes", "no" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- Y/N
      elements = { "Y", "N" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- y/n
      elements = { "y", "n" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- True/False
      elements = { "True", "False" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- Enable/Disable
      elements = { "Enable", "Disable" },
      word = false,
      cyclic = true,
    }),
    augend.constant.new({ -- enable/disable
      elements = { "enable", "disable" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- public/private
      elements = { "public", "private" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- *= / /=
      elements = { "*=", "/=" },
      word = false,
      cyclic = true,
    }),
    augend.constant.new({ -- += / -=
      elements = { "+=", "-=" },
      word = false,
      cyclic = true,
    }),
    augend.constant.new({ -- == / !=
      elements = { "==", "!=" },
      word = false,
      cyclic = true,
    }),
    augend.constant.new({ -- and/or
      elements = { "and", "or" },
      word = true,
      cyclic = true,
    }),
    augend.constant.new({ -- || / &&
      elements = { "||", "&&" },
      word = false,
      cyclic = true,
    }),
    augend.constant.new({ -- [ ] / [x]
      elements = { "[ ]", "[x]" },
      word = false,
      cyclic = true,
    }),
    augend.hexcolor.new({
      -- case = "lower",
    }),
    augend.integer.alias.decimal,
    augend.integer.alias.hex,
    augend.constant.alias.alpha,
    augend.constant.alias.Alpha,
    augend.constant.alias.bool,
    augend.date.new{ -- Y/m/d
      pattern = "%Y/%m/%d",
      default_kind = "day",
      only_valid = true,
      word = false,
    },
    augend.date.new{ -- d/m/Y
      pattern = "%d/%m/%Y",
      default_kind = "year",
      only_valid = true,
      word = false,
    },
    augend.date.alias["%H:%M"],
    augend.date.alias["%H:%M:%S"],
  },
})
