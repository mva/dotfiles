local f = require"lib.funcs"
local u = f.u

local icons = {
  dap = {
    Stopped = {
      -- " ",
      " ",
      "DiagnosticWarn",
      "DapStoppedLine"
    },
    Breakpoint = " ",
    BreakpointCondition = " ",
    BreakpointRejected = { " ", "DiagnosticError" },
    LogPoint = ".>",
  },
  diagnostics = {
    Error = " ",
    -- Error = " ",
    -- Error = "✗ ",
    Warn = " ",
    -- Warn = "⚠ ",
    Hint = " ",
    -- Hint = " ",
    -- Hint = " ",
    -- Hint = " ",
    -- Hint = " ",
    -- Hint = "➤ ",
    Info = " ",
    -- Info = " ",
  },
  folders = {
    closed   = " ",
    -- Folder= " ",
    open     = " ", -- 📂
  },
  indent = {
    top           = "│ ",
    middle        = "├╴",
    last          = "└╴",
    -- last       = "╰╴", -- rounded
    fold_open     = " ",
    fold_closed   = " ",
    ws            = "  ",
  },
  git = {
    -- add      = u'2795', -- ➕  -- '   ',
    -- del      = u'2796', -- ➖ -- '   ',
    -- mod      = u'2797', -- ➗ -- '   ',
    added = " ",
    changed = " ",
    removed = " ",
  },
  kinds = {
    Array = " ",
    Boolean       = "󰨙 ",
    -- Boolean = " ",
    Class = " ",
    -- Class = "ﴯ",
    Color = " ",
    -- Color = "",
    Constant      = "󰏿 ",
    -- Constant = " ",
    Constructor   = " ",
    -- Constructor = " ",
    Copilot = " ",
    Enum          = " ",
    EnumMember    = " ",
    -- Enum = " ",
    -- EnumMember = " ",
    Event = " ",
    -- Event = "",
    Field         = " ",
    -- Field = " ",
    -- Field = "",
    File = " ",
    -- File = "",
    Folder = " ",
    -- Folder = " ",
    Function      = "󰊕 ",
    -- Function = " ",
    Interface     = " ",
    -- Interface = " ",
    Key = " ",
    Keyword = " ",
    -- Keyword = "",
    Method        = "󰊕 ",
    -- Method = " ",
    Module        = " ",
    -- Module = " ",
    Namespace     = "󰦮 ",
    -- Namespace = " ",
    Null = " ",
    Number        = "󰎠 ",
    -- Number = " ",
    Object = " ",
    Operator = " ",
    -- Operator = "",
    Package       = " ",
    -- Package = " ",
    Property      = " ",
    -- Property = "ﰠ",
    -- Property = " ",
    Reference = " ",
    -- Reference = "",
    -- Reference = "",
    Snippet = " ",
    -- Snippet = "",
    String        = " ",
    -- String = " ",
    Struct        = "󰆼 ",
    -- Struct = " ",
    -- Struct = "",
    -- Struct = "פּ",
    Text = " ",
    -- Text = "",
    -- Text = "",
    TypeParameter = " ",
    -- TypeParameter = ""
    Unit = " ",
    -- Unit = "",
    Value = " ",
    -- Value = " ",
    Variable      = "󰀫 ",
    -- Variable = " ",
    -- Variable = "",
    -- Variable = "",
  },
  sep = {
    left_heavy  = u'e0b0',  -- nr2char(57520), --  .
    -- nr2char(57520+(4*1)),
    left_light  = u'e0b1',  -- nr2char(57521), --  .
    -- nr2char(57521+(4*1)), --
    right_heavy = u'e0b2',  -- nr2char(57522), --  .
    -- nr2char(57522+(4*1)), --
    right_light = u'e0b3',  -- nr2char(57523), --  .
    -- nr2char(57523+(4*1)), --
  },
  position = {
    lnum = u'e0a1',         -- nr2char(57505), --  .
    cnum = u'e0a3',         -- nr2char(57507), --  .
  },
  fileinfo = {
    -- ro      = u'1f50f',      -- nr2char(128271), -- 🔏
    -- ro   = nr2char(57506), --  .
    ro = u'f023', --  
    saved = "",
    -- saved   = '🖬',
    -- u'f693', --  .
    -- saved        = u'2714', -- nr2char(10004), -- ✔ .
    -- saved   = vim.fn.nr2char(10003), -- ✓ .
    -- unsaved      = u'274c', -- nr2char(10060), -- ❌ .
    -- unsaved = '🖬',
    unsaved = '',
    -- --u'f693', --  .
    -- unsaved  = nr2char(10007), -- ✗ .
  },
  other = {
    date = u'1f4c5',         -- nr2char(8986),  -- 📅
    time = '⏰', -- u'231a',          -- nr2char(8986),  -- ⌚ -- ⏳ ⏱️ ⏰ 
    vim = "",
  },
  vcs = {
    vcs       = u'e0a0',    -- nr2char(57504), --  .
    --  .
    git       = u'e0a0', -- u'f1d3',    --  .
    -- git          = nr2char(177),   -- ± .
    github    = u'f113',    --   .
    gitlab    = u'f296',    --   .
    bitbucket = u'f171',    --  .
    hg        = u'f0c3',    --  .
    -- hg           = nr2char(9791),  -- ☿ .
    svn       = u'f28a',    --  .
    -- svn          = u'e72d', -- 
  },
  nl = {
    dos  = u'e70f',         --  .
    unix = u'f17c',         --  .
    mac  = u'f179',         --  .
  },
  lsp = {
    status = u'f0e8',   --  .
    -- status   = u'f816', --  .
    -- status   = u'f02d', --  .
  },
  lazy = {
    -- require = "🌙",
    cmd = " ",
    -- cmd = "⌘",
    -- config = "",
    config = "🛠 ",
    event = "",
    -- event = "📅",
    ft = " ",
    -- ft = "📂",
    -- init = " ",
    init = "⚙",
    import = " ",
    keys = " ",
    -- keys = "🗝 ",
    -- lazy = "󰒲 ",
    lazy = "💤 ",
    loaded = "●",
    not_loaded = "○",
    plugin = " ",
    -- plugin = "🔌",
    runtime = " ",
    -- runtime = "💻",
    source = " ",
    -- source = "📄",
    -- start = "",
    start = "🚀",
    -- task = "✔ ",
    task = "📌",
    list = {
      "●",
      "➜",
      "★",
      "‒",
    },
  },
  tabs = {
    -- noclose = " ",
    -- noclose = " ",
    noclose = " ✏ ",
    close = "",
    -- close = " ",
    -- close = "",
    -- close = "",
    -- close = "×",
    -- close = "❌",
  },
  noice = {
    cmdline = "",
    search = {
      down = " ",
      up = " ",
    },
    filter = "", -- "$",
    lua = "",
    help = "󰋖",
    input = "󰥻",
  },
  todo = {
    FIX = "",
    TODO = "",
    HACK = "",
    WARN = "",
    PERF = "󰅒",
    NOTE = "󰍨", -- 󱜾 ?
    TEST = "⏲",
  },
}

return icons
