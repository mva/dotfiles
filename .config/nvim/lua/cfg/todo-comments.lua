local i = require"cfg.icons".todo
return {
  keywords = {
    FIX = {
      icon = i.FIX, -- icon used for the sign, and in search results
      color = "error", -- can be a hex color, or a named color (see below)
      alt = { "FIXME", "BUG", "FIXIT", "ISSUE", "ERROR", },
      -- a set of other keywords that all map to this FIX keywords
      -- signs = false, -- configure signs for some keywords individually
    },
    --- FIX:
    TODO = { icon = i.TODO, color = "info" },
    --- TODO:
    HACK = { icon = i.HACK, color = "warning" },
    --- HACK:
    WARN = { icon = i.WARN, color = "warning", alt = { "WARNING", "XXX" } },
    --- WARN:
    --- WARNING:
    PERF = { icon = i.PERF, alt = { "OPTIM", "PERFORMANCE", "OPTIMIZE" } },
    --- PERF:
    NOTE = { icon = i.NOTE, color = "hint", alt = { "INFO" } }, --  ?
    --- NOTE:
    TEST = { icon = i.TEST, color = "test", alt = { "TESTING", "PASSED", "FAILED" } },
    -- TEST:
  },
}
