-- luacheck: globals vim

local mason_lsp = require"mason-lspconfig"

local function E(list)
  local ret = {}
  local ls_bins = {
    clangd   = "clangd",
    ltex     = "ltex-ls",
    ccls     = "ccls",
    gopls    = "gopls",
    pylsp    = "pylsp",
    html     = "vscode-html-language-server",
    jsonls   = "vscode-json-language-server",
    cssls    = "vscode-css-language-server",
    eslint   = "vscode-eslint-language-server",
    lua_ls   = "lua-language-server",
    markdown = "vscode-markdown-language-server",
    lua      = "lua-lsp",
    yamlls   = "yaml-language-server",
    bashls   = "bash-language-server",
    dockerls = "docker-langserver",
    cmake    = "cmake-language-server",
    qml      = "qml-lsp",
  }
  for _, srv in ipairs(list) do
    local ex = io.popen(("which %s 2>/dev/null"):format(ls_bins[srv])):read()
    -- print(srv..":"..(tostring(ex)),"\n")
    if ex then
      table.insert(ret, srv)
    end
  end
  --  print(vim.inspect(ret))
  return ret
end

local ensure

if _G.NVIM_HACKS["install:mason"] then
  ensure = true
end

if mason_lsp and mason_lsp.setup then
  mason_lsp.setup{
    -- automatic_installation = true,
    -- automatically detect which servers to install (based on which servers are set up via lspconfig)
    automatic_installation = ensure and {
      exclude = E(require"cfg.lsp.servers"),
    },
    ui = {
      icons = { -- TODO: bigger (bold) icons
        server_installed = "✅",
        server_uninstalled = "❌",
        server_pending = "🔄",
      }
    }
  }
  --  mason_lsp.setup_handlers(require"cfg.lsp.setups")
end
