-- luacheck: globals req
require"mason".setup({
  --  ensure_installed = {
  --    "stylua", -- TODO: systemwide, need ebuild
  --    "shellcheck", -- TODO: systemwide, has ebuild, but either binary or need haskell
  --    "shfmt", -- TODO: systemwide, need ebuild
  --    -- "flake8", -- systemwide
  --  },
  ui = {
    icons = {
      server_installed = "✅",
      server_uninstalled = "❌",
      server_pending = "🔄",
    },
    keymaps = { apply_language_filter = "<C-f>", },
    border = "double",
  },
})
