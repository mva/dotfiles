-- luacheck: globals vim T luasnip, no max line length

-- local plugins = _G.packer_plugins or {}

local cmp = require"cmp"
local luasnip = require"cfg.luasnip"
local lspkind = require"lspkind"
-- local cmp_buffer = require"cmp_buffer"
local hi = vim.api.nvim_set_hl

-- [ Colors (higllight) ] {{{
--[[ hi(0, "CmpItemAbbrDeprecated", { bg = "NONE", strikethrough=1, fg = "#808080", })
hi(0, "CmpItemAbbrMatch", { bg = "NONE", fg = "#569CD6", })
hi(0, "CmpItemAbbrMatchFuzzy", { link = "CmpItemAbbrMatch", })
hi(0, "CmpItemKindVariable", { bg = "NONE", fg = "#9CDCFE", })
hi(0, "CmpItemKindInterface", { link = "CmpItemKindVariable", })
hi(0, "CmpItemKindText", { link = "CmpItemKindVariable", })
hi(0, "CmpItemKindFunction", { bg = "NONE", fg = "#C586C0", })
hi(0, "CmpItemKindMethod", { link = "CmpItemKindFunction", })
hi(0, "CmpItemKindKeyword", { bg = "NONE", fg = "#D4D4D4", })
hi(0, "CmpItemKindProperty", { link = "CmpItemKindKeyword", })
hi(0, "CmpItemKindUnit", { link = "CmpItemKindKeyword" }) ]]
local cmp_hls = {
  CmpItemAbbrDeprecated = { fg = "#7E8294", bg = "NONE", strikethrough = 1, },
  CmpItemAbbrMatch = { fg = "#82AAFF", bg = "NONE", bold = 1, },
  CmpItemAbbrMatchFuzzy = { fg = "#82AAFF", bg = "NONE", bold = 1, },
  CmpItemMenu = { fg = "#C792EA", bg = "NONE", italic = 1, bold = 1, },

  CmpItemKindField = { bold = 1, fg = "#B5585F" },
  CmpItemKindProperty = { bold = 1, fg = "#B5585F" },
  CmpItemKindEvent = { bold = 1, fg = "#B5585F" },

  CmpItemKindText = { bold = 1, fg = "#9FBD73" },
  CmpItemKindEnum = { bold = 1, fg = "#9FBD73" },
  CmpItemKindKeyword = { bold = 1, fg = "#9FBD73" },

  CmpItemKindConstant = { bold = 1, fg = "#D4BB6C" },
  CmpItemKindConstructor = { bold = 1, fg = "#D4BB6C" },
  CmpItemKindReference = { bold = 1, fg = "#D4BB6C" },

  CmpItemKindFunction = { bold = 1, fg = "#A377BF" },
  CmpItemKindStruct = { bold = 1, fg = "#A377BF" },
  CmpItemKindClass = { bold = 1, fg = "#A377BF" },
  CmpItemKindModule = { bold = 1, fg = "#A377BF" },
  CmpItemKindOperator = { bold = 1, fg = "#A377BF" },

  CmpItemKindVariable = { bold = 1, fg = "#7E8294" },
  CmpItemKindFile = { bold = 1, fg = "#7E8294" },

  CmpItemKindUnit = { bold = 1, fg = "#D4A959" },
  CmpItemKindSnippet = { bold = 1, fg = "#D4A959" },
  CmpItemKindFolder = { bold = 1, fg = "#D4A959" },

  CmpItemKindMethod = { bold = 1, fg = "#6C8ED4" },
  CmpItemKindValue = { bold = 1, fg = "#6C8ED4" },
  CmpItemKindEnumMember = { bold = 1, fg = "#6C8ED4" },

  CmpItemKindInterface = { bold = 1, fg = "#58B5A8" },
  CmpItemKindColor = { bold = 1, fg = "#58B5A8" },
  CmpItemKindTypeParameter = { bold = 1, fg = "#58B5A8" },
}

for k, v in pairs(cmp_hls) do hi(0, k, v) end

-- }}}

local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match('%s') == nil
end

--[=[
--]=]
local mapping = {
  ["<Tab>"] = cmp.mapping({
    c = function(fallback)
      if cmp.visible() then
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        else
          cmp.select_next_item()
          -- cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
        end
      elseif has_words_before() then
        cmp.complete()
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        else
          cmp.select_next_item()
        end
      else
        if fallback then fallback() end
      end
    end,
    i = function(fallback)
      if cmp.visible() then
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        else
          cmp.select_next_item()
          -- cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
        end
        -- XXX: Maybe replace the expand_or_jumpable() calls with expand_or_locally_jumpable()
        -- that way it will only jump inside the snippet region
      elseif luasnip.expand_or_jumpable() then
        -- elseif has_words_before() and luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        end
      else
        if fallback then fallback() end
      end
    end,
    s = function(fallback)
      if cmp.visible() then
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        else
          cmp.select_next_item()
          -- cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
        end
        -- XXX: Maybe replace the expand_or_jumpable() calls with expand_or_locally_jumpable()
        -- that way it will only jump inside the snippet region
      elseif luasnip.expand_or_jumpable() then
        -- elseif has_words_before() and luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
        if #cmp.get_entries() == 1 then
          cmp.confirm({ select = true })
        end
      else
        if fallback then fallback() end
      end
    end
  }),
  ["<S-Tab>"] = cmp.mapping({
    c = function()
      if cmp.visible() then
        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
      else
        cmp.complete()
      end
    end,
    i = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        if fallback then fallback() end
      end
    end,
    s = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        if fallback then fallback() end
      end
    end
  }),
  --      ['<Down>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
  --      ['<Up>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
  ['<Down>'] = cmp.mapping({
    c = function()
      if cmp.visible() then
        cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
      else
        vim.api.nvim_feedkeys(t('<Down>'), 'n', true)
      end
    end,
    i = function(fallback)
      if cmp.visible() then
        cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
      else
        if fallback then fallback() end
      end
    end
  }),
  ['<Up>'] = cmp.mapping({
    c = function()
      if cmp.visible() then
        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
      else
        vim.api.nvim_feedkeys(t('<Up>'), 'n', true)
      end
    end,
    i = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
      else
        if fallback then fallback() end
      end
    end
  }),
  ['<Right>'] = cmp.mapping({
    -- i = function(fallback)
    --   --[[ if cmp.visible() and has_words_before() and luasnip.expand_or_jumpable() then
    --     vim.api.nvim_feedkeys(t'<Plug>luasnip-expand-or-jump', '', true)
    --   else ]]
    --     fallback()
    --   -- end
    -- end,
    s = function(fallback)
      --if has_words_before() and luasnip.expand_or_jumpable() then
      if luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        if fallback then fallback() end
      end
    end,
  }),
  ['<Left>'] = cmp.mapping({
    -- i = function(fallback)
    --   --[[ if luasnip.jumpable(-1) then
    --     vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Plug>luasnip-jump-prev', true, true, true), '', true)
    --   else ]]
    --     fallback()
    --   -- end
    -- end,
    s = function(fallback)
      if luasnip.jumpable(-1) then
        luasnip.jump(-1)
        --vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Plug>luasnip-jump-prev', true, true, true), '', true)
      else
        if fallback then fallback() end
      end
    end,
  }),
  ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
  ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
  --      ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), {'i', 'c'}),
  ['<C-e>'] = cmp.mapping({ i = cmp.mapping.close(), c = cmp.mapping.close() }),
  ['<Esc>'] = cmp.mapping({
    i = function(fallback)
      if cmp.visible() then
        cmp.mapping.close()(fallback)
      else
        if fallback then fallback() end
      end
    end,
    c = function(_fallback)
      if cmp.visible() then
        cmp.mapping.close()(function()
          vim.api.nvim_feedkeys(t'<C-c>', "", true)
        end)
      else
        vim.api.nvim_feedkeys(t'<C-c>', "", true)
        -- fallback() somewhy executes commandline
      end
    end,
  }),
  ['<CR>'] = cmp.mapping({
    i = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false }),
    c = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false }),
    s = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false }),
    -- c = function(fallback)
    --   if cmp.visible() then
    --     cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
    --   else
    --     fallback()
    --   end
    -- end
  }),
  --[[
  ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
  ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
  ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
  ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
  ['<C-e>'] = cmp.mapping({
    i = cmp.mapping.abort(),
    c = cmp.mapping.close(),
  }),
  ['<CR>'] = cmp.mapping.confirm({ select = true }),
  --]]
}

cmp.setup{
  completion = {
    -- autocomplete = false,
    -- completeopt = "menu,menuone,noinsert",
  },
  experimental = {
    -- ghost_text = { --- preview of text to be added
    --   hl_group = "TodoBgTEST", --- TODO: replace with direct highlight group, instead of link
    -- },
    ghost_text = false, --- TODO: check how to make it appear only after I select completion variant, and not just randomly
    custom_menu = false,
    menu = false,
  },
  view = {
    docs = {
      auto_open = true,
    },
    entries = {
      -- name = 'native', --- BUG: totally broken: invisible and SLOOOOW
      name = 'custom',
      -- selection_order = 'near_cursor' --- NOTE: not comfortable when reversed 🤷
    },
  },
  window = {
    documentation = {
      -- border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" }, -- NOTE: default?
      border = "rounded",
      winhighlight = 'Normal:Normal,FloatBorder:TelescopeBorder,Search:None', --CursorLine:xxx
    },
  },
  formatting = {
    border = "rounded",
    -- fields = { "kind", "abbr" },
    -- fields = { "abbr", "kind", ... },
    format = lspkind.cmp_format({
      preset = "default", -- codicons
      mode = "text_symbol", -- symbol, symbol_text, text_symbol, text
      maxwidth = 80,
      symbol_map = require"cfg.icons".kinds,
      menu = {
        fuzzy_buffer = "[Buffer(fz)]",
        buffer = "[Buffer]",
        nvim_lsp = "[LSP]",
        luasnip = "[LuaSnip]",
        ultisnips = "[UltiSnips]",
        nvim_lua = "[Lua]",
        latex_symbols = "[LaTeX]",
        path = "[Path]",
        async_path = "[Path]",
        calc = "[Calc]",
        cmdline = "[CmdLine]",
        cmdline_history = "[CmdLine(hst)]",
        emoji = "[Emoji]",
        tmux = "[Tmux]",
        rg = "[RG]",
        zsh = "[ZSH]",
        treesitter = "[TS]",
        nvim_lsp_signature_help = "[LSP-H]",
        ["diag-codes"] = "[DiagCode]",
      },
    }),
    --[[ no-lspkind way:
    format = function(entry, vim_item)
      vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)
      vim_item.menu = ({buffer = "[Buffer]", nvim_lsp = "[LSP]", luasnip = "[LuaSnip]", nvim_lua = "[Lua]", latex_symbols = "[LaTeX]",})[entry.source.name]
      return vim_item
    end
    --]]
  },
  sorting = {
    comparators = {
      -- require('cmp_fuzzy_buffer.compare'),
      cmp.config.compare.offset,
      cmp.config.compare.exact,
      -- function(...) return cmp_buffer:compare_locality(...) end,
      -- cmp.config.compare.sort_text,
      cmp.config.compare.score,
      require"cmp-under-comparator".under,
      cmp.config.compare.recently_used,
      cmp.config.compare.kind,
      cmp.config.compare.sort_text,
      -- cmp.config.compare.recently_used,
      cmp.config.compare.length,
      cmp.config.compare.order,
    },
  },
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  sources = {
    { name = "diag-codes",
      -- default completion available only in comment context
      -- use false if you want to get in other context
      option = { in_comment = true },
      group_index = 1,
    },
    { name = 'luasnip',
      group_index = 1,
    },
    -- { name = 'luasnip_choice', },
    { name = "nvim_lua", -- move to ft?
      group_index = 1,
    },
    { name = 'treesitter',
      group_index = 1,
    },
    { name = 'nvim_lsp',
      group_index = 1,
    },
    { name = "nvim_lsp_signature_help",
      group_index = 1,
    },
    -- { name = "calc", },
    { name = "emoji",
      -- option = { insert = false, },
      group_index = 9, -- not interfere with another sources
    },
    -- { name = "async_path",
    { name = "path",
      -- max_item_count = 20,
      group_index = 1,
      option = {
        trailing_slash = true,
        -- get_cwd = ...,
      },
    },
    { name = "rg",
      group_index = 1,
      keyword_length = 3,
      option = {
        additional_arguments = (function()
          if vim.fn.getcwd() == vim.env.HOME then
            return "--max-depth 1"
          end
        end)()
      },
    },
    { name = 'tmux',
      group_index = 9, -- 🤔 maybe give it a chance?
      option = {
        all_panes = false,
        label = '[tmux]',
        trigger_characters = { '.' },
        trigger_characters_ft = {} -- { filetype = { '.' } }
      }
    },
    { name = 'fuzzy_buffer',
      group_index = 9, -- 🤔
      -- { name = 'buffer',
      option = {
        -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%([\-.]\w*\)*\)]],
        -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\S*\%([\-.]\S*\)*\)]],
        -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%([\-.]\w*\)*\|[а-яА-ЯеёЕЁ]*\)]], -- ломает ввод в текущем виде
        get_bufnrs = function() -- visible buffers
          local bufs = {}
          for _, win in ipairs(vim.api.nvim_list_wins()) do
            -- local buf = vim.api.nvim_get_current_buf()
            local buf = vim.api.nvim_win_get_buf(win)
            local byte_size = vim.api.nvim_buf_get_offset(buf, vim.api.nvim_buf_line_count(buf))
            if byte_size <= 1024 * 1024 then -- Max: 1 MB (TODO: maybe even lesser?)
              bufs[buf] = true
            end
          end
          return vim.tbl_keys(bufs)
          -- return vim.api.nvim_list_bufs() -- all buffers
        end,
      },
    },
  },
  mapping = mapping,
}

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ "/", "?" }, {
  -- mapping = cmp.mapping.preset.cmdline(),
  -- completion = { autocomplete = false },
  mapping = mapping,
  sources = {
    { name = "buffer",
      keyword_length = 1,
      option = {
        keyword_length = 1,
        -- keyword_pattern = [=[\.\+]=],
        -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\%(\w\|á\|Á\|é\|É\|í\|Í\|ó\|Ó\|ú\|Ú\)*\%(-\%(\w\|á\|Á\|é\|É\|í\|Í\|ó\|Ó\|ú\|Ú\)*\)*\)]],
        -- get_bufnrs = function()
        --   return vim.api.nvim_list_bufs()
        -- end
      },
    },
    -- { name = "cmdline", },
    -- { name = "nvim_lua", },
    -- { name = 'nvim_lsp_document_symbol',
    --   -- group_index = 1,
    -- },
    -- { name = 'fuzzy_buffer',
    --   -- group_index = 2,
    --   option = {
    --     -- keyword_pattern = [=[[^\v[:blank:]].*]=], -- "\?v?" - is a hack to care of loupe's hook
    --     -- "\k\+" -- ?
    --   }
    -- },
  },
  -- view = {
  --   entries = {
  --     name = "custom",
  --   }
  -- },
  -- completion = {
  --   completeopt = 'menu,menuone,noselect',
  -- },
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
  completion = { autocomplete = false },
  mapping = mapping,
  sources = {
    { name = "nvim_lua",
      group_index = 1,
      priority = 20,
    },
    -- { name = "async_path",
    { name = "path",
      -- { name = "fuzzy_path",
      max_item_count = 20,
      option = {
        -- get_cwd = function()
        --   return vim.fn.getcwd()
        -- end,
      },
      group_index = 2,
      priority = 10,
    },
    { name = 'cmdline',
      group_index = 3,
      priority = -10,
      -- option = { ignore_cmds = { }, }
    },
    -- { name = 'cmdline_history', -- XXX: maybe don't need at all
    --   group_index = 3,
    --   max_item_count = 10,
    --   priority = -20,
    --   condition = function() return #cmp.get_entries() <= 0 end, -- XXX: does not work
    -- },
    -- { name = 'zsh' },
    -- { name = "latex_symbols", },
  },
  sorting = {
    priority_weight = 2,
    comparators = {
      -- cmp.config.compare.offset,
      -- cmp.config.compare.exact,
      -- cmp.config.compare.sort_text,
      cmp.config.compare.score,
      -- cmp.config.compare.recently_used,
      -- cmp.config.compare.kind,
      -- cmp.config.compare.sort_text,
      -- cmp.config.compare.recently_used,
      -- cmp.config.compare.length,
      -- cmp.config.compare.order,
    },
  },
})

-- DAP
cmp.setup.filetype({ "dap-repl", "dapui_watches" }, {
  sources = {
    {
      name = "dap",
    },
  },
})
cmp.setup.filetype({ 'markdown', 'help' }, {
  sources = {
    { name = 'async_path' },
    { name = 'buffer' },
  }
})
