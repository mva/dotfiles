-- luacheck: globals vim req

local icons = require"cfg.icons"

local throttle = nil --600 -- 🤷 trying to guess optimal balance between CPU eat and responsiveness

require"noice".setup{
  cmdline = {
    throttle = throttle, -- 1000 / 30, -- frequency to update lsp progress message
    enabled = true,      -- enables the Noice cmdline UI
    -- view for rendering the cmdline. Change to `cmdline` to get a classic cmdline at the bottom
    view = "cmdline_popup",
    -- view = "cmdline",
    -- opts = { buf_options = { filetype = "vim" } },
    -- ^^^ enable syntax highlighting in the cmdline
    -- ^^^ makes treesitter-comment-conntext to brake somhow. WTF?!?
    format = {
      -- conceal: (default=true) This will hide the text in the cmdline that matches the pattern.
      -- view: (default is cmdline view)
      -- opts: any options passed to the view
      -- icon_hl_group: optional hl_group for the icon
      cmdline = { pattern = "^:", icon = icons.noice.cmdline, lang = "vim", },
      search_down = { kind = "search", pattern = "^/", icon = icons.noice.search.down, lang = "regex", },
      search_up = { kind = "search", pattern = "^%?", icon = icons.noice.search.up, lang = "regex", },
      filter = { pattern = "^:%s*!", icon = icons.noice.filter, lang = "bash", },
      lua = { pattern = { "^:%s*lua%s+", "^:%s*lua%s*=%s*", "^:%s*=%s*" }, icon = icons.noice.lua, lang = "lua", },
      help = { pattern = "^:%s*he?l?p?%s+", icon = icons.noice.help, },
      input = { view = "cmdline_input", icon = icons.noice.input, }, -- Used by input()
      -- lua = false, -- to disable a format, set to `false`
    },
  },
  messages = {
    -- NOTE: If you enable messages, then the cmdline is enabled automatically.
    -- This is a current Neovim limitation.
    enabled = true,              -- enables the Noice messages UI
    throttle = throttle,         -- 1000 / 30, -- frequency to update lsp progress message
    view = "notify",             -- default view for messages
    view_error = "notify",       -- view for errors
    view_warn = "notify",        -- view for warnings
    -- view_history = "split", -- view for :messages
    view_history = "popup",      -- view for :messages
    -- view_history = "messages",   -- view for :messages
    view_search = "virtualtext", -- view for search count messages. Set to `false` to disable
  },
  popupmenu = {
    enabled = true,      -- enables the Noice popupmenu UI
    throttle = throttle, -- 1000 / 30, -- frequency to update lsp progress message
    ---@type 'nui'|'cmp'
    backend = "cmp",     -- backend to use to show regular cmdline completions
    -- backend = "nui",     -- backend to use to show regular cmdline completions
    -- Icons for completion item kinds (see defaults at noice.config.icons.kinds)
    -- kind_icons = {}, -- set to `false` to disable icons
  },
  -- default options for require('noice').redirect
  -- see the section on Command Redirection
  redirect = {
    view = "popup",
    filter = { event = "msg_show" },
  },
  -- You can add any custom commands below that will be available with `:Noice command`
  commands = {
    history = {
      -- options for the message history that you get with `:Noice`
      view = "popup", -- "split"
      opts = { enter = true, format = "details" },
      filter = { event = { "msg_show", "notify" }, ["not"] = { kind = { "search_count", "echo" } } },
    },
    messages = {
      -- options for the message history that you get with `:Noice`
      view = "popup",
      opts = { enter = true, format = "details" },
      filter = { event = { "msg_show", "notify" }, kind = { "search_count", "echo" } },
    },
    -- :Noice last
    last = {
      view = "popup",
      opts = { enter = true, format = "details" },
      filter = { event = { "msg_show", "notify" }, ["not"] = { kind = { "search_count", "echo" } } },
      filter_opts = { count = 1 },
    },
    -- :Noice errors
    errors = {
      -- options for the message history that you get with `:Noice`
      view = "popup",
      opts = { enter = true, format = "details" },
      filter = { error = true },
      filter_opts = { reverse = true },
    },
  },
  notify = {
    -- Noice can be used as `vim.notify` so you can route any notification like other messages
    -- Notification messages have their level and other properties set.
    -- event is always "notify" and kind can be any log level as a string
    -- The default routes will forward notifications to nvim-notify
    -- Benefit of using Noice for this is the routing and consistent history view
    enabled = true,
    view = "notify",
  },
  lsp = {
    override = {
      -- override markdown rendering so that **cmp** and other plugins use **Treesitter**
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
      -- override the default lsp markdown formatter with Noice
      -- ["vim.lsp.util.convert_input_to_markdown_lines"] = false,
      -- -- override the lsp markdown formatter with Noice
      -- ["vim.lsp.util.stylize_markdown"] = false,
      -- -- override cmp documentation with Noice (needs the other options to work)
      -- ["cmp.entry.get_documentation"] = false,
    },
    progress = {
      enabled = false,
      -- Lsp Progress is formatted using the builtins for lsp_progress. See config.format.builtin
      -- See the section on formatting for more details on how to customize.
      format = "lsp_progress",
      format_done = "lsp_progress_done",
      throttle = 1000 / 3, -- frequency to update lsp progress message
      view = "mini",
    },
    hover = {
      throttle = throttle, -- 1000 / 30, -- frequency to update lsp progress message
      silent = false,      -- set to true to not show a message if hover is not available
      enabled = true,
      view = nil,          -- "notify", -- when nil, use defaults from documentation
      opts = {
        -- merged with defaults from documentation
        lang = "markdown",
        replace = true,
        render = "plain",
        format = { "{message}" },
        buf_options = { iskeyword = '!-~,^*,^|,^",192-255', keywordprg = ":help" },
        win_options = { concealcursor = "n", conceallevel = 3 },
      },
    },
    signature = {
      throttle = throttle, -- 1000 / 30, -- frequency to update lsp progress message
      enabled = true,
      auto_open = {
        enabled = true,
        trigger = true, -- Automatically show signature help when typing a trigger character from the LSP
        luasnip = true, -- Will open signature help when jumping to Luasnip insert nodes
        throttle = 50,  -- Debounce lsp signature help request by 50ms
      },
      view = nil,       -- when nil, use defaults from documentation
      opts = {},        -- merged with defaults from documentation
    },
    message = {
      -- Messages shown by lsp servers
      enabled = true,
      view = "notify",
      opts = {},
    },
    documentation = {
      throttle = throttle, -- 1000 / 30, -- frequency to update lsp progress message
      view = "hover",
      opts = {
        lang = "markdown",
        replace = true,
        render = "plain",
        format = { "{message}" },
        win_options = { concealcursor = "n", conceallevel = 3 },
      },
    },
  },
  markdown = {
    hover = {
      ["|(%S-)|"] = vim.cmd.help,                     -- vim help links
      ["%[.-%]%((%S-)%)"] = require"noice.util".open, -- markdown links
    },
    highlights = {
      ["|%S-|"] = "@text.reference",
      ["@%S+"] = "@parameter",
      ["^%s*(Parameters:)"] = "@text.title",
      ["^%s*(Return:)"] = "@text.title",
      ["^%s*(See also:)"] = "@text.title",
      ["{%S-}"] = "@parameter",
    },
  },
  health = {
    checker = true, -- Disable if you don't want health checks to run
  },
  smart_move = {
    -- noice tries to move out of the way of existing floating windows.
    enabled = true, -- you can disable this behaviour here
    -- add any filetypes here, that shouldn't trigger smart move.
    excluded_filetypes = { "cmp_menu", "cmp_docs", "notify" },
  },
  presets = {
    -- you can enable a preset by setting it to true, or a table that will override the preset config
    -- you can also add custom presets that you can enable/disable with enabled=true
    bottom_search = false,         -- use a classic bottom cmdline for search
    command_palette = true,        -- position the cmdline and popupmenu together
    -- ^ N.B.: "true" makes cmdline popup appear at the top, "false": at the middle 🤷
    long_message_to_split = false, -- long messages will be sent to a split
    inc_rename = true,             -- enables an input dialog for inc-rename.nvim
    lsp_doc_border = true,         -- add a border to hover docs and signature help
  },
  throttle = throttle,             --1000 / 30,
  -- how frequently does Noice need to check for ui updates? This has no effect when in blocking mode.
  views = {}, ---@see section on views
  routes = {
    { -- Hide message about saved lines/bytes (or not hide?)
      filter = {
        event = "msg_show",
        kind = "",
        find = "%d+L, %d+B",
      },
      opts = { skip = true },
    },
    { -- Show key mappings in popup
      view = "popup",
      filter = {
        event = "msg_show",
        kind = "", -- ?
        any = {
          { find = "^[icnvx!][ 	]*%<[^>]*>[ 	]*", },
          { find = "^Normal.*xxx", },
          { min_height = 20, },
        },
      },
    },
  },           --- @see section on routes
  status = {}, --- @see section on statusline components
  format = {}, --- @see section on formatting
}


vim.keymap.set({ "n", "i", "s" }, "<c-f>", function()
  if not require("noice.lsp").scroll(4) then
    return "<c-f>"
  end
end, { silent = true, expr = true })

vim.keymap.set({ "n", "i", "s" }, "<c-b>", function()
  if not require("noice.lsp").scroll(-4) then
    return "<c-b>"
  end
end, { silent = true, expr = true })
-- vim.keymap.set(
--   {"n"},
--   "<esc><esc>",
--   function() require"notify".dismiss{silent=true,pending=true} end,
--   {silent = true, desc = "(Notify/Noice) Dismiss all notifications"}
-- )
