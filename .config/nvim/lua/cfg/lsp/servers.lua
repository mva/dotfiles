-- luacheck: globals vim

-- TODO: fill with only values corresponding to currently opened files, maybe?
local srv = {
  -- "pyright", -- python
  -- "jedi_language_server", -- python
  -- "rust_analyzer",
  -- "tsserver", -- typescript
  -- "solargraph", -- ruby

  -- C/C++ {{{
  -- Can't finally choose between this two: clangd is developed by upstream, but ccls supports cxx-highlight plugin
  "clangd",
  -- "ccls",
  -- }}}

  "cmake",
  "dockerls",
  "gopls",
  "rls",
  "pylsp",
  "jsonls",
  "cssls",
  "cssmodules_ls",
  "html",
  "eslint",
  "arduino_language_server",
  "yamlls",
  "ansiblels",
  "asm_lsp",
  "awk_ls",
  -- "diagnosticls",
  "sqlls",
  -- sqls",
  -- "golangci_lint_ls", -- not sure if needed in pair with gopls

  -- "efm",

  -- "grammarly", -- heavy, semiproprietary, cloud crap, isn't it?

  -- Markdown {{{
  "marksman",
  -- "remark_ls",
  -- "prosemd_lsp",
  -- "zk",
  -- }}}

  -- PHP {{{
  -- "intelephsense",
  -- "phpactor",
  -- "psalm",
  -- }}}

  -- Perl {{{
  -- "perlnavigator",
  -- }}}

  -- PowerShell {{{
  -- "powershell_es",
  -- }}}

  "stylelint_lsp",

  -- VimL
  "vimls",

  -- XML
  "lemminx",

  -- LTeX
  -- "ltex", -- Custom name to don't trigger bundled setup, as I use `ltex-ls` plugin instead of it

  -- "lua", -- not included in upstream lspconfig. I added it manually a bit above
  -- "lua_ls", -- ex sumneko -- too heavy
}

if _G.NVIM_HACKS["lsp:lua"] then
  table.insert(srv, "lua")
elseif _G.NVIM_HACKS["lsp:sumneko"] or _G.NVIM_HACKS["lsp:lua_ls"] then
  table.insert(srv, "lua_ls")
end

return srv
