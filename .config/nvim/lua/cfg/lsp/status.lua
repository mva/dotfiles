-- luacheck: globals req

local lsp_status = require"lsp-status"
lsp_status.register_progress()

return lsp_status
