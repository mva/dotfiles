-- luacheck: globals vim T
-- {{{ capabilities
local def_caps = vim.lsp.protocol.make_client_capabilities()
local cmplsp_caps = require"cfg.cmp.lsp"

local capabilities = vim.tbl_deep_extend(
  'force',
  def_caps or {},
  cmplsp_caps
)
-- capabilities = vim.tbl_extend('keep', capabilities or {}, lsp_status.capabilities)

capabilities.textDocument.foldingRange = {
  dynamicRegistration = false,
  lineFoldingOnly = true,
}

capabilities.textDocument.completion.completionItem.documentationFormat = {
  "markdown",
  "plaintext"
}

capabilities.textDocument.completion.completionItem.snippetSupport = true

capabilities.textDocument.completion.completionItem.preselectSupport = true
capabilities.textDocument.completion.completionItem.insertReplaceSupport = true
capabilities.textDocument.completion.completionItem.labelDetailsSupport = true
capabilities.textDocument.completion.completionItem.deprecatedSupport = true
capabilities.textDocument.completion.completionItem.commitCharactersSupport = true
capabilities.textDocument.completion.completionItem.tagSupport = { valueSet = { 1 } }
capabilities.textDocument.completion.completionItem.resolveSupport = {
  properties = {
    "documentation",
    "detail",
    "additionalTextEdits",
  },
}


return capabilities
