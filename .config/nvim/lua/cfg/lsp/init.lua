-- luacheck: globals vim T nmap, no max line length
local lsp = require"lspconfig" or {}
local lsp_configs = require"lspconfig.configs"
-- local lsp_util = require"lspconfig.util"

local sign_define = vim.fn.sign_define

for name, icon in pairs(require"cfg.icons".diagnostics) do
  name = T{ "DiagnosticSign", name }:concat()
  sign_define(name, {
    text = icon,
    texthl = name,
    numhl = name,
    -- linehl = name,
  })
end

require"cfg.lsp.ui".init()

local servers -- = {}
local setups  -- = {}

-- if vim.fn.filereadable(("%s/.load_lspservers"):format(vim.fn.stdpath'data')) == 1 then
if _G.NVIM_HACKS["load:lsp"] then
  servers = require"cfg.lsp.servers" ---@type table
  setups  = require"cfg.lsp.setups" ---@type table
else
  servers, setups = {}, {}
end

-- Custom servers declarations {{{

if _G.NVIM_HACKS["lsp:lua"] then
  lsp_configs.lua = { -- Custom lua-lsp server
    default_config = {
      cmd = { "lua-lsp" },
      filetypes = { "lua", "moon" },
      root_dir = function()
        return vim.loop.cwd() --- @diagnostic disable-line: undefined-field
      end
      -- lsp_util.path.dirname
    },
    -- on_new_config = function(new_config) end;
    -- on_attach = function(client, bufnr) end;
  }
end

if _G.NVIM_HACKS["lsp:emmet"] then
  local srv_p = ("%s/mason/bin"):format(vim.fn.stdpath("data"))
  lsp_configs.emmet_ls = { -- Custom LSP server for "emmet" (creates HTML blocks from selector line)
    default_config = {
      cmd = {
        T{ srv_p, "emmet-ls" }:concat(),
        '--stdio'
      },
      filetypes = { 'html', 'css', 'blade' },
      root_dir = function()
        return vim.loop.cwd()
      end,
      init_options = {
        html = {
          options = {
            -- For possible options, see: https://github.com/emmetio/emmet/blob/master/src/config.ts#L79-L267
            ["bem.enabled"] = true,
          },
        },
      },
      -- settings = {},
    },
  }
end

-- /custom servers }}}

--[[ In case of debug {{{
lsp.util.default_config = vim.tbl_extend(
  "force", lsp.util.default_config, {log_level = vim.lsp.protocol.MessageType.Debug}
)
--]]                  --}}}

local custom_lsps = { -- For plugins with alternate configurations (instead of lspconfig)
  -- ltex = _G.NVIM_HACKS["lsp:ltex"] and require'ltex-ls'
}

for _, srv in pairs(servers) do --- NOTE: ipairs is bad with "holes" (nil values) in the middle of the tables
  local l ---@type table

  if custom_lsps[srv] then
    l = custom_lsps
  elseif lsp[srv] then
    l = lsp
  else
    l = {
      setup = function() vim.notify(("Attempted to setup LSP server that has no declarations: %s"):format(srv), "warn") end, }
  end

  l[srv].setup(setups[srv] or setups.default)
end

local lsp_att_aug = vim.api.nvim_create_augroup('CfgLspHandler', {})
vim.api.nvim_create_autocmd("LspAttach", {
  group = lsp_att_aug,
  callback = function(args)
    local buffer = args.buf
    local cid = vim.lsp.get_client_by_id(args.data.client_id)
    local on_attach = require"cfg.lsp.handlers".on_attach
    on_attach(cid, buffer)
  end,
})

return { lsp = lsp, defaults = setups.default or {} }
