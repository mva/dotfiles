-- luacheck: globals vim T

local has = require"lib.lazyutil".has

local _M = T{}

function _M.init()
  local is_lines = has"lsp_lines.nvim"
  _G.diagnostic_virtuals_settings = {}
  _G.diagnostic_virtuals_settings.base = {
    virtual_text = { spacing = 1, prefix = "●" },
    virtual_lines = { only_current_line = true, }, -- keep_text = true, },
    signs = true,
  }
  _G.diagnostic_virtuals_settings.mode = {
    global = {
      virtual_text = true,
      virtual_lines = false,
      signs = true,
    },
    lazy = {
      virtual_text = true,
      virtual_lines = false,
      signs = false,
    },
  }

  local c = T(vim.diagnostic.config())

  if is_lines then
    require"lsp_lines".setup()
    _G.diagnostic_virtuals_settings.lines_available = true
    _G.diagnostic_virtuals_settings.mode.global.signs = true
    _G.diagnostic_virtuals_settings.mode.global.virtual_lines = true
    _G.diagnostic_virtuals_settings.mode.global.virtual_text = false
  end

  _G.diagnostic_virtuals_settings.prev_state = c
  c.signs = _M.state_fun("signs")
  c.underline = true
  c.update_in_insert = false
  c.virtual_lines = _M.state_fun("virtual_lines")
  c.virtual_text = _M.state_fun("virtual_text")
  c.severity_sort = true
  -- ^^ XXX: { filter = function(diag) ... return diag.message end}
  _G.diagnostic_virtuals_settings.defauts = c
  -- _G.diagnostic_virtuals_settings.cur_state = c

  vim.diagnostic.config(c) --- @diagnostic disable-line: param-type-mismatch
end

function _M.state_fun(typ)
  return function(_, bufnr)
    local ft = vim.bo[bufnr].ft
    local mt = _G.diagnostic_virtuals_settings.mode
    local ret = mt.global[typ]
    if mt[ft] then
      if mt[ft][typ] then
        return _G.diagnostic_virtuals_settings.base[typ]
      end
    elseif ret then
      return _G.diagnostic_virtuals_settings.base[typ]
    else
      return ret
    end
  end
end

function _M.reload()
  vim.diagnostic.config(vim.diagnostic.config())
end

function _M.set_lines(reason)
  local c = vim.diagnostic.config()
  if not _G.diagnostic_virtuals_settings.prev_state then
    _G.diagnostic_virtuals_settings.prev_state = { c = c, reason = reason }
  end
  _G.diagnostic_virtuals_settings.mode.global.virtual_lines = true
  _G.diagnostic_virtuals_settings.mode.global.virtual_text = false
  vim.diagnostic.config(c)
end

function _M.set_virttext(reason)
  local c = vim.diagnostic.config()
  if not _G.diagnostic_virtuals_settings.prev_state then
    _G.diagnostic_virtuals_settings.prev_state = { c = c, reason = reason }
  end
  _G.diagnostic_virtuals_settings.mode.global.virtual_lines = false
  _G.diagnostic_virtuals_settings.mode.global.virtual_text = true
  vim.diagnostic.config(c)
end

function _M.restore(only_from_reasons)
  local ps = _G.diagnostic_virtuals_settings.prev_state
  if not ps then
    return
  elseif only_from_reasons then
    for i = 1, #only_from_reasons do
      if ps.reason ~= only_from_reasons[i] then
        return
      end
    end
  end
  vim.diagnostic.config(ps.c)
end

function _M.toggle()
  local c = vim.diagnostic.config()
  _G.diagnostic_virtuals_settings.mode.global.virtual_text =
      not _G.diagnostic_virtuals_settings.mode.global.virtual_text
  _G.diagnostic_virtuals_settings.mode.global.virtual_lines =
      not _G.diagnostic_virtuals_settings.mode.global.virtual_lines
  vim.diagnostic.config(c)
end

return _M
