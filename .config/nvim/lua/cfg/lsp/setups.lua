-- luacheck: globals vim T nmap fn, no max line length

-- local on_attach = require"cfg.lsp.handlers".on_attach

local capabilities = require"cfg.lsp.caps"

local setups = T{}

---@diagnostic disable-next-line
local function goenv()
  local env = io.popen("go env")
  if not env then return {} end
  local ret = {}
  for line in env:lines() do
    local k, v = line:match([=[^([^=]+)%="(.*)"$]=])
    ret[k] = v
  end
  return ret
end
-- stylua: ignore
-- luacheck: ignore
---@diagnostic disable-next-line
local function gopath()
  local vp = vim.g.gopath;
  if vp then
    return { GOPATH = vp }
  else
    return goenv().GOPATH
  end
end

setups.default = {
  -- {{{
  -- on_attach = on_attach,
  capabilities = capabilities,
  flags = {
    debounce_text_changes = 150 -- ?
  },
  --    root_dir = vim.loop.cwd,
} -- }}}

setups.gopls = {
  -- {{{
  --{{{   cmd = {
  --      --("%s/bin/gopls"):format(goenv().GOROOT),
  --      ("%s/bin/gopls"):format(gopath()),
  --      "serve",
  --}}}   },
  settings = {
    gopls = {
      analyses = {
        unusedresults = true,
        unusedparams = true,
        composites = true,
      },
      staticcheck = true,
      completeUnimported = true,
      expandWorkspaceToModule = true,
      semanticTokens = true,
      usePlaceholders = true,
      hints = {
        assignVariableTypes = true,
        compositeLiteralFields = true,
        compositeLiteralTypes = true,
        constantValues = true,
        functionTypeParameters = true,
        paramaterNames = true,
        rangeVariableTypes = true,
      },
    },
  },
  filetypes = {
    "go",
    "gomod",
    "gowork",
    "gotmpl",
  },
  capabilities = capabilities,
  -- on_attach = on_attach,
} -- }}}

setups.ccls = {
  -- {{{
  init_options = {
    highlight = {
      lsRanges = true,
    },
  },
  -- on_attach = on_attach,
  capabilities = capabilities,
} -- }}}
setups.clangd = {
  -- handlers = ext.clangd.setup(),
  init_options = {
    clangdFileStatus = true
  },
  -- on_attach = on_attach,
  capabilities = capabilities,
}

-- Schemastore {{{
local schemastore_schemas;

local has = require"lib.lazyutil".has
if has"schemastore.nvim" then
  schemastore_schemas = require'schemastore'.json.schemas()
end
-- }}}

setups.jsonls = {
  -- on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    json = {
      schemas = schemastore_schemas,
      validate = { enable = true },
    },
  },
}

setups.yamlls = {
  -- on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    yaml = {
      schemas = schemastore_schemas,
      validate = { enable = true },
    },
  },
}

setups.ansiblels = {
  -- on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    ansible = {
      ansible = {
        useFullyQualifiedCollectionNames = true,
      }
    }
  }
}

setups.pylsp = {
  -- on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    pylsp = {
      configurationSources = { "pyflake8" },
      plugins = {
        flake8 = {
          enabled = true,
          exclude = { '.git', '__pycache__' },
          -- ignore = {"E501"},
          maxComplexity = 10,
        },
        pycodestyle = { enabled = false, },
        pyflakes = { enabled = false, },
        mccabe = { enabled = false, },
      },
    },
  },
}

setups.lemminx = {
  -- on_attach = on_attach,
  capabilities = capabilities,
  filetypes = { 'xml', 'xsd', 'xsl', 'xslt', 'svg', "gentoo-metadata", "guidexml", },
  settings = {
    xml = {
      useCache = true, -- to be able to disable it sometimes, when it goes out of mind
      server = {
        -- workDir = "~/.local/state/lemminx",
        workDir = "~/.cache/lemminx",
      },
    },
  },
}

if _G.NVIM_HACKS["lsp:ltex"] then
  setups.ltex = {
    -- on_attach = on_attach,
    capabilities = capabilities,
    use_spellfile = false,
    filetypes = { "latex", "tex", "bib", "markdown", "gitcommit", "text", },
    settings = {
      ltex = {
        enabled = { "latex", "tex", "bib", "markdown", "gitcommit", "text", },
        language = "auto",
        diagnosticSeverity = {
          default = "hint",
          MORFOLOGIK_RULE_EN_US = "warning", -- spelling
        },
        sentenceCacheSize = 2000,
        additionalRules = {
          enablePickyRules = true,
          motherTongue = "ru",
        },
        markdown = { -- https://valentjn.github.io/ltex/settings.html#ltexmarkdownnodes
          nodes = {},
        },
        disabledRules = {
          --       fr = { "APOS_TYP", "FRENCH_WHITESPACE" } -- example
          --       ["en-US"] = { "EN_QUOTES" }, -- don't expect smart quotes
        },
        dictionary = (function()
          -- For dictionary, search for files in the runtime to have
          -- and include them as externals the format for them is
          -- dict/{LANG}.txt
          --
          -- Also add dict/default.txt to all of them
          local files = {}
          for _, file in ipairs(vim.api.nvim_get_runtime_file("dict/*", true)) do
            local lang = fn.fnamemodify(file, ":t:r")
            local fullpath = vim.fs.normalize(file)
            files[lang] = { ":" .. fullpath }
          end

          if files.default then
            for lang, _ in pairs(files) do
              if lang ~= "default" then
                vim.list_extend(files[lang], files.default)
              end
            end
            files.default = nil
          end
          return files
        end)(),
      },
    },
  }
end

if _G.NVIM_HACKS["lsp:lua_ls"] or _G.NVIM_HACKS["lsp:sumneko"] then
  local expand = vim.fn.expand
  local USER_SETTINGS = expand('~/.config/lua/sumneko.json')
  local DEFAULT_SETTINGS = {
    lib = {
      include_vim = true, -- Make the server aware of Neovim runtime files
      extra = {},
    },
    log_level = 2,
  }
  local function merge(t, extra)
    if type(t) == 'table' and type(extra) == 'table' then
      for k, v in pairs(extra) do
        t[k] = merge(t[k], v)
      end
      return t
    end
    return extra
  end

  local function load_user_settings()
    local settings = DEFAULT_SETTINGS
    if not fn.filereadable(USER_SETTINGS) then
      return settings
    end
    local user = _G._read_json_file(USER_SETTINGS)
    if not user then
      return settings
    end
    return merge(settings, user)
  end
  local function lua_libs(opts)
    local libs = {}
    if opts.include_vim then
      libs[expand('$VIMRUNTIME/lua')] = true
      libs[expand('$VIMRUNTIME/lua/vim/lsp')] = true
      libs[expand(('%s/lua'):format(vim.fn.stdpath"config"))] = true
      libs[expand(('%s/lua/cfg'):format(vim.fn.stdpath"config"))] = true
      libs[expand(('%s/lua/plugins'):format(vim.fn.stdpath"config"))] = true
    end
    for _, item in ipairs(opts.extra) do
      libs[expand(item)] = true
    end

    libs[expand("$PWD")] = true
    libs[expand("$PWD/lua")] = true
    libs[expand("$PWD/src")] = true
    return libs
  end

  local settings = load_user_settings()
  local library = lua_libs(settings.lib)
  local path = T(vim.split(package.path, ';'))
  for lib in pairs(library) do
    path:insert(lib .. '/?.lua')
    path:insert(lib .. '/?/init.lua')
  end

  local function editing_nvim_cfg()
    if vim.fn.expand"%:p:h":match"/.config/nvim/lua" then
      return vim.api.nvim_get_runtime_file('', true)
    end
  end

  setups.lua_ls = {
    -- on_attach = on_attach,
    capabilities = capabilities,
    -- cmd = { SERVER },
    log_level = settings.log_level,
    settings = {
      Lua = {
        format = {
          enable = true,
          defaultConfig = {
            space_before_attribute = false,
            space_before_function_call_single_arg = false,
          },
        },
        runtime = {
          version = 'LuaJIT', -- neovim implies luajit
          path = path,
        },
        completion = {
          enable = true,
          -- callSnippet = "Replace",
          callSnippet = "Both",
          keywordSnippet = "Both",
          displayContext = 4,
        },
        signatureHelp = {
          enable = true,
        },
        hint = {
          -- enable = true,
          setType = true,
        },
        hover = {
          enable = true,
        },
        diagnostics = {
          enable = true,
          worksaceDelay = 0, --5000,
          workspaceRate = 20,
          neededFileStatus = {
            -- ["codestyle-check"] = "Opened",
            ["codestyle-check"] = "Any",
          },
          disable = {
            'lowercase-global',
          },
          globals = {
            'vim',
            -- openresty/kong globals
            'ngx',
            'kong',
            -- busted globals
            'after_each',
            'before_each',
            'describe',
            'expose',
            'finally',
            'insulate',
            'it',
            'lazy_setup',
            'lazy_teardown',
            'mock',
            'pending',
            'pending',
            'randomize',
            'setup',
            'spec',
            'spy',
            'strict_setup',
            'strict_teardown',
            'stub',
            'teardown',
            'test',
          },
        },
        workspace = {
          -- library = library,
          -- library = vim.api.nvim_get_runtime_file('', true),
          library = editing_nvim_cfg(),
          ignoreSubmodules = false,
          checkThirdParty = false,
          userThirdParty = {
            "OpenResty",
          },
          -- maxPreload = 100000,
          -- preloadFileSize = 10000,
          maxPreload = 1000,
          preloadFileSize = 500,
        },
        telemetry = {
          -- don't phone home
          enable = false,
        },
        semantic = {
          enable = true,
        },
      },
    },
  }
end

return setups
