-- luacheck: globals vim

local luv = require('luv')
require"lazy".setup({
  spec = {
    { import = "plugins" },
  },
  rocks = { enabled = false, },
  defaults = {
    -- By default, only LazyVim plugins will be lazy-loaded. Your custom plugins will load during startup.
    -- If you know what you're doing, you can set this to `true` to have all your custom plugins lazy-loaded by default.
    lazy = true,
    -- It's recommended to leave version=false for now, since a lot the plugin that support versioning,
    -- have outdated releases, which may break your Neovim install.
    version = false, -- always use the latest git commit
    -- version = "*", -- try installing the latest stable version for plugins that support semver
    -- cond = function() return not is_viewonly() end,
  },
  --  install = { colorscheme = { "luakai" } },
  checker = { -- TODO: conditional checks (make function for that)
    enabled = false,
  }, -- automatically check for plugin updates
  concurrency = luv.available_parallelism(),
  install = { -- TODO: conditional (make function for that)
    missing = false, -- think about it
    colorscheme = {
      "luakai",
    },
  },
  ui = {
    wrap = false,
    size = {
      width = 0.6,
      -- height = 0.8,
    },
    icons = require"cfg.icons".lazy,
    custom_keys = {
      -- you can define custom key maps here.
      -- To disable one of the defaults, set it to false

      -- open lazygit log
      ["<localleader>l"] = false,
      ["<localleader>zl"] = function(plugin)
        require("lazy.util").float_term({ "lazygit", "log" }, {
          cwd = plugin.dir,
        })
      end,

      -- open a terminal for the plugin dir
      ["<localleader>t"] = false,
      ["<localleader>zt"] = function(plugin)
        require("lazy.util").float_term(nil, {
          cwd = plugin.dir,
        })
      end,
    },
  },
  diff = {
    cmd = "diffview.nvim",
  },
  change_detection = { -- TODO: conditional checks (make functionnn for that)
    enabled = false,
    notify = false, -- ?
  },
  dev = { ---@diagnostic disable-line: assign-type-mismatch
    path = vim.env.NVIM_DEV, -- "~/.vcs_repos/nvim",
    patterns = {
      "msva",
    },
    fallback = true,
  },
  performance = {
    rtp = {
      -- disable some rtp plugins
      disabled_plugins = {
        "gzip",
        "matchit",    --
        "matchparen", -- IIRC, there is plugins for both
        -- "netrwPlugin", -- remote edit
        "tarPlugin",
        "tohtml",
        "tutor",
        "zipPlugin",
      },
    },
  },
})
