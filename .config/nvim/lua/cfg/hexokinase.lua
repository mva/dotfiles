vim.g.Hexokinase_highlighters = { 'virtual' }
-- { 'backgroundfull' } -- emulate Colorizer hehaviour

--[[
   'virtual',

   'sign_column',

   'background',
   'backgroundfull',

   'foreground',
   'foregroundfull',
--]]

vim.g.Hexokinase_optInPatterns = {
  "full_hex",
  "triple_hex",
  "rgb",
  "rgba",
  "hsl",
  "hsla",
  "colour_names",
}

vim.g.Hexokinase_ftEnabled = {
  "css",
  "html",
  "javascript",
  "lua",
  "vim",
}


--[[ vim.g.Hexokinase_ftOptInPatterns = {
  css = "full_hex,rgb,rgba,hsl,hsla,colour_names",
  html = "full_hex,rgb,rgba,hsl,hsla,colour_names",
} ]]
