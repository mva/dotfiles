-- luacheck: globals vim
require"fold-cycle".setup{
  open_if_max_closed = true,
  close_if_max_closed = true,
}
--[[
-- vim.keymap.set('n', '<tab>',
--   function() return require('fold-cycle').open() end,
--   {silent = true, desc = 'Fold-cycle: open current folds'})
-- vim.keymap.set('n', '<s-tab>',
--   function() return require('fold-cycle').close() end,
--   {silent = true, desc = 'Fold-cycle: close current fold'})
--]]
vim.keymap.set('n', 'zo',
  function() return require"fold-cycle".open() end,
  { silent = true, desc = '(Fold-cycle) Open current fold' })
vim.keymap.set('n', 'zc',
  function() return require"fold-cycle".close() end,
  { silent = true, desc = '(Fold-cycle) Close current fold' })
vim.keymap.set('n', 'zO',
  function() return require"fold-cycle".open_all() end,
  { remap = true, silent = true, desc = '(Fold-cycle) Open all folds under cursor' })
vim.keymap.set('n', 'zC',
  function() return require"fold-cycle".close_all() end,
  { remap = true, silent = true, desc = '(Fold-cycle) Close all folds under cursor' })
