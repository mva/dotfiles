local f = require"lib.funcs"

require"FTerm".setup{
  dimensions = { height = 0.9, width = 0.9 },
  border = 'shadow',
  hl = 'FTermBackground'
}

vim.cmd[[
  command! FTermToggle lua require"FTerm".toggle()
  hi LazygitBackground guibg=#1a1b26
  hi FTermBackground   guibg=#1a1b26
]]
f.add_cmd(
  "LazygitToggle",
  "lua require'cfg.lazygit'.toggle()",
  true
)
