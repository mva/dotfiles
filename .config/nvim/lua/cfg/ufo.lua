-- luacheck: globals vim T nop
-- luacheck: globals nnoremap map imap vmap noremap inoremap nmap xmap xnoremap tmap tnoremap smap omap _map

local ufo = require"ufo"
local foldingrange = require("ufo.model.foldingrange")
local bufmanager = require("ufo.bufmanager")


local filetype_exclude = {
  "help",
  "alpha",
  "dashboard",
  "neo-tree",
  "Trouble",
  "lazy",
  "mason",
  "noice",
  "notify",
}

vim.api.nvim_create_autocmd("FileType", {
  group = vim.api.nvim_create_augroup("local_detach_ufo", { clear = true }),
  pattern = filetype_exclude,
  callback = function()
    ufo.detach()
  end,
})

local foldIcon = ""
local hlgroup = "NonText"

local function foldTextFormatter(virtText, lnum, endLnum, width, truncate)
  local newVirtText = {}
  local suffix = ("  %s  %d"):format(foldIcon, endLnum - lnum)
  local sufWidth = vim.fn.strdisplaywidth(suffix)
  local targetWidth = width - sufWidth
  local curWidth = 0
  for _, chunk in ipairs(virtText) do
    local chunkText = chunk[1]
    local chunkWidth = vim.fn.strdisplaywidth(chunkText)
    if targetWidth > curWidth + chunkWidth then
      table.insert(newVirtText, chunk)
    else
      chunkText = truncate(chunkText, targetWidth - curWidth)
      local hlGroup = chunk[2]
      table.insert(newVirtText, { chunkText, hlGroup })
      chunkWidth = vim.fn.strdisplaywidth(chunkText)
      if curWidth + chunkWidth < targetWidth then
        suffix = suffix .. (" "):rep(targetWidth - curWidth - chunkWidth)
      end
      break
    end
    curWidth = curWidth + chunkWidth
  end
  table.insert(newVirtText, { suffix, hlgroup })
  return newVirtText
end

local CustomMarkerProvider = {}

function CustomMarkerProvider.getFolds(bufnr)
  local buf = bufmanager:get(bufnr)
  if not buf then return end
  local fmrOpen     = vim.opt.foldmarker:get()[1]
  local fmrClose    = vim.opt.foldmarker:get()[2]
  -- local commentstring = vim.opt.commentstring:get()
  -- if commentstring == "/*%s*/" then
  --   -- Hack for c++ and other // and /* */ langs
  --   commentstring = "//%s"
  -- end
  -- local openRegx = commentstring .. "*.-" .. fmrOpen
  -- local closeRegx = commentstring .. "*.-" .. fmrClose
  local openRegx    = ".-" .. fmrOpen
  local closeRegx   = ".-" .. fmrClose
  local summaryRegx = openRegx .. "%s*(.*)"
  local lines       = buf:lines(1, -1)

  local ranges      = {}
  local stack       = {}

  for lnum, line in ipairs(lines) do
    -- Check for start marker
    if line:match(openRegx) then
      table.insert(stack, lnum)
      -- Check for end marker
    elseif line:match(closeRegx) then
      local startLnum = table.remove(stack)
      if startLnum then
        local summary = lines[startLnum]:match(summaryRegx)
        table.insert(ranges, foldingrange.new(startLnum - 1, lnum - 1, summary))
      end
    end
  end

  return ranges
end

local function customSelector(bufnr)
  local promise = require"promise"

  local treesitter, lsp

  local sources = {}
  local ranges = {}
  local marker = CustomMarkerProvider.getFolds(bufnr)

  table.insert(sources, "marker")

  local lsp_available = false
  local ts_available = false
  if require"vim.treesitter.highlighter".active[bufnr] then
    ts_available = true
    treesitter = ufo.getFolds(bufnr, "treesitter")
    -- treesitter = promise.resolve(ufo.getFolds(bufnr, "treesitter")):catch(function()end)
  end

  local lsp_clients = vim.lsp.get_clients()
  if #lsp_clients > 0 then
    for _, v in ipairs(lsp_clients) do
      if v.attached_buffers[bufnr] then
        lsp_available = true
        -- lsp = promise.resolve(ufo.getFolds(bufnr, "lsp"))
        lsp = promise.resolve(ufo.getFolds(bufnr, "lsp")):catch(function() end)
      end
    end
  end

  ranges = vim.list_extend(ranges, marker or {})

  if lsp_available and #lsp > 0 then
    ranges = vim.list_extend(ranges, lsp)
    table.insert(sources, "lsp")
  elseif ts_available and #treesitter > 0 then
    ranges = vim.list_extend(ranges, treesitter)
    table.insert(sources, "treesitter")
  else
    -- end
    local indent = ufo.getFolds(bufnr, "indent")
    ranges = vim.list_extend(ranges, indent)
    table.insert(sources, "indent")
  end
  vim.b[bufnr].ufo_sources = table.concat(sources, ", ")
  vim.b[bufnr].fold_ranges = ranges
  return ranges
end

-- lsp->treesitter->indent
---@diagnostic disable-next-line unused
local function providerSelector(bufnr, filetype, buftype) -- luacheck: no unused args
  local ftMap = {
    -- vim = "indent",
    -- python = {"indent"},
    git = "",
    gitcommit = "",
    -- lua = {"treesitter", "indent"},
    -- [""] = "indent",
    -- markdown = {"treesitter", "indent"},
    -- sh = {"treesitter", "indent"},
    -- bash = {"treesitter", "indent"},
    -- zsh = {"treesitter", "indent"},
    -- css = {"treesitter", "indent"},
  }
  return ftMap[filetype] or customSelector
end

ufo.setup{
  provider_selector = providerSelector,
  close_fold_kinds_for_ft = {
    default = {
      "comment", ---@diagnostic disable-line: assign-type-mismatch
      "imports", ---@diagnostic disable-line: assign-type-mismatch
    },
    json = {
      "array",
    },
    c = {
      "comment",
      "region",
    },
  },
  preview = {
    win_config = {
      border = { "", "─", "", "", "", "─", "", "" },
      winhighlight = "Normal:Folded",
      winblend = 0
    },
    mappings = {
      scrollU = "<C-u>",
      scrollD = "<C-d>",
      jumpTop = "[",
      jumpBot = "]"
    }
  },
  open_fold_hl_timeout = 500,
  fold_virt_text_handler = foldTextFormatter,
  -- enable_get_fold_virt_text = true,
}
