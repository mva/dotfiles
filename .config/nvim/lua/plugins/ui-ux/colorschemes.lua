-- luacheck: globals vim T
return {
  {
    "msva/luakai",
    config = function()
      vim.cmd.colorscheme"luakai"
    end,
    enabled = function() return not (vim.env.firstrun) end,
    lazy = false,
    -- event = { "UIEnter", },
    priority = 100000,
    -- dev = true,
  },
}
