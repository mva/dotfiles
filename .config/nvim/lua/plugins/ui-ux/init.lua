return {
  { import = "plugins.ui-ux.colorschemes", }, -- ColorSchemes
  { import = "plugins.ui-ux.bars", },         -- Bars ("Panels", "Lines")
  { import = "plugins.ui-ux.ux", },           -- General UI/UX Reworkinng
  { import = "plugins.ui-ux.syntax", },
}

-- -- the opts function can also be used to change the default opts:
-- {
--   "nvim-lualine/lualine.nvim",
--   event = "VeryLazy",
--   opts = function(_, opts)
--     table.insert(opts.sections.lualine_x, "😄")
--   end,
-- },
--
-- or you can return new options to override all the defaults
-- {
--   "nvim-lualine/lualine.nvim",
--   event = "VeryLazy",
--   opts = function()
--     return {
--       --[[add your custom lualine config here]]
--     }
--   end,
-- },
