-- luacheck: globals vim T
return {
  {
    'jamessan/vim-gnupg',
    -- lazy = false,
    ft = { "gpg" },
  },                    -- don't support ft 😢
  {
    'chrisbra/csv.vim', -- {{{
    ft = { "csv" },
    config = function() -- {{{
      local hi = vim.api.nvim_set_hl
      hi(0, "CSVColumnOdd", { link = "String", })
      hi(0, "CSVColumnEven", { link = "Statement", })
      hi(0, "CSVColumnHeaderOdd", { link = "CSVColumnOdd", })
      hi(0, "CSVColumnHeaderEven", { link = "CSVColumnEven", })
      -- highlight CSVDelimiter guifg=#aa0000 guibg=bg
      -- hi link CSVDelimiter Todo
    end, -- }}}
  },     -- }}}
  -- { 'chrisbra/vim-sh-indent', -- {{{
  --   -- until both vims fetch updates to their bundle
  --   -- ft = { "sh", "bash", "zsh", "ebuild", "gentoo-conf-d", "gentoo-init-d", "gentoo-env-d" },
  -- }, -- }}}
  {
    'PProvost/vim-ps1',     -- {{{
    ft = { "ps1", "psm1", "psd1", "ps1xml", "pssc", "psrc", "cdxml" },
  },                        -- }}}
  { 'msva/tdesktop-colorscheme.vim', ft = { "tdesktop-theme" } },
  {
    "https://gitlab.com/HiPhish/jinja.vim",
    -- "pearofducks/ansible-vim",
    ft = { "jinja2", "*.jinja2", },
    event = { "BufRead *.j2, *.jinja2", },
    -- "yaml.ansible", "ansible_hosts", }
  },
  {
    'gentoo/gentoo-syntax', -- {{{
    -- TODO: port hooks to lua (as bundled are slow as fuck)
    ft = {
      "ebuild", "gentoo-changelog", "gentoo-conf-d", "gentoo-env-d",
      "gentoo-init-d", "gentoo-make-conf", "gentoo-metadata",
      "gentoo-package-keywords", "gentoo-package-license",
      "gentoo-package-mask", "gentoo-package-properties",
      "gentoo-package-use", "gentoo-use-desc"
    }
  }, -- }}}
  { 'spacewander/openresty-vim',     ft = { "nginx", } },
  { 'dbrgn/librepcb.vim' },
  { 'robbles/logstash.vim' },
  {
    'lervag/vimtex', -- {{{
    ft = { "tex", "latex" },
    config = function()
      vim.g.vimtex_view_general_viewer = 'okular'
      vim.g.vimtex_view_general_options = '--unique file:@pdf\\#src:@line@tex'
      vim.g.vimtex_view_general_options_latexmk = '--unique'
    end,
  },                          -- }}}
  {
    'alpaca-tc/beautify.vim', -- {{{
    ft = { "html", "css", "js" },
    cmd = "Beautify",
  },                                         -- }}}
  { 'arzg/vim-rust-syntax-ext', ft = "rust" }, -- rust: syntax extension
  {
    'simrat39/rust-tools.nvim',              -- {{{ --- rust: loads of tools
    dependencies = { "neovim/nvim-lspconfig", },
    config = function() require"cfg.rust" end,
    ft = "rust",
  }, -- }}}
  -- { 'fatih/vim-go', -- {{{ -- XXX
  --   -- TODO: get rid of it, too noisy, duplicates LSP.
  --   -- Stoppers: functionality that I can't do with LSP
  --   config = function() require"cfg.vim-go" end,
  --   ft = {"go"},
  -- }, -- }}}
  {
    "ray-x/go.nvim",
    dependencies = {
      "ray-x/guihua.lua",
      "neovim/nvim-lspconfig",
      {
        "nvim-treesitter/nvim-treesitter",
        cond = function()
          local e = vim.fn.exepath"cc" or vim.fn.exepath"gcc" or vim.fn.exepath"clang"; return #e > 0
        end,
      },
    },
    ft = { "go", "gomod", },
    opts = {
      comment_placeholder = "  ",
      lsp_cfg = false, -- will use external
      dap_debug = true,
    },
    -- keys = {},
  },
  { 'rhysd/vim-clang-format',   ft = { 'cpp', 'c', 'h', 'hpp' } }, -- CPP
  -- { 'plasticboy/vim-markdown', ft = "markdown", },
  -- { 'iamcco/markdown-preview.nvim', -- {{{
  --   run = function() vim.fn['mkdp#util#install']() end,
  --   config = function() require"cfg.markdown-preview" end,
  --   ft = {"markdown"},
  -- }, -- }}}
  -- {'mzlogin/vim-markdown-toc', cmd = {'GenTocGFM'}},
  {
    'TimUntersberger/neogit', -- {{{
    dependencies = {
      'nvim-lua/plenary.nvim',
      'sindrets/diffview.nvim',

      -- Only one of these is needed, not both.
      -- "nvim-telescope/telescope.nvim", -- optional
      -- "ibhagwan/fzf-lua",              -- optional
    },
    config = function()
      local has = require"lib.lazyutil".has
      require'neogit'.setup{
        integrations = {
          diffview = has"diffview.nvim",
          telescope = has"telescope.nvim",
        },
        -- Change the default way of opening neogit
        kind = "floating",
        -- customize displayed signs
        signs = {
          -- { CLOSED, OPENED },
          section = { "", "" },
          item = { "", "" },
          -- hunk = {"", ""},
          hunk = { "", "" },
        },
        graph_style = "unicode",
      }
    end,
    cmd = "Neogit",
    keys = {
      { "<leader>gg", "<cmd>Neogit<cr>", desc = "Open NeoGit" }
    },
  }, -- }}}
  {
    "NvChad/nvim-colorizer.lua",
    event = { "BufReadPre", "BufNewFile" },
    -- event = { "VeryLazy" },
    -- "FileType"}, -- WARN: makes nvim-treesitter's feature of auto_install'ing parsers
    config = function() require"cfg.nvim-colorizer" end
  }
  -- {"brenoprata10/nvim-highlight-colors"}
  -- { 'RRethy/vim-hexokinase', -- {{{
  --   run = 'make',
  --   -- cmd = "HexokinaseToggle",
  --   setup = function() require"cfg.hexokinase" end,
  -- }, -- }}}
  -- ^^^ TODO: maybe switch to nvchad colorizer?
  -- TODO: comments colors
  --[[  use { 'Shougo/context_filetype.vim', -- {{{
      setup = function()
        vim.g['context_filetype#filetypes'] = {
          perl6 = {
            {
              filetype = 'pir',
              start = 'Q:PIR\\s*{',
              ['end']  = '}',
            }
          },
          vim = {
            {
              filetype = 'python',
              start = '^\\s*python <<\\s*\\(\\h\\w*\\)',
              ['end'] = '^\\1'
            },
            {
              filetype = 'lua',
              start = '^\\s*lua <<\\s*\\(\\h\\w*\\)',
              ['end'] = '^\\1'
            },
          },
        }
        vim.g['context_filetype#same_filetypes'] = {
          c = 'cpp,d', -- In c buffers, completes from cpp and d buffers.
          cpp = 'c',
          gitconfig = '_', -- In gitconfig buffers, completes from all buffers.
          ['_'] = '_', -- In default, completes from all buffers.
        }
        vim.g['context_filetype#ignore_composite_filetypes'] = {
            ['ruby.spec'] = 'ruby',
        }
      end,
    } -- }}}]]
}
