-- luacheck: globals vim T

return { -- [Bars ("Panels", "Lines")]
  {
    "romgrk/barbar.nvim",
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    config = function() require"cfg.barbar" end,
    event = "VimEnter",
    cond = false,
  },
  {
    "Bekaboo/dropbar.nvim",
    opts = {
      {
        general = {
          enable = false
        }
      }
    }
  },
  {
    "rebelot/heirline.nvim",
    config = function() require"cfg.heirline" end,
    -- event = {"VimEnter", "BufEnter"},
    event = { "VimEnter", },
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
  },
  {
    "nvim-neo-tree/neo-tree.nvim",
    -- branch = "v2.x",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons", -- not strictly required, but recommended
      "MunifTanjim/nui.nvim",
      {
        -- only needed if you want to use the commands with "_with_window_picker" suffix
        's1n7ax/nvim-window-picker',
        -- branch = "v1.*",
        config = function() require"cfg.nvim-window-picker" end,
      },
    },
    init = function()
      vim.g.neo_tree_remove_legacy_commands = 1
      if vim.fn.argc() == 1 then
        local stat = vim.loop.fs_stat(vim.fn.argv(0))
        if stat and stat.type == "directory" then
          require("neo-tree")
        end
      end
    end,
    config = function() require"cfg.neo-tree" end, --- @diagnostic disable-line: different-requires
    cmd = { "Neotree", "NeoTree" },
    deactivate = function()
      vim.cmd[[Neotree close]]
    end,
    module = "neo-tree",
    keys = {
      { "<leader>tt", "<cmd>Neotree toggle<cr>", mode = { "n", }, desc = "Toggle NeoTree" },
    },
  }
}
