-- luacheck: globals vim T req
return {
  -- auto pairs
  {
    "echasnovski/mini.pairs",
    -- nice thing, but not not comfortable (yet?) in my usecases.
    -- Switched to nvim-autopairs for now.
    enabled = false,
    event = {
      -- "VeryLazy",
      "InsertEnter",
    },
    config = function(_, opts)
      require"mini.pairs".setup(opts)
    end,
  },
  {
    "windwp/nvim-autopairs", -- {{{
    dependencies = {
      "iguanacucumber/magazine.nvim",
      -- "hrsh7th/nvim-cmp",
      "nvim-treesitter/nvim-treesitter"
    },
    config = function() --{{{
      require"cfg.autopairs"
    end,                -- }}}
    lazy = true,
    event = {
      -- "VeryLazy",
      "InsertEnter",
    },
  }, --}}}
  {
    "NvChad/menu",
    dependencies = {
      "nvchad/volt",
      "nvchad/minty",
    },
    keys = {
      {
        "<C-t>",
        function() require("menu").open("default") end,
        mode = "n",
        desc = "Open Menu (NvChad's)"
      },
      {
        "<RightMouse>",
        function()
          vim.cmd.exec'"normal! \\<RightMouse>"'

          local options = vim.bo.ft == "NvimTree" and "nvimtree" or "default"
          require("menu").open(options, { mouse = true })
        end,
        mode = "n",
        desc = "Open Menu (NvChad's)",
      },
    },
  },
  {
    "echasnovski/mini.surround",
    -- enabled = false,
    keys = function(_, keys)
      -- Populate the keys based on the user's options
      local plugin = require"lazy.core.config".spec.plugins["mini.surround"]
      local opts = require"lazy.core.plugin".values(plugin, "opts", false)
      local mappings = {
        { opts.mappings.add,            desc = "Add surrounding", mode = { "n", "v" } },
        { opts.mappings.delete,         desc = "Delete surrounding" },
        { opts.mappings.find,           desc = "Find right surrounding" },
        { opts.mappings.find_left,      desc = "Find left surrounding" },
        { opts.mappings.highlight,      desc = "Highlight surrounding" },
        { opts.mappings.replace,        desc = "Replace surrounding" },
        { opts.mappings.update_n_lines, desc = "Update `MiniSurround.config.n_lines`" },
      }
      mappings = vim.tbl_filter(function(m)
        return m[1] and #m[1] > 0
      end, mappings)
      return vim.list_extend(mappings, keys)
    end,
    opts = {
      mappings = {
        add = "gza",            -- Add surrounding in Normal and Visual modes
        delete = "gzd",         -- Delete surrounding
        find = "gzf",           -- Find surrounding (to the right)
        find_left = "gzF",      -- Find surrounding (to the left)
        highlight = "gzh",      -- Highlight surrounding
        replace = "gzr",        -- Replace surrounding
        update_n_lines = "gzn", -- Update `n_lines`
      },
    },
    config = function(_, opts)
      -- use gz mappings instead of s to prevent conflict with leap
      require"mini.surround".setup(opts)
    end,
  },
  --[[
  {
    "lukas-reineke/indent-blankline.nvim",
    enabled = false, -- for now, looks like indentscope is enough 🤷
    config = function()
      require"cfg.indent-blankline"
    end,
    event = "VeryLazy",
  },
  -- active indent guide and indent text objects
  {
    "echasnovski/mini.indentscope",
    event = { "BufRead", "VeryLazy", },
    opts = {
      -- symbol = "▏", -- makes cursor invisible
      symbol = "│",
      -- options = { try_as_border = true },
    },
    init = function()
      vim.api.nvim_create_autocmd("FileType", {
        pattern = { "help", "man", "alpha", "dashboard", "neo-tree", "Trouble", "lazy", "mason" },
        callback = function()
          vim.b.miniindentscope_disable = true
        end,
      })
    end,
    config = function(_, opts)
      require("mini.indentscope").setup(opts)
    end,
  },
  ]]
  {
    "shellRaining/hlchunk.nvim",
    event = { "BufReadPre", "BufNewFile" },
    config = function()
      require"cfg.hlchunk"
    end
  },
  {
    "wincent/loupe",
    event = { "VeryLazy", "CmdlineEnter", "ModeChanged", }
    -- adds \v on search, replace and other places on cmdline
    -- enabled = false,
  },

  {
    "numToStr/Comment.nvim",
    -- enabled = false,
    event = { "VeryLazy" },
    config = function() require"cfg.comment" end,
    dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
  },

  {
    "echasnovski/mini.comment",
    enabled = false, -- preferring "Comment" over this plugin, as it supports block comments.
    event = "VeryLazy",
    opts = {
      hooks = {
        pre = function()
          require("ts_context_commentstring.internal").update_commentstring({}) --- @diagnostic disable-line: missing-fields
        end,
      },
    },
    config = function(_, opts)
      require("mini.comment").setup(opts)
    end,
    dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
    --    keys = {"gcc", "gc"},
  },

  --[[
--   { "b3nj5m1n/kommentary", -- {{{ -- Not sure, whether mini's one is better, keep for now
--     enabled = false,
--     dependencies = { "JoosepAlviste/nvim-ts-context-commentstring" },
--     event = "VeryLazy",
--     config = function()
--       require"kommentary.config".configure_language("default", {
--         single_line_comment_string = "auto",
--         multi_line_comment_strings = "auto",
--         hook_function = function()
--           require"ts_context_commentstring.internal".update_commentstring()
--         end,
--       })
--       require"kommentary.config".use_extended_mappings()
--     end,
-- --    keys = { "gcc", "gc" }
--   }, -- }}}
]]
  --[[ -- better text-objects
  { "echasnovski/mini.ai",
    -- keys = {
    --   { "a", mode = { "x", "o" } },
    --   { "i", mode = { "x", "o" } },
    -- },
    event = "VeryLazy",
    --enabled = false,
    dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" },
    opts = function()
      local ai = require"mini.ai"
      if not ai then return {} end -- to make lua_ls linter happy
      return {
        n_lines = 500,
        custom_textobjects = {
          o = ai.gen_spec.treesitter({
            a = { "@block.outer", "@conditional.outer", "@loop.outer" },
            i = { "@block.inner", "@conditional.inner", "@loop.inner" },
          }, {}),
          f = ai.gen_spec.treesitter({ a = "@function.outer", i = "@function.inner" }, {}),
          c = ai.gen_spec.treesitter({ a = "@class.outer", i = "@class.inner" }, {}),
        },
      }
    end,
    config = function(_, opts)
      require"mini.ai".setup(opts)
      -- register all text objects with which-key
      local has = require"lib.lazyutil".has
      if has"which-key.nvim" then
        ---@type table<string, string|table>
        local i = {
          [" "] = "Whitespace",
          ['"'] = 'Balanced "',
          ["'"] = "Balanced '",
          ["`"] = "Balanced `",
          ["("] = "Balanced (",
          [")"] = "Balanced ) including white-space",
          [">"] = "Balanced > including white-space",
          ["<lt>"] = "Balanced <",
          ["]"] = "Balanced ] including white-space",
          ["["] = "Balanced [",
          ["}"] = "Balanced } including white-space",
          ["{"] = "Balanced {",
          ["?"] = "User Prompt",
          _ = "Underscore",
          a = "Argument",
          b = "Balanced ), ], }",
          c = "Class",
          f = "Function",
          o = "Block, conditional, loop",
          q = "Quote `, \", '",
          t = "Tag",
        }
        local a = vim.deepcopy(i)
        for k, v in pairs(a) do
          a[k] = tostring(v):gsub(" including.*", "")
        end

        local ic = vim.deepcopy(i)
        local ac = vim.deepcopy(a)
        for key, name in pairs({ n = "Next", l = "Last" }) do
          i[key] = vim.tbl_extend("force", { name = "Inside " .. name .. " textobject" }, ic)
          a[key] = vim.tbl_extend("force", { name = "Around " .. name .. " textobject" }, ac)
        end
        require"which-key".register({
          mode = { "o", "x" },
          i = i,
          a = a,
        })
      end
    end,
  }, ]]
  -- [[ global stuff (redefines, and so on) ]] -- {{{
  {
    "folke/noice.nvim", -- {{{
    --enabled = false,
    event = "VeryLazy",
    config = function()
      require"cfg.noice"
    end,
    dependencies = { -- {{{
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
      -- OPTIONAL:
      --   `nvim-notify` is only needed, if you want to use the notification view.
      --   If not available, we use `mini` as the fallback
      "rcarriga/nvim-notify",
    }, -- }}}
    keys = {
      {
        "<M-CR>",
        function() require"noice".redirect(vim.fn.getcmdline()) end, --- @diagnostic disable-line: different-requires
        mode = "c",
        desc = "Redirect Cmdline"
      },
      { "<leader>snl", function() require"noice".cmd("last") end,    desc = "(Noice) Last Message" }, --- @diagnostic disable-line: different-requires
      { "<leader>snh", function() require"noice".cmd("history") end, desc = "(Noice) History" }, --- @diagnostic disable-line: different-requires
      { "<leader>sna", function() require"noice".cmd("all") end,     desc = "(Noice) All" }, --- @diagnostic disable-line: different-requires
      { "<esc><esc>",  function() require"noice".cmd("dismiss") end, desc = "(Noice) Dismiss All" }, --- @diagnostic disable-line: different-requires
      {
        "<c-f>",
        function() if not require("noice.lsp").scroll(4) then return "<c-f>" end end,
        silent = true,
        expr = true,
        desc = "Scroll forward",
        mode = { "i", "n", "s" }
      },
      {
        "<c-b>",
        function() if not require("noice.lsp").scroll(-4) then return "<c-b>" end end,
        silent = true,
        expr = true,
        desc = "Scroll backward",
        mode = { "i", "n", "s" }
      },
    },
  },                        -- }}}
  {
    "rcarriga/nvim-notify", --{{{
    -- enabled = false,
    config = function()     -- {{{
      require"cfg.notify"
    end,                    -- }}}
    dependencies = {
      -- [[ Plenary.nvim ]] async and other yummy things.
      -- Actually it is mostly dependency for others, and still not use it myself in my scripts yet
      "nvim-lua/plenary.nvim",
    },
    init = function()
      -- when noice is not enabled, install notify on VeryLazy
      local Util = require"lib.lazyutil"
      if not Util.has"noice.nvim" then
        Util.on_very_lazy(function()
          vim.notify = require"notify" --- @diagnostic disable-line: different-requires
        end)
      end
    end,
  }, -- }}}
  { "nvim-lua/plenary.nvim" },

  {
    "jghauser/fold-cycle.nvim",
    lazy = true, -- loaded by keymap
    event = "VeryLazy",
    config = function() require"cfg.fold-cycle" end,
    keys = {
      "zc", "zo", "zC", "zO",
    },
    enabled = false,
  },
  -- { "Vonr/foldcus.nvim", event = "VimEnter", }, -- doesn't work 🤷
  {
    "kevinhwang91/nvim-ufo", -- folding {{{
    dependencies = {
      "kevinhwang91/promise-async",
      -- "nvim-treesitter/nvim-treesitter",
      -- "neovim/nvim-lspconfig",
    },
    config = function()
      require"cfg.ufo"
    end,
    event = {
      "BufReadPost",
    },
    keys = {
      { "zR", function() require"ufo".openAllFolds() end,  desc = "(UFO) Open all folds (in buffer)", }, --- @diagnostic disable-line: different-requires
      { "zM", function() require"ufo".closeAllFolds() end, desc = "(UFO) Close all folds (in buffer)", }, --- @diagnostic disable-line: different-requires
      {
        "zr",
        function() require"ufo".openFoldsExceptKinds() end, --- @diagnostic disable-line: different-requires
        desc = "(UFO) Open all folds (in buffer) except kinds",
      },
      { "zm", function() require"ufo".closeFoldsWith() end, desc = "(UFO) Close folds with level", }, --- @diagnostic disable-line: different-requires
      {
        "<leader>H",
        function()
          local w = require"ufo".peekFoldedLinesUnderCursor() --- @diagnostic disable-line: different-requires
          if not w then
            vim.lsp.buf.hover()
          end
        end,
        desc = "(UFO) Peek folded lines or show LSP's definition hover",
      },
    },
  },
  -- { "pseewald/vim-anyfold", cmd = {"AnyFoldActivate"}, }, -- also notice lsp-folding in LSP section
  {
    "beauwilliams/focus.nvim", -- {{{
    enabled = false,           -- Not using it anyway
    cmd = {
      "FocusSplitNicely", "FocusSplitCycle",
      "FocusSplitLeft", "FocusSplitRight",
      "FocusSplitDown", "FocusSplitUp",
    },
    config = function()
      require"focus".setup({ hybridnumber = true, bufnew = true })
    end,
    keys = {
      { "<C-Space><left>",  "<cmd>FocusSplitLeft<cr>",   desc = "(Focus) Split Left", },
      { "<C-Space><right>", "<cmd>FocusSplitRight<cr>",  desc = "(Focus) Split Right", },
      { "<C-Space><down>",  "<cmd>FocusSplitDown<cr>",   desc = "(Focus) Split Down", },
      { "<C-Space><up>",    "<cmd>FocusSplitUp<cr>",     desc = "(Focus) Split Up", },
      { "<C-Space><s>",     "<cmd>FocusSplitNicely<cr>", desc = "(Focus) Split Nicely", },
    },
  }, -- }}}
  {
    "junegunn/vim-easy-align",
    cmd = "EasyAlign",
    keys = {
      {
        "ga",
        -- function() end,
        "<Plug>(EasyAlign)",
        mode = {
          "n", -- gaip
          "x", -- vipga
        },
        desc = "EasyAlign"
      },
    },
  }, -- align
  --[[
  --  "RRethy/nvim-align", cmd = "Align", },
  --  { "Vonr/align.nvim", cmd = { "Align" }, keys = { "<leader>aa" }, },
  --  ~/.vcs_repos/align.nvim.lua
  --]]
  --[[ Specs.nvim. Somewhy doesn't work for me. TODO: check
  { "edluffy/specs.nvim", -- {{{
    config = function() --{{{
      require"cfg.specs"
    end, -- }}}
    event = "VeryLazy",
  }, -- }}}
  --]]
  { "mg979/vim-visual-multi",     keys = { "<S-Left>", "<S-Right>", "<C-Up>", "<C-Down>" }, }, -- multi cursor
  { -- Smear cursors (visible jumps)
    "sphamba/smear-cursor.nvim",
    event = { "VeryLazy", },
    opts = {
      -- Smear cursor when switching buffers or windows.
      smear_between_buffers = true,

      -- Smear cursor when moving within line or to neighbor lines.
      -- Use `min_horizontal_distance_smear` and `min_vertical_distance_smear` for finer control
      smear_between_neighbor_lines = false,

      -- Draw the smear in buffer space instead of screen space when scrolling
      scroll_buffer_space = true,

      -- Set to `true` if your font supports legacy computing symbols (block unicode symbols).
      -- Smears will blend better on all backgrounds.
      legacy_computing_symbols_support = true,

      -- Smear cursor in insert mode.
      -- See also `vertical_bar_cursor_insert_mode` and `distance_stop_animating_vertical_bar`.
      smear_insert_mode = true,

      -- Smear cursor color. Defaults to Cursor GUI color if not set.
      -- Set to "none" to match the text color at the target cursor position.
      cursor_color = "none",

      -- Faster!!
      --                             -- Default  Range
      stiffness = 0.8,               -- 0.6      [0, 1]
      trailing_stiffness = 0.5,      -- 0.3      [0, 1]
      distance_stop_animating = 0.5, -- 0.1      > 0
      -- trailing_exponent = 5,
      -- hide_target_hack = true,
      -- gamma = 1,

      -- transparent_bg_fallback_color = "#303030",
    },
  },
  {
    "3rd/image.nvim",
    build = false,
    -- 👆 so that it doesn't build the rock https://github.com/3rd/image.nvim/issues/91#issuecomment-2453430239
    -- TODO: consider to try "magic" luarock for FFI-powered integration
    event = { "VeryLazy", },
    config = function()
      require"image".setup{
        backend = "kitty",
        -- backend = "ueberzug",
        -- processor = "magick_rock",
        processor = "magick_cli",
        integrations = {
          markdown = {
            enabled = true,
            clear_in_insert_mode = true,
            download_remote_images = true,
            only_render_image_at_cursor = true,
            floating_windows = true, -- if true, images will be rendered in floating markdown windows
            filetypes = { "markdown", "vimwiki" }, -- markdown extensions (ie. quarto) can go here
          },
          neorg = {
            enabled = true,
            filetypes = { "norg" },
          },
          typst = {
            enabled = true,
            filetypes = { "typst" },
          },
          html = {
            enabled = true,
          },
          css = {
            enabled = true,
          },
        },
        max_width = nil,
        max_height = nil,
        max_width_window_percentage = nil,
        max_height_window_percentage = 50,
        window_overlap_clear_enabled = true, -- toggles images when windows are overlapped
        window_overlap_clear_ft_ignore = { "cmp_menu", "cmp_docs", "snacks_notif", "scrollview", "scrollview_sign" },
        editor_only_render_when_focused = true, -- auto show/hide images when the editor gains/looses focus
        tmux_show_only_in_active_window = true, -- auto show/hide images in the correct Tmux window (needs visual-activity off)
        hijack_file_patterns = { "*.png", "*.jpg", "*.jpeg", "*.gif", "*.webp", "*.avif" }, -- render image files as images when opened
      }
    end
  },
  { "nvim-lua/popup.nvim" },
  { "lambdalisue/suda.vim",       cmd = { "SudaWrite", "SudaRead" } }, -- reopen/force-write file when forget sudo
  --[[ { "famiu/nvim-reload", -- {{{
    cmd = {"Reload", "Restart"},
    dependencies = { "nvim-lua/plenary.nvim", },
  }, ]]
  { "ruanyl/vim-gh-line" },
  { "MattesGroeger/vim-bookmarks" },
  { "vim-scripts/genutils" },
  -- git signs
  {
    "lewis6991/gitsigns.nvim",
    event = { "BufReadPre", "BufNewFile" },
    config = function() require"cfg.gitsigns" end, --- @diagnostic disable-line: different-requires
    --[[
    opts = {
      on_attach = function(buffer)
        local gs = package.loaded.gitsigns

        local function map(mode, l, r, desc)
          vim.keymap.set(mode, l, r, { buffer = buffer, desc = desc })
        end

        -- stylua: ignore start
        map("n", "]h", gs.next_hunk, "Next Hunk")
        map("n", "[h", gs.prev_hunk, "Prev Hunk")
        map({ "n", "v" }, "<leader>ghs", ":Gitsigns stage_hunk<CR>", "Stage Hunk")
        map({ "n", "v" }, "<leader>ghr", ":Gitsigns reset_hunk<CR>", "Reset Hunk")
        map("n", "<leader>ghS", gs.stage_buffer, "Stage Buffer")
        map("n", "<leader>ghu", gs.undo_stage_hunk, "Undo Stage Hunk")
        map("n", "<leader>ghR", gs.reset_buffer, "Reset Buffer")
        map("n", "<leader>ghp", gs.preview_hunk, "Preview Hunk")
        map("n", "<leader>ghb", function() gs.blame_line({ full = true }) end, "Blame Line")
        map("n", "<leader>ghd", gs.diffthis, "Diff This")
        map("n", "<leader>ghD", function() gs.diffthis("~") end, "Diff This ~")
        map({ "o", "x" }, "ih", ":<C-U>Gitsigns select_hunk<CR>", "GitSigns Select Hunk")
      end,
    },
    ]]
  },
  -- { "mhinz/vim-signify", -- {{{
  --   setup = function()
  --     require"cfg.signify"
  --   end,
  --   event = {"VeryLazy",},
  -- }, -- }}}
  {
    "numtostr/FTerm.nvim",
    config = function() require"cfg.fterm" end,
    cmd = { "FTermToggle", "LazygitToggle" },
    keys = {
      { "<leader>lg", "<cmd>LazygitToggle<cr>", desc = "(LazyGit) Toggle", },
      -- { "<c-\\>", "", desc = "", },
      { "<c-\\>",     "<cmd>FTermToggle<cr>",   mode = { "t", "n", },      remap = false,         desc = "(FTerm) Toggle terminal", },
      { "<c-n>",      "<c-\\><c-n>",            remap = false,             desc = "(FTerm) ...?", },
    },
  },
  { "tmux-plugins/vim-tmux",         ft = "tmux", },
  { "embear/vim-localvimrc",         event = "VeryLazy", },
  -- { "sheerun/vim-polyglot", cond = false, }, -- takes too much time in `startuptime` test, not sure how to fix 🤷
  { "editorconfig/editorconfig-vim", event = "VeryLazy", },
  -- TODO: treesj and so on
  {
    "windwp/nvim-spectre",
    keys = {
      { "<leader>sr", function() require"spectre".open() end, desc = "Replace in files (Spectre)" },
    },
  },

  -- todo comments
  {
    "folke/todo-comments.nvim",
    cmd = { "TodoTrouble", "TodoTelescope" },
    event = { "BufReadPost", "BufNewFile" },
    opts = require"cfg.todo-comments",
    keys = {
      { "]t",         function() require("todo-comments").jump_next() end, desc = "Next todo comment" },
      { "[t",         function() require("todo-comments").jump_prev() end, desc = "Previous todo comment" },
      { "<leader>xt", "<cmd>TodoTrouble<cr>",                              desc = "Todo (Trouble)" },
      { "<leader>xT", "<cmd>TodoTrouble keywords=TODO,FIX,FIXME<cr>",      desc = "Todo/Fix/Fixme (Trouble)" },
      { "<leader>st", "<cmd>TodoTelescope<cr>",                            desc = "Todo" },
      { "<leader>sT", "<cmd>TodoTelescope keywords=TODO,FIX,FIXME<cr>",    desc = "Todo/Fix/Fixme" },
    },
  },
  -- references
  -- { "RRethy/vim-illuminate",
  --   enabled = false,
  --   event = { "BufReadPost", "BufNewFile" },
  --   opts = { delay = 200 },
  --   config = function(_, opts)
  --     require("illuminate").configure(opts)

  --     local function map(key, dir, buffer)
  --       vim.keymap.set("n", key, function()
  --         require("illuminate")["goto_" .. dir .. "_reference"](false)
  --       end, { desc = dir:sub(1, 1):upper() .. dir:sub(2) .. " Reference", buffer = buffer })
  --     end

  --     map("]]", "next")
  --     map("[[", "prev")

  --     -- also set it after loading ftplugins, since a lot overwrite [[ and ]]
  --     vim.api.nvim_create_autocmd("FileType", {
  --       callback = function()
  --         local buffer = vim.api.nvim_get_current_buf()
  --         map("]]", "next", buffer)
  --         map("[[", "prev", buffer)
  --       end,
  --     })
  --   end,
  --   keys = {
  --     { "]]", desc = "Next Reference" },
  --     { "[[", desc = "Prev Reference" },
  --   },
  -- },
  -- { "echasnovski/mini.bufremove",
  --   enabled = false,
  --   keys = {
  --     { "<leader>bq", function() require("mini.bufremove").delete(0, false) end, desc = "Delete Buffer" },
  --     { "<leader>bQ", function() require("mini.bufremove").delete(0, true) end, desc = "Delete Buffer (Force)" },
  --   },
  -- },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    opts = {
      plugins = { spelling = true },
      -- defaults = {
      --   mode = { "n", "v" },
      --   { "<leader><tab>", group = "tabs" },
      --   { "<leader>b", group = "buffer" },
      --   { "<leader>c", group = "code" },
      --   { "<leader>d", group = "debug" },
      --   { "<leader>da", group = "adapters" },
      --   { "<leader>f", group = "file/find" },
      --   { "<leader>q", group = "quit/session" },
      --   { "<leader>s", group = "search" },
      --   { "<leader>u", group = "ui" },
      --   { "<leader>w", group = "windows" },
      --   { "<leader>x", group = "diagnostics/quickfix" },
      --   { "]", group = "next" },
      --   { "[", group = "prev" },
      --   { "g", group = "goto" },
      --   { "gz", group = "surround" },
      --   -- { "<leader>g", group = "git" },
      --   -- { "<leader>gh", group = "hunks" },
      -- },
    },
    -- config = function(_, opts)
    --   local wk = require("which-key")
    --   wk.setup(opts)
    --   wk.register(opts.defaults)
    -- end,
    -- dependencies = { "echasnovski/mini.icons" },
  },
  -- easily jump to any location and enhanced f/t motions for Leap
  {
    "ggandor/flit.nvim",
    keys = function()
      local ret = {}
      for _, key in ipairs({ "f", "F", "t", "T" }) do
        ret[#ret + 1] = { key, mode = { "n", "x", "o" }, desc = key }
      end
      return ret
    end,
    opts = { labeled_modes = "nx" },
    events = "VeryLazy",
  },
  {
    "ggandor/leap.nvim",
    keys = {
      { "s",  mode = { "n", "x", "o" }, desc = "Leap forward to" },
      { "S",  mode = { "n", "x", "o" }, desc = "Leap backward to" },
      { "gs", mode = { "n", "x", "o" }, desc = "Leap from windows" },
    },
    config = function(_, opts)
      local leap = require("leap")
      for k, v in pairs(opts) do
        leap.opts[k] = v
      end
      leap.add_default_mappings(true)
      vim.keymap.del({ "x", "o" }, "x")
      vim.keymap.del({ "x", "o" }, "X")
      require"leap".opts.highlight_unlabeled_phase_one_targets = true
    end,
    dependencies = { "tpope/vim-repeat" },
    event = "VeryLazy",
  },
  -- better vim.ui (using noice, so no need)
  -- { "stevearc/dressing.nvim",
  --   lazy = true,
  --   init = function()
  --     ---@diagnostic disable-next-line: duplicate-set-field
  --     vim.ui.select = function(...)
  --       require("lazy").load({ plugins = { "dressing.nvim" } })
  --       return vim.ui.select(...)
  --     end
  --     ---@diagnostic disable-next-line: duplicate-set-field
  --     vim.ui.input = function(...)
  --       require("lazy").load({ plugins = { "dressing.nvim" } })
  --       return vim.ui.input(...)
  --     end
  --   end,
  --   event = "VeryLazy",
  --   enabled = false,
  -- },

  -- cursor animations
  {
    "echasnovski/mini.animate",
    enabled = false, -- lagging. Also, probably the source of folding-related rendering bugs
    event = "VeryLazy",
    opts = function()
      -- don't use animate when scrolling with the mouse
      local mouse_scrolled = false
      for _, scroll in ipairs({ "Up", "Down" }) do
        local key = "<ScrollWheel" .. scroll .. ">"
        vim.keymap.set({ "", "i" }, key, function()
          mouse_scrolled = true
          return key
        end, { expr = true })
      end

      local animate = require("mini.animate")
      return {
        resize = {
          timing = animate.gen_timing.linear({ duration = 100, unit = "total" }),
        },
        scroll = {
          timing = animate.gen_timing.linear({ duration = 150, unit = "total" }),
          subscroll = animate.gen_subscroll.equal({
            predicate = function(total_scroll)
              if mouse_scrolled then
                mouse_scrolled = false
                return false
              end
              return total_scroll > 1
            end,
          }),
        },
      }
    end,
    config = function(_, opts)
      require("mini.animate").setup(opts)
    end,
  },
  -- split/join code blocks
  -- { "Wansmer/treesj",
  --   enabled = false,
  --   keys = { "<Leader>m", "<Leader>j", "<Leader>s" },
  --   dependencies = { "nvim-treesitter/nvim-treesitter" },
  --   config = function()
  --     require("treesj").setup()
  --   end,
  -- },
  {
    "echasnovski/mini.splitjoin",
    keys = { "<leader>.m", "<leader>.j", "<leader>.s" },
    opts = {
      mappings = {
        toggle = "<leader>.m",
        split = "<leader>.s",
        join = "<leader>.j",
      },
    },
  },
  {
    "sindrets/diffview.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-tree/nvim-web-devicons",
    },
  },
  -- {"tpope/vim-sleuth", event="VeryLazy"}
  -- {"kevinhwang91/nvim-bqf"}
  -- { "anuvyklack/pretty-fold.nvim",
  --   event = "BufReadPost",
  -- },
  -- { "anuvyklack/fold-preview.nvim",
  --   event = "BufReadPost",
  -- },
  -- {"tiagovla/scope.nvim",event = "VeryLazy"} -- doesnt work with barbar
  -- {"mhartington/formatter.nvim"}
  -- }}}
  {
    "monaqa/dial.nvim",
    config = function() require"cfg.dial" end,
    keys = {
      {
        "<C-a>",
        function()
          require("dial.map").manipulate("increment", "normal")
        end,
        mode = { "i" },
        desc = "(Dial) Increment (insert mode)",
      },
      {
        "<C-x>",
        function()
          require("dial.map").manipulate("decrement", "normal")
        end,
        mode = { "i" },
        desc = "(Dial) Increment (insert mode)",
      },
      {
        "<C-a>",
        function()
          require("dial.map").manipulate("increment", "normal")
        end,
        mode = { "n" },
        desc = "(Dial) Increment (normal mode)",
      },
      {
        "<C-x>",
        function()
          require("dial.map").manipulate("decrement", "normal")
        end,
        mode = { "n" },
        desc = "(Dial) Increment (normal mode)",
      },
      {
        "g<C-a>",
        function()
          require("dial.map").manipulate("increment", "gnormal")
        end,
        mode = { "n" },
        desc = "(Dial) Increment (glob) (normal mode)",
      },
      {
        "g<C-x>",
        function()
          require("dial.map").manipulate("decrement", "gnormal")
        end,
        mode = { "n" },
        desc = "(Dial) Decrement (normal mode)",
      },
      {
        "<C-a>",
        function()
          require("dial.map").manipulate("increment", "visual")
        end,
        mode = { "v" },
        desc = "(Dial) Increment (visual mode)",
      },
      {
        "<C-x>",
        function()
          require("dial.map").manipulate("decrement", "visual")
        end,
        mode = { "v" },
        desc = "(Dial) Decrement (visual mode)",
      },
      {
        "g<C-a>",
        function()
          require("dial.map").manipulate("increment", "gvisual")
        end,
        mode = { "v" },
        desc = "(Dial) Increment (glob) (visual mode)",
      },
      {
        "g<C-x>",
        function()
          require("dial.map").manipulate("decrement", "gvisual")
        end,
        mode = { "v" },
        desc = "(Dial) Decrement (glob) (visual mode)",
      },
    },
  },
  {
    "folke/twilight.nvim",
    event = { "VeryLazy" },
    opts = {
      dimming = {
        alpha = 0.25, -- amount of dimming
        -- we try to get the foreground from the highlight groups or fallback color
        color = { "Normal", "#ffffff" },
        term_bg = "#000000", -- if guibg=NONE, this will be used to calculate text color
        inactive = false,    -- when true, other windows will be fully dimmed (unless they contain the same buffer)
      },
      context = 10,          -- amount of lines we will try to show around the current line
      treesitter = true,     -- use treesitter when available for the filetype
      -- treesitter is used to automatically expand the visible text,
      -- but you can further control the types of nodes that should always be fully expanded
      expand = { -- for treesitter, we we always try to expand to the top-most ancestor with these types
        "function",
        "method",
        "table",
        "if_statement",
      },
      exclude = {}, -- exclude these filetypes
    },
    cmd = { "Twilight", "TwilightEnable" },
  },
  -- { "brenton-leighton/multiple-cursors.nvim",
  --   opts = {
  --     enable_split_paste = true,
  --     pre_hook = function()
  --       vim.opt.cursorline = false
  --       vim.cmd("NoMatchParen")
  --     end,
  --     post_hook = function()
  --       vim.opt.cursorline = true
  --       vim.cmd("DoMatchParen")
  --     end,
  --   },
  --   keys = {
  --     {"<C-Down>", "<Cmd>MultipleCursorsAddDown<CR>", mode = {"n", "i"}},
  --     -- {"<C-j>", "<Cmd>MultipleCursorsAddDown<CR>"},
  --     {"<C-Up>", "<Cmd>MultipleCursorsAddUp<CR>", mode = {"n", "i"}},
  --     -- {"<C-k>", "<Cmd>MultipleCursorsAddUp<CR>"},
  --     {"<C-LeftMouse>", "<Cmd>MultipleCursorsMouseAddDelete<CR>", mode = {"n", "i"}},
  --     {"<Leader>a", "<Cmd>MultipleCursorsAddMatches<CR>", mode = {"n", "x"}},
  --     {"<Leader>A", "<Cmd>MultipleCursorsAddMatchesV<CR>", mode = {"n", "x"}},
  --     {"<Leader>d", "<Cmd>MultipleCursorsAddJumpNextMatch<CR>", mode = {"n", "x"}},
  --     {"<Leader>D", "<Cmd>MultipleCursorsJumpNextMatch<CR>"},
  --   },
  -- },
}
