-- luacheck: globals vim T

local telescope = require"lib.lazyutil".telescope

return {
  {
    "nvim-telescope/telescope.nvim",
    cmd = { "Telescope", "TelescopeToggle", },
    enabled = false,
    keys = {
      { "<leader>,",       "<cmd>Telescope buffers show_all_buffers=true<cr>",  desc = "Switch Buffer" },
      -- { "<leader>,", "<cmd>Telescope buffers show_all_buffers=true theme=get_dropdown<cr>", desc = "Switch Buffer" },
      { "<leader>/",       telescope("live_grep"),                              desc = "Grep (root dir)" },
      { "<leader>:",       "<cmd>Telescope command_history<cr>",                desc = "Command History" },
      { "<leader><space>", telescope("files"),                                  desc = "Find Files (root dir)" },
      -- find
      { "<leader>fb",      "<cmd>Telescope buffers<cr>",                        desc = "Buffers" },
      { "<leader>ff",      telescope("files"),                                  desc = "Find Files (root dir)" },
      { "<leader>fF",      telescope("files", { cwd = false }),                 desc = "Find Files (cwd)" },
      { "<leader>fr",      "<cmd>Telescope oldfiles<cr>",                       desc = "Recent" },
      -- git
      { "<leader>gc",      "<cmd>Telescope git_commits<CR>",                    desc = "commits" },
      { "<leader>gs",      "<cmd>Telescope git_status<CR>",                     desc = "status" },
      -- search
      { "<leader>sa",      "<cmd>Telescope autocommands<cr>",                   desc = "Auto Commands" },
      { "<leader>sb",      "<cmd>Telescope current_buffer_fuzzy_find<cr>",      desc = "Buffer" },
      { "<leader>sc",      "<cmd>Telescope command_history<cr>",                desc = "Command History" },
      { "<leader>sC",      "<cmd>Telescope commands<cr>",                       desc = "Commands" },
      { "<leader>sd",      "<cmd>Telescope diagnostics bufnr=0<cr>",            desc = "Document diagnostics" },
      { "<leader>sD",      "<cmd>Telescope diagnostics<cr>",                    desc = "Workspace diagnostics" },
      { "<leader>sg",      telescope("live_grep"),                              desc = "Grep (root dir)" },
      { "<leader>sG",      telescope("live_grep", { cwd = false }),             desc = "Grep (cwd)" },
      { "<leader>sh",      "<cmd>Telescope help_tags<cr>",                      desc = "Help Pages" },
      { "<leader>sH",      "<cmd>Telescope highlights<cr>",                     desc = "Search Highlight Groups" },
      { "<leader>sk",      "<cmd>Telescope keymaps<cr>",                        desc = "Key Maps" },
      { "<leader>sM",      "<cmd>Telescope man_pages<cr>",                      desc = "Man Pages" },
      { "<leader>sm",      "<cmd>Telescope marks<cr>",                          desc = "Jump to Mark" },
      { "<leader>so",      "<cmd>Telescope vim_options<cr>",                    desc = "Options" },
      { "<leader>sR",      "<cmd>Telescope resume<cr>",                         desc = "Resume" },
      { "<leader>sw",      telescope("grep_string"),                            desc = "Word (root dir)" },
      { "<leader>sW",      telescope("grep_string", { cwd = false }),           desc = "Word (cwd)" },
      { "<leader>uC",      telescope("colorscheme", { enable_preview = true }), desc = "Colorscheme with preview" },
      {
        "<leader>ss",
        telescope("lsp_document_symbols", {
          symbols = {
            "Class",
            "Function",
            "Method",
            "Constructor",
            "Interface",
            "Module",
            "Struct",
            "Trait",
            "Field",
            "Property",
          },
        }),
        desc = "Goto Symbol",
      },
      {
        "<leader>sS",
        telescope("lsp_dynamic_workspace_symbols", {
          symbols = {
            "Class",
            "Function",
            "Method",
            "Constructor",
            "Interface",
            "Module",
            "Struct",
            "Trait",
            "Field",
            "Property",
          },
        }),
        desc = "Goto Symbol (Workspace)",
      },
      -- -- {{{ Telecscope
      -- if has"telescope.nvim" then
      --   nmap("<leader>tf", "<cmd>lua require'telescope.builtin'.find_files{}<cr>")
      --   nmap("<leader>tg", "<cmd>lua require'telescope.builtin'.live_grep{}<cr>")

      --   local silent = { silent = true, noremap = true }
      --   -- Navigate buffers and repos
      --   nmap("<leader>tb", [[<cmd>Telescope buffers show_all_buffers=true theme=get_dropdown<cr>]], silent)
      --   nmap("<leader>tc", [[<cmd>Telescope commands theme=get_dropdown<cr>]], silent)
      --   -- nmap('<c-e>', [[<cmd>Telescope frecency theme=get_dropdown<cr>]], silent)
      --   nmap("<leader>tee", [[<cmd>Telescope git_files theme=get_dropdown<cr>]], silent)
      --   nmap("<leader>tef", [[<cmd>Telescope find_files theme=get_dropdown<cr>]], silent)
      --   nmap("<leader>teg", [[<cmd>Telescope live_grep theme=get_dropdown<cr>]], silent)
      -- end -- }}}
    },
    config = function()
      require"cfg.telescope"
    end,
    dependencies = {
      {
        "nvim-telescope/telescope-fzf-native.nvim",
        build = "make",
        config = function()
          require"telescope".load_extension"fzf" --- @diagnostic disable-line: different-requires
        end,
        cond = function()
          local e = vim.fn.exepath"cc" or vim.fn.exepath"gcc" or vim.fn.exepath"clang"; return #e > 0
        end,
      },
      'nvim-telescope/telescope-ui-select.nvim',
      'crispgm/telescope-heading.nvim',
      'nvim-telescope/telescope-file-browser.nvim',
    },
  },
}
