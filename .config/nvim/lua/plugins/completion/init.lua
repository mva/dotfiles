-- luacheck: globals vim T
return {
  -- Use <tab> for completion and snippets (supertab)
  -- first: disable default <tab> and <s-tab> behavior in LuaSnip
  {
    "L3MON4D3/LuaSnip",
    keys = function()
      return {}
    end,
    lazy = true,
    dependencies = {
      "rafamadriz/friendly-snippets",
    },
    build = "make install_jsregexp",
    config = function() require"cfg.luasnip" end,
  },
  {
    "iguanacucumber/magazine.nvim",
    -- "hrsh7th/nvim-cmp",
    config = function() require"cfg.cmp" end,
    lazy = true,
    event = { 'InsertEnter', 'CmdlineEnter', "LspAttach", },
    dependencies = {
      { "L3MON4D3/LuaSnip", },
      {
        "saadparwaiz1/cmp_luasnip",
        event = { "InsertEnter", }
      },
    },
  },
  {
    "onsails/lspkind-nvim",
    event = { "LspAttach", },
  },
  {
    "hrsh7th/cmp-nvim-lsp-document-symbol",
    event = { "LspAttach", },
  },
  {
    "hrsh7th/cmp-nvim-lsp-signature-help",
    event = { "LspAttach", },
  },
  {
    "hrsh7th/cmp-nvim-lsp",
    event = { "LspAttach", }, -- TODO:
  },
  {
    "ray-x/cmp-treesitter",
    event = { "InsertEnter", },
  },
  {
    "lukas-reineke/cmp-under-comparator",
    event = { "InsertEnter", },
    -- event = { "MenuPopup", },
  },
  {
    "JMarkin/cmp-diag-codes",
    event = { "LspAttach", },
  },
  {
    "lukas-reineke/cmp-rg",
    event = { "InsertEnter", },
  },
  {
    "hrsh7th/cmp-emoji",
    event = { "InsertEnter", },
  },
  {
    "hrsh7th/cmp-buffer",
    event = { "InsertEnter", "CmdlineEnter", },
  },
  -- { "hrsh7th/cmp-path",
  --   event = { "InsertEnter", "CmdlineEnter", },
  -- },
  -- { "FelipeLema/cmp-async-path",
  {
    "msva/cmp-path-ng",
    event = { "InsertEnter", "CmdlineEnter", },
  },
  {
    "hrsh7th/cmp-nvim-lua",
    event = { "InsertEnter", "CmdlineEnter", },
  },
  -- { "hrsh7th/cmp-calc",
  --   event = { "InsertEnter", "CmdlineEnter", },
  -- },
  {
    "hrsh7th/cmp-cmdline",
    event = { 'CmdlineEnter', },
  },
  {
    "andersevenrud/cmp-tmux",
    event = { "InsertEnter", }, -- TODO: check for bad behaviour
  },
  {
    "amarakon/nvim-cmp-lua-latex-symbols",
    event = { "InsertEnter", },
    ft = { "tex", "latex", } -- TODO: check for behaviour
  },
  {
    "dmitmel/cmp-cmdline-history",
    event = { 'CmdlineEnter', },
  },
  {
    "tzachar/fuzzy.nvim",
    dependencies = {
      -- { "romgrk/fzy-lua-native",
      {
        "msva/fzy-lua-native",
        build = "make"
      },
      -- or
      -- { "nvim-telescope/telescope-fzf-native.nvim",
      --   run = "make"
      -- },
    },
  },
  {
    "tzachar/cmp-fuzzy-buffer",
    event = { "InsertEnter", "CmdlineEnter", },
  },
  {
    "tzachar/cmp-fuzzy-path",
    event = { "InsertEnter", "CmdlineEnter", },
  },
  {
    "petertriho/cmp-git",
    config = function() require"cfg.cmp.git" end,
    dependencies = { "nvim-lua/plenary.nvim", },
    event = { "InsertEnter", },
    ft = { "gitcommit", },
  },
  {
    "tamago324/cmp-zsh",
    config = function() require"cfg.cmp.zsh" end,
    event = { "InsertEnter", },
    ft = { "zsh", },
  },
  -- { 'tzachar/cmp-tabnine', run='./install.sh', requires = 'hrsh7th/nvim-cmp' },
}
