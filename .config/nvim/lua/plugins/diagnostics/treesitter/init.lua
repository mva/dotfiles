-- luacheck: globals vim T
return {
  -- add more treesitter parsers
  {
    "JoosepAlviste/nvim-ts-context-commentstring",
    -- lazy = false,
    config = function() require"cfg.tree-sitter.context-commentstring" end,
  },
  {
    "nvim-treesitter/nvim-treesitter",
    config = function() require"cfg.tree-sitter" end,
    build = ":TSUpdate",
    dependencies = {
      "nvim-treesitter/nvim-treesitter-refactor",
      -- ^ try on large files.
      -- in case of poor performance, try
      -- 'tzachar/local-highlight.nvim'
      "nvim-treesitter/nvim-treesitter-textobjects",
      "RRethy/nvim-treesitter-textsubjects",
      -- "nvim-treesitter/playground", -- deprecated as nvim have it out the box
      "p00f/nvim-ts-rainbow",
      "RRethy/nvim-treesitter-endwise",
      {
        "nvim-treesitter/nvim-treesitter-context",
        config = function() require"cfg.tree-sitter.context" end,
      },
      "windwp/nvim-ts-autotag",
      "JoosepAlviste/nvim-ts-context-commentstring",
      {
        "andymass/vim-matchup",
        init = function()
          vim.g.matchup_matchparen_offscreen = {} -- method = "popup" }
          vim.g.matchup_transmute_enabled = 1
          vim.g.matchup_matchparen_deferred = 1
          vim.g.matchup_matchparen_hi_surround_always = 1
        end,
      },
    },
    -- lazy = false,
    event = "Syntax",
  },

  {
    "mfussenegger/nvim-treehopper",
    config = function()
      -- TODO: move to keys.lua
      local tsht = require"tsht"
      if not tsht then return end -- make lua_ls happy
      _G.omap("<leader>mm", tsht.nodes, { silent = true })
      _G.omap("<leader>ms", function() tsht.move{ side = "start" } end, { silent = true })
      _G.omap("<leader>me", function() tsht.move{ side = "end" } end, { silent = true })
      _G.xnoremap("<leader>mm", tsht.nodes, { silent = true })
      _G.xnoremap("<leader>ms", function() tsht.move{ side = "start" } end, { silent = true })
      _G.xnoremap("<leader>me", function() tsht.move{ side = "end" } end, { silent = true })
    end,
  },
  {
    'drybalka/tree-climber.nvim',
    config = function()
      -- TODO: move to keya.lua
      local tc = require"tree-climber"
      if not tc then return end -- make lua_ls happy
      local keyopts = { noremap = true, silent = true }
      vim.keymap.set({ 'n', 'v', 'o' }, 'H', tc.goto_parent, keyopts)
      vim.keymap.set({ 'n', 'v', 'o' }, 'L', tc.goto_child, keyopts)
      vim.keymap.set({ 'n', 'v', 'o' }, 'J', tc.goto_next, keyopts)
      vim.keymap.set({ 'n', 'v', 'o' }, 'K', tc.goto_prev, keyopts)
      vim.keymap.set({ 'v', 'o' }, 'in', tc.select_node, keyopts)
      vim.keymap.set('n', '<c-k>', tc.swap_prev, keyopts)
      vim.keymap.set('n', '<c-j>', tc.swap_next, keyopts)
      vim.keymap.set('n', '<c-h>', tc.highlight_node, keyopts)
    end,
  },
  {
    "ziontee113/syntax-tree-surfer",
    config = function() require"cfg.sts" end,
  },
  --[[
  { "folke/twilight.nvim",
    config = function() require"cfg.twilight" end,
  },
  ]]
  --[=[
  -- since `vim.tbl_deep_extend`, can only merge tables and not lists, the code above
  -- would overwrite `ensure_installed` with the new value.
  -- If you'd rather extend the default config, use the code below instead:
  {
    "nvim-treesitter/nvim-treesitter",
    opts = function(_, opts)
      -- add tsx and treesitter
      vim.list_extend(opts.ensure_installed, {
          "tsx",
          "typescript",
      })
    end,
  },
  --]=]
}
