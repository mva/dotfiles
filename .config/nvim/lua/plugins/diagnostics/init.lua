return {
  { import = "plugins.diagnostics.treesitter", },
  { import = "plugins.diagnostics.lsp", },
  { import = "plugins.diagnostics.dap", },
}
