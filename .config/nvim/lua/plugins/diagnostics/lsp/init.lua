-- luacheck: globals T vim
-- local has = require"lib.lazyutil".has

return {
  {
    "neovim/nvim-lspconfig",
    config = function() require"cfg.lsp" end,
    event = { "BufReadPost", },
    -- event = { "BufReadPre", "BufNewFile" },
    dependencies = {
      -- { "folke/neoconf.nvim", cmd = "Neoconf", config = true },
      -- { "folke/neodev.nvim", -- assumes lua_ls for nvim configs. Not using ATM
      --   opts = { experimental = { pathStrict = true } }
      -- },
      -- "jose-elias-alvarez/typescript.nvim",
      -- {"williamboman/mason.nvim"},
      -- {"williamboman/mason-lspconfig.nvim"},
      "lsp_lines.nvim", -- not sure if should be dep
      "b0o/schemastore.nvim",
      {
        "hrsh7th/cmp-nvim-lsp",
        -- cond = function()
        --   return has"nvim-cmp"
        -- end,
      },
      -- { "SmiteshP/nvim-navbuddy",
      --   dependencies = {
      --       "SmiteshP/nvim-navic",
      --       "MunifTanjim/nui.nvim",
      --       -- "numToStr/Comment.nvim",        -- Optional
      --   },
      --   opts = { lsp = { auto_attach = true } },
      --   cmd = "Navbuddy",
      -- },
    },
  },
  -- { "jose-elias-alvarez/null-ls.nvim",
  --   -- event = { "BufReadPost", },
  --   -- event = { "BufReadPre", "BufNewFile", },
  --   ft = { "javascript", "css", },
  --   dependencies = {
  --     "williamboman/mason.nvim",
  --     "nvim-lua/plenary.nvim",
  --   },
  --   opts = function()
  --     local nls = require"null-ls"
  --     local sources = {}
  --     if nls then
  --       sources = {
  --         -- nls.builtins.formatting.fish_indent,
  --         -- nls.builtins.diagnostics.fish,
  --         nls.builtins.diagnostics.eslint,
  --         -- nls.builtins.formatting.stylua,
  --         nls.builtins.formatting.shfmt,
  --         -- nls.builtins.completion.spell,
  --         -- nls.builtins.diagnostics.flake8,
  --       }
  --     end
  --     return {
  --       root_dir = require"null-ls.utils".root_pattern(".null-ls-root", ".neoconf.json", "Makefile", ".git"),
  --       sources = sources,
  --     }
  --   end,
  -- },
  {
    "williamboman/mason-lspconfig.nvim",
    dependencies = {
      "williamboman/mason.nvim",
    },
    lazy = true,
    config = function() require"cfg.mason.lspconfig" end,
    cond = function()
      return false
      -- TODO: condition when LSP servers and other stuff needs to be ensureinstalled
    end,
    -- event = { "VeryLazy", },
  },
  {
    "williamboman/mason.nvim",
    cmd = "Mason",
    keys = { { "<leader>cm", "<cmd>Mason<cr>", desc = "Mason" } },
    config = function() require"cfg.mason" end,
    lazy = true,
  },
  {
    "vigoux/ltex-ls.nvim",
    dependencies = { "neovim/nvim-lspconfig", },
    event = { "LspAttach", },
    enabled = false,
  },
  {
    "barreiroleo/ltex_extra.nvim",
    dependencies = { "vigoux/ltex-ls.nvim", },
    enabled = false,
  },
  {
    "p00f/clangd_extensions.nvim",
    dependencies = { "neovim/nvim-lspconfig", },
  },
  {
    "https://git.sr.ht/~whynothugo/lsp_lines.nvim",
    event = { "LspAttach", },
    keys = {
      {
        "<leader>ll",
        function()
          require"cfg.lsp.ui".toggle()
        end,
        desc = "(lsp_lines) Toggle LSP Lines view",
      },
    },
    -- config = function()
    --   require("lsp_lines").setup()
    -- end,
  },
  {
    "b0o/schemastore.nvim",
    event = { "LspAttach", },
  },

  -- { "mfussenegger/nvim-lint" }
  -- { 'ray-x/lsp_signature.nvim' }, -- can't remember why I disabled it. TODO: check
  -- { 'nvim-lua/lsp-status.nvim', }
}
