-- luacheck: globals vim T

return {
  {
    "mfussenegger/nvim-dap", -- {{{
    config = function()
      require"cfg.dap"
    end,
    dependencies = {
      {
        "rcarriga/nvim-dap-ui", -- {{{
        keys = {
          { "<leader>du", function() require("dapui").toggle({}) end, desc = "Dap UI" }
        },
        config = function()
          require"cfg.dap.ui"
        end,
      },                                   -- }}}
      {
        "theHamsta/nvim-dap-virtual-text", -- {{{
        config = function()
          require"cfg.dap.virtual-text"
        end,
      },                      -- }}}
      {
        "leoluz/nvim-dap-go", -- {{{
        config = function()
          require"cfg.dap.go"
        end,
      },                                -- }}}
      {
        "mfussenegger/nvim-dap-python", -- {{{
        config = function()
          require"cfg.dap.python"
        end,
      }, -- }}}
      {
        "jbyuki/one-small-step-for-vimkind",
        keys = {
          { "<leader>daL", function() require("osv").launch({ port = 8086 }) end, desc = "Adapter Lua Server" },
          { "<leader>dal", function() require("osv").run_this() end,              desc = "Adapter Lua" },
        },
        config = function()
          local dap = require("dap")
          dap.adapters.nlua = function(callback, config)
            callback({
              type = "server",
              host = config.host or "127.0.0.1", ---@diagnostic disable-line: undefined-field
              port = config.port or 8086, ---@diagnostic disable-line: undefined-field
            })
          end
          dap.configurations.lua = {
            {
              type = "nlua",
              request = "attach",
              name = "Attach to running Neovim instance",
            },
          }
        end,
      },
      -- mason.nvim integration
      {
        "jay-babu/mason-nvim-dap.nvim",
        dependencies = "mason.nvim",
        cmd = { "DapInstall", "DapUninstall" },
        opts = {
          -- Makes a best effort to setup the various debuggers with
          -- reasonable debug configurations
          automatic_setup = true,

          -- You can provide additional configuration to the handlers,
          -- see mason-nvim-dap README for more information
          handlers = {},

          -- You'll need to check that you have the required things installed
          -- online, please don't ask me how to install them :)
          ensure_installed = {
            -- Update this to ensure that you have the debuggers for the langs you want
          },
        },
      },
    },
  }, -- }}}
}
