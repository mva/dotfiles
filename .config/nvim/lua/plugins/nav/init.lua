-- luacheck: globals vim T req

-- {{{
print()
-- }}}

-- {{{
print()
-- }}}

-- {{{
-- moo
-- }}}

return {
  -- change trouble config
  {
    "folke/trouble.nvim",
    -- opts will be merged with the parent spec
    opts = { use_diagnostic_signs = true },
    lazy = true,
    cmd = { "Trouble", },
    keys = {
      {
        "<leader>xx",
        "<cmd>Trouble diagnostics toggle<cr>",
        desc = "Diagnostics (Trouble)",
      },
      {
        "<leader>xX",
        "<cmd>Trouble diagnostics toggle filter.buf=0<cr>",
        desc = "Buffer Diagnostics (Trouble)",
      },
      {
        "<leader>cs",
        "<cmd>Trouble symbols toggle focus=false<cr>",
        desc = "Symbols (Trouble)",
      },
      {
        "<leader>cl",
        "<cmd>Trouble lsp toggle focus=false win.position=right<cr>",
        desc = "LSP Definitions / references / ... (Trouble)",
      },
      {
        "<leader>xL",
        "<cmd>Trouble loclist toggle<cr>",
        desc = "Location List (Trouble)",
      },
      {
        "<leader>xQ",
        "<cmd>Trouble qflist toggle<cr>",
        desc = "Quickfix List (Trouble)",
      },
      { "<leader>xQ", "<cmd>TroubleToggle quickfix<cr>", desc = "Quickfix List (Trouble)" },
      {
        "[q",
        function()
          if require"trouble".is_open() then
            require"trouble".previous({ skip_groups = true, jump = true })
          else
            vim.cmd.cprev()
          end
        end,
        desc = "Previous trouble/quickfix item",
      },
      {
        "]q",
        function()
          if require"trouble".is_open() then
            require"trouble".next({ skip_groups = true, jump = true })
          else
            vim.cmd.cnext()
          end
        end,
        desc = "Next trouble/quickfix item",
      },
    },
  },
  -- add symbols-outline
  -- { "simrat39/symbols-outline.nvim",
  --   cmd = "SymbolsOutline",
  --   keys = {
  --     -- TODO: function(name,k) k=require"lazylazy.keymaps"..name; return k; end,
  --     { "<leader>cs", "<cmd>SymbolsOutline<cr>", mode = {"n"}, desc = "Symbols Outline" }
  --   },
  --   config = true,
  --   lazy = true,
  -- },
  {
    "hedyhli/outline.nvim",
    lazy = true,
    cmd = { "Outline", "OutlineOpen" },
    keys = { -- Example mapping to toggle outline
      { "<leader>o", "<cmd>Outline<CR>", desc = "Toggle outline" },
    },
    opts = {
      -- Your setup opts here
    },
  },
  {
    "stevearc/aerial.nvim",
    -- enabled = false,
    config = function() require"cfg.aerial" end, --- @diagnostic disable-line: different-requires
    -- opts = {},
    -- Optional dependencies
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      "nvim-tree/nvim-web-devicons"
    },
    keys = {
      { "<leader>aa", "<cmd>AerialToggle!<CR>", mode = { "n" }, desc = "Toggle Aerial", },
    },
  },
  {
    "pechorin/any-jump.vim", -- {{{
    config = function()
      vim.g.any_jump_window_width_ratio = 0.8
      vim.g.any_jump_window_height_ratio = 0.9
      vim.g.any_jump_disable_default_keybindings = 1
    end,
    cmd = { "AnyJump", "AnyJumpBack", "AnyJumpVisual", "AnyJumpLastResults", },
    keys = {
      { "<leader>j",  "<cmd>AnyJump<cr>",            mode = { "n", }, remap = false, desc = "AnyJump" },
      { "<leader>j",  "<cmd>AnyJumpVisual<cr>",      mode = { "x", }, remap = false, desc = "AnyJump (visual mode)" },
      { "<leader>ab", "<cmd>AnyJumpBack<cr>",        mode = { "n", }, remap = false, desc = "AnyJump back" },
      { "<leader>al", "<cmd>AnyJumpLastResults<cr>", mode = { "n", }, remap = false, desc = "AnyJump's last results" },
    },
  }, -- }}}
}
