-- luacheck: globals T
return {
  {
    "folke/lazy.nvim",
    -- lazy = false,
    dependencies = {
      "nvim-tree/nvim-web-devicons",
    },
    config = function() require"cfg.lazy" end,
    priority = 100000,
  },
  {
    "nvim-tree/nvim-web-devicons",
    config = function() require"cfg.nvim-web-devicons" end, --- @diagnostic disable-line: different-requires
  },
}
