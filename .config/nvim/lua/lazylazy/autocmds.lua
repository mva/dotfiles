-- luacheck: globals vim f nmap T

-- Some commands was took or inspired by the ones from LazyVim

-- [ Sugar ] {{{
-- local f = require"lib.funcs"

-- local luv = vim.loop
local au     = f.au
local au2    = f.au2

---@diagnostic disable-next-line
local aug    = f.aug -- luacheck: ignore

local api    = vim.api
local cmd    = vim.cmd
local fn     = vim.fn

-- local has    = fn.has
local exists = fn.exists

local empty  = fn.empty
local expand = fn.expand
local line   = fn.line

local g      = vim.g
local o      = vim.o
local b      = vim.b

---@diagnostic disable-next-line
local wo     = vim.wo -- luacheck: ignore
local bo     = vim.bo

local ol     = vim.opt_local

local env    = vim.env

local function nop() end
-- }}}

-- [[ Lazy Inspired Funcs ]] {{{
-- Check if we need to reload the file when it changed
au2({ "FocusGained", "TermClose", "TermLeave" }, {
  command = "checktime",
})

-- Highlight on yank
au("TextYankPost", "*", function()
  vim.highlight.on_yank()
end)

-- resize splits if window got resized
au({ "VimResized" }, "*", function()
  cmd[[tabdo wincmd =]]
end)

-- Try to jump to the last spot the cursor was at in a file when reading it. {{{
au("BufReadPost", "*", function()
  if not (g.leave_my_cursor_position_alone) then
    local mark = api.nvim_buf_get_mark(0, '"')
    local lcount = api.nvim_buf_line_count(0)
    if mark[1] > 0 and mark[1] <= lcount then
      pcall(api.nvim_win_set_cursor, 0, mark)
    end
  end
end)
-- }}}

-- [ Pre-Save mkdir (if not exist) ] {{{
local function MkNonExDir(file, buf)
  -- local file = vim.loop.fs_realpath(event.match) or event.match
  if empty(fn.getbufvar(buf, '&buftype')) > 0 and not (file:match'^%w+:/') then
    local dir = fn.fnamemodify(file, ':p:h')
    if fn.isdirectory(dir) == 0 then
      fn.mkdir(dir, 'p')
      api.nvim_set_current_dir(dir)
    end
  end
end

au("BufWritePre", "*", function() MkNonExDir(expand('<afile>'), expand('<abuf>')) end)
-- }}}

-- [[ Relative Numbering ]] {{{
if o.buftype ~= "help" and not (env.MANOPT) and (o.filetype ~= "man" and o.filetype ~= "help") then
  -- au BufEnter,FocusGained,InsertLeave * set relativenumber
  -- au BufLeave,FocusLost,InsertEnter   * set norelativenumber
  au("BufEnter", "*", function() if o.number then o.relativenumber = true end end)
  au("InsertLeave", "*", function() if o.number then o.relativenumber = true end end)
  au("BufLeave", "*", function() if o.number then o.relativenumber = false end end)
  au("InsertEnter", "*", function() if o.number then o.relativenumber = false end end)
end
-- }}}

au("BufWinLeave", "*.*", function() vim.cmd.mkview() end)
au({ "UIEnter", "BufWinEnter" }, "*.*", function() vim.cmd[[silent! loadview]] end)

-- [ Hooks ] {{{

local function on_AnyBufRead() -- {{{
  -- vim.cmd.args'%' -- HACK: Don't alert about "there are N files to edit"
  -- btw, adding it in VimEnter or UIEnter leads to bugs.
  -- Also, adding it even here (bufread), brakes netrw 🤔
  local bufnr = api.nvim_get_current_buf()
  local bufname = api.nvim_buf_get_name(bufnr)

  if not (bufname) or #bufname == 0 then
    bufname = expand('.')
  end
  b.filename = bufname

  --[[ local buftype = api.nvim_buf_get_option(bufnr, 'filetype')
	if not(buftype) or #buftype == 0 then
		buftype = fn.join(fn.split(fn.system(("file -s -p -L -z -b --mime-type %s 2>/dev/null"):format(bufname))))
		-- ^^^ takes 15ms startup time on calling time. TODO: Consider rewriting
		if buftype:match("cannot open") then
			buftype = "empty"
		end
	end
	b.filetype = buftype ]]
  --	if buftype:match("directory") then
  --		g.loaded_netrw = "v999"
  --		b.loaded_netrw = "v999"
  --[[
		local tree = require'nvim-tree'
		o.laststatus=0
		if #api.nvim_buf_get_name(bufnr) > 0 then
			cmd"bwipeout"
		end
		require'bufferline.state'.set_offset(31, 'FileTree')
		--tree.open()
		]]
  --	end
end -- }}}
au("BufRead", "*", on_AnyBufRead)

-- local function on_FileType(meta) --{{{
--     -- buf, event, file, id, match
--     local ft = meta.match
--     local hooks = T{
--       -- ebuild = function() end,
--     }
--     local h = hooks[ft]
--     if h and type(h) == "function" then h() end
-- --	return
-- end -- }}}
-- au("FileType", "*", on_FileType)

local function on_AnyBufEnter() -- {{{
  --[=[
	function open_url(url)
		vim.g.loaded_netrw=nil
		vim.g.loaded_netrwPlugin=nil
		vim.cmd[[runtime! plugin/netrwPlugin.vim]]
		-- return vim.fn['netrw#BrowseX'](url, vim.fn['netrw#CheckIfRemote']())
		return vim.fn['netrw#FileUrlEdit'](url)
	end
]=]
  local bufnr = api.nvim_get_current_buf()
  local bufname = api.nvim_buf_get_name(bufnr)
  if not (bufname) or #bufname == 0 then
    bufname = "<empty>"
  end
  b.filename = bufname
  local ft = bo.filetype or o.filetype -- NOTE: it is not always possible to detect filetype at bufenter phase
  local bt = bo.buftype or o.buftype
  local specials = {                   --- TODO: move to commonn place (from here and cfg.heirline.utils) and include from there
    buftype = {
      -- "nofile",
      "prompt",
      -- "help",
      "quickfix",
      "terminal",
    },
    filetype = {
      "^aerial$",
      "^alpha$",
      "^chatgpt$",
      "dashboard",
      "^DressingInput$",
      "^frecency$",
      "fugitive",
      "^git.*",
      "harpoon",
      -- "help",
      "^lazy$",
      "lspinfo",
      -- "man",
      "neo-tree",
      "^netrw$",
      "oil",
      "^TelescopePrompt$",
      "toggleterm",
      "Trouble",
      "^undotree$",
    },
  }

  for _, typ in ipairs(specials.buftype) do
    if bt:match(typ) then return end
  end
  for _, typ in ipairs(specials.filetype) do
    if ft:match(typ) then return end
  end
  if bt == "help"
  -- or ft == ""
  --             --- HACK: dirty hack, as some helps fails are going witht empty ft on BufEnter (and even bufread),
  --             ---       however later (when it's too late), they get their ft set correctly.
  --             --- TODO: check, if it will not collide with anything and explode.
  --             --- N.B.: collides witth trouble. So added check for "special", thaks to code in heirline
  then
    -- print(bo.buflisted, "::", ft, ",,",bt,"..",bufname)
    bo.buflisted = true
    cmd[[only]]
    -- if g.neotree_was_opened then
    -- g.neotree_help_was_opened = true
    -- end
  elseif ft == "man" then
    bo.buflisted = true
    return
  end
  -- if ft:match"neo%-tree" then
  -- if g.neotree_help_was_opened then
  -- cmd[[Neotree toggle]]
  -- end
  -- end
  if bufname:match"^%w+://%w+" then
    nop()
    --			open_url(bufname)
  else
    local wd = expand("%:p:h")
    if fn.isdirectory(wd) > 0 then
      api.nvim_set_current_dir(wd)
      if bufname == "<empty>" and bo.buflisted and not (g.neotree_was_opened) then
        g.neotree_was_opened = true
        -- cmd[[NeoTreeFloatToggle]] -- Open NeoTree if called "nvim" without args
      end
    end
  end
  --[[
	local set_bufline_offset = function(w, name)
		local blapi = require"bufferline.api"
		blapi.set_offset(w, name)
	end
	]]
  local ft_actions = T{
    -- ["NvimTree"] = function() set_bufline_offset(31, 'NvimTree') end, -- unhardcode, take value, or just remove
    -- I've chosen "float" way, so don't need this
    --[[
		["neo-tree"] = function()
			local nt = require"neo-tree"
			local w = nt.config.window.width
			local name = "NeoTree"
			local sp = (" "):rep((w-#name)/2)
			name = ("%s%s"):format(sp, name)
			set_bufline_offset(w+1, name)
		end,
		]]
  }
  local fta = ft_actions[ft]
  if not (fta) then
    o.laststatus = 3
    print('\n') -- hide remains after 'laststatus=0'
  else
    fta()
  end
  --[[ if not(bo.buflisted) and b.filename == "<empty>" then
    print("killme")
  end ]]
end -- }}}
au("BufEnter", "*", on_AnyBufEnter)

-- local function on_AnyBufLeave() -- {{{
-- --[[
-- 	local ft = o.filetype
-- 	if ft == 'neo-tree' then
-- 		require'bufferline.api'.set_offset(0)
-- 		return
-- 	end
--  ]]
-- end --}}}
-- au("BufLeave", "*", on_AnyBufLeave)

local function on_VimEnter() -- {{{
  if line"$" > 500 then      -- XXX: think about value
    o.icm = nil              -- disable interactive search and replace preview on big files
  end
end                          -- }}}
au("VimEnter", "*", on_VimEnter)

-- local function on_AnyBufNewFile() end
-- au("BufNewFile", "*", on_AnyBufNewFile)

-- }}}


-- [[ FileType-related ]] {{{

au("FileType", {
    "PlenaryTestPopup",
    "lspinfo", "qf",
    "spectre_panel", "startuptime", "tsplayground",
  },
  function(event)
    bo[event.buf].buflisted = false
    nmap("q", "<cmd>bwipeout<cr>", { buffer = event.buf, silent = true, })
  end
)

au("FileType", { "gitcommit", "markdown" }, function()
  ol.wrap = true
  ol.spell = true
end)

-- GPG
-- au("BufReadCmd", "*.{gpg,asc,pgp}", function() vim.cmd[[ call gnupg#init(1) | call gnupg#decrypt(1) ]] end)
au("FileReadCmd", "*.{gpg,asc,pgp}", function()
  vim.fn['gnupg#init'](0)
  vim.fn['gnupg#decrypt'](0)
end)
au("BufWriteCmd", "*.{gpg,asc,pgp}", function()
  if not vim.b.GPGCorrespondingTo then ---@diagnostic disable-line: undefined-field
    vim.fn['gnupg#init'](0)
    vim.fn['gnupg#encrypt'](0)
  end
end)
au("FileWriteCmd", "*.{gpg,asc,pgp}", function()
  if not vim.b.GPGCorrespondingTo then ---@diagnostic disable-line: undefined-field
    vim.fn['gnupg#init'](0)
    vim.fn['gnupg#encrypt'](0)
  end
end)

-- Autoupdate plugins on config save
-- au("BufWritePost", "*lua/plugins/*.lua", function() vim.cmd[[source <afile> | Lazy sync]] end)

--[[
au( -- can reduce performance a bit (by reconfiguring diagnostics too often 🤷)
  {
    "FileType", -- initially matched lazy, but only after cursor move 🤷
    "BufEnter", -- to match after return
    "TextChanged", -- needed to match lazy
  },
  {
    "*",
  },
  require"cfg.lsp.ui".reload
)
]]

-- [ Gentoo related ] {{{
au({
    "BufEnter",
    "BufNewFile",
    "BufRead",
  },
  "*-*/*/metadata.xml",
  function(meta)
    if meta.event == "BufNewFile" then
      au("User", "VeryLazy", function() vim.cmd.NewMetadata() end)
    end
    bo.sw = 2
    bo.ts = 2
    bo.sts = 2
    bo.et = true
  end
)

au({
    "BufEnter",
    "BufNewFile",
    "BufRead",
  },
  {
    "*-*/*/*-*.ebuild",
    "*.eclass",
  },
  function(meta)
    if meta.event == "BufNewFile" then
      -- vim.cmd.NewEbuild() -- slower than verylazy variant, freezes UI on start
      au("User", "VeryLazy", function() vim.cmd.NewEbuild() end)
    end
    bo.sw = 2
    bo.ts = 2
    bo.sts = 2
    bo.et = false
  end
)

-- }}}

-- [ C/C++ ] {{{
-- Use :make to compile C/C++, even without a makefile
au("FileType", "c", function() if (vim.fn.glob('[Mm]akefile') == "") then vim.o.mp = "clang -o %< %" end end)
au("FileType", "cpp", function() if (vim.fn.glob('[Mm]akefile') == "") then vim.o.mp = "clang++ -o %< %" end end)
-- }}}
--
-- [ Xorg settings ] {{{
au("BufWritePost", "~/.Xdefaults", function()
  vim.cmd.redraw()
  vim.cmd.system('xrdb ' .. vim.fn.expand('<amatch>'))
end)
-- }}}

-- [ Y(A)ML (and ansible (+jinja2)) ] {{{
au({
    "BufEnter",
    "BufNewFile",
    "BufRead",
  }, {
    "*.sls",
    "*.yml",
    "*.yaml",
  },
  function()
    bo.ft = "yaml"
    bo.sw = 2
    bo.ts = 2
    bo.sts = 2
    bo.et = true
  end
)
au({
    "BufEnter",
    "BufNewFile",
    "BufRead",
  }, {
    "*/playbooks/*.yml",
    "*/playbooks/*.yaml",
  },
  function()
    bo.ft = "yaml.ansible"
    bo.sw = 2
    bo.ts = 2
    bo.sts = 2
    bo.et = true
  end
)
au({
    "BufEnter",
    "BufNewFile",
    "BufRead",
  }, {
    "*.j2",
    "*.jinja2",
  },
  function()
    -- bo.ft = "jinja"
    bo.sw = 2
    bo.ts = 2
    bo.sts = 2
    bo.et = false
  end
)
-- }}}

-- [ *Makefile* ] {{{
au("BufEnter", "?akefile*", function()
  bo.et = false
  bo.cindent = false
end)
-- }}}

-- [ JS ] {{{
au({
    "BufRead",
    "BufNewFile",
  },
  "*.js",
  function()
    bo.ft = "javascript.jquery"
  end
)
-- }}}

-- [ CSS ] {{{
au({
    "BufRead",
    "BufNewFile",
  },
  "*.css",
  function()
    bo.ft = "css"
    bo.syntax = "css"
  end
)
-- }}}

-- [ PHP ] {{{
au({
    "BufRead",
    "BufNewFile",
  },
  "*.php",
  function()
    -- bo.foldmethod="manual";
    -- bo.fen = true
    b.php_sql_query = 1     -- подсветка SQL запросов
    b.php_htmlInStrings = 1 -- подсветка HTML кода в PHP строках
    b.php_folding = 1       -- фолдинг функций и классов
  end
)
-- }}}
-- [ DOC ] {{{
au("BufReadPre", "*.doc", function() bo.ro = true end)
-- au("BufReadPre", "*.doc", function() bo.hlsearch = not(o.hlsearch) end)
au("BufReadPost", "*.doc", function() cmd[[%!antiword "%"]] end)
-- }}}

-- [ CSV ] {{{
au("BufRead", "*.csv", function() bo.ft = "csv" end)

au({
    "BufRead",
    "BufWritePost",
  },
  "*.csv",
  function()
    if line'$' < 100 then
      cmd[[%ArrangeColumn]]
    end
  end
)

au("BufWritePre", "*.csv", function()
  if line'$' < 100 then
    cmd[[%UnArrangeColumn]]
  end
end)
-- }}}
-- [ nginx (in portage build dir) ] {{{
au("BufRead", "*portage*nginx*work*/config", function()
  bo.ft = "nginx"
  bo.sw = 2
  bo.ts = 2
end)
-- }}}
-- }}}

au("BufRead", "*/portage*.conf", function()
  bo.commentstring = "# %s"
  bo.sw = 2
  bo.ts = 2
end)

au("FileWritePost", "*", function() b.filetype = nil end)
au("VimLeave", "*",
  function()
    -- TODO: either invent a way to get original cursor shape before entering NVim,
    -- or keep in sync with terminal settings somehow
    o.guicursor = "a:ver1-Cursor/lCursor-blinkon1"
  end
)

au({ "BufReadPre", "FileReadPre", "FileType" }, "*", function()
  -- NOTE: see also cfg/tree-sitter/init.lua and disabling conditions there
  if fn.getfsize(expand("%")) > 100 * 1024 then -- 100 kb
    if exists(':TSBufDisable') then
      vim.cmd'TSBufDisable autotag'
      vim.cmd'TSBufDisable highlight'
      vim.cmd'TSBufDisable incremental_selection'
      vim.cmd'TSBufDisable indent'
      vim.cmd'TSBufDisable playground'
      vim.cmd'TSBufDisable query_linter'
      vim.cmd'TSBufDisable rainbow'
      vim.cmd'TSBufDisable refactor'
      vim.cmd'TSBufDisable matchup'
      vim.cmd'TSBufDisable refactor.highlight_definitions'
      vim.cmd'TSBufDisable refactor.navigation'
      vim.cmd'TSBufDisable refactor.smart_rename'
      vim.cmd'TSBufDisable refactor.highlight_current_scope'
      vim.cmd'TSBufDisable textobjects.swap'
      vim.cmd'TSBufDisable textobjects.lsp_interop'
      vim.cmd'TSBufDisable textobjects.select'
      -- vim.cmd 'TSBufDisable textobjects.move'
    end
    o.foldmethod = "manual"
  end
end)
