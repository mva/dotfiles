-- luacheck: globals vim T

if not (vim.env.firstrun) then
  local opt      = vim.opt
  local cmd      = vim.cmd
  local fn       = vim.fn
  local g        = vim.g
  -- local go       = vim.go
  -- local f = require'lib.funcs'

  local data     = fn.stdpath("data")
  local cache    = fn.stdpath("cache")

  local enabled  = T{
    --  "autowrite", -- autosave
    --  "confirm", -- autoconfirm save before leaving buf
    "modeline",
    --  "modifiable",
    "ttyfast",
    "hidden",
    "showmatch",
    "ignorecase",
    "smartcase", -- Don't ignore case with capitals
    "hlsearch",
    "incsearch",
    "showmatch",
    "relativenumber",
    "number",
    -- "ruler",
    -- "title",
    -- "cursorline", -- Enable highlighting of the current line
    -- "cursorcolumn",
    "visualbell",
    "showfulltag",
    "writeany",
    "termguicolors",
    "expandtab",  -- tabs -> spaces
    "shiftround", -- Round indent
  }

  local disabled = T{
    "compatible",
    "showmode",
    -- "showcmd",
    "autoindent",
    "smartindent", -- Insert indents automatically
    "cindent",
    -- "wrapscan",
    "wrap",
    -- "number",
    -- "relativenumber",
  }

  cmd[[filetype on]]
  cmd[[filetype plugin on]]
  cmd[[filetype indent on]]
  cmd[[syntax on]]

  -- local enc = 'utf-8'
  -- opt.termencoding = enc -- removed
  -- opt.encoding = enc
  -- opt.fileencoding = enc
  opt.fileencodings = {
    "utf-8",
    "cp1251",
    "koi8-r",
    "cp866",
    "latin1"
  }

  opt.mouse = "ar" --{}--"a" -- nv
  opt.scrolljump = 5
  opt.scrolloff = 3
  opt.matchtime = 2

  opt.formatoptions = "qn1crjol"
  --[=[ ^^^^^^
    q = Format text with gq, but don't format as I type.
    n = gq recognizes numbered lists, and will try to
    1 = break before, not after, a 1 letter word
    c = auto-wrap comments using textwidth, inserting comment leader automatically
    r = Automatically insert the current comment leader after hitting <Enter> in Insert mode
    t = auto-wrap using textwidth
    j = Where it makes sense, remove a comment leader when joining lines.

    see :help fo-table

    default = "tcqj"
  --]=]
  opt.nrformats = {
    "bin",
    "hex",
    "alpha",
    "octal"
  }                       -- maybe disable alpha? or even hex? (and I also never used octal in real life, IIRC)

  opt.shada:append"r/tmp" -- don't save backups and swaps for stuff in /tmp

  if vim.env.VIM_NOBACKUP then
    opt.shada:append(("n/tmp/.%s.shada"):format(vim.env.USER))
    disabled:insert"backup"
    disabled:insert"swapfile"
    disabled:insert"undofile"
  else
    local backupdir = ("%s/backup"):format(cache)
    if not fn.isdirectory(backupdir) then
      fn.mkdir(backupdir, "p")
    end
    opt.backupdir = backupdir ---@diagnostic disable-line
    opt.directory = ("%s/swap"):format(data) ---@diagnostic disable-line
    -- opt.undodir = ("%s/undo"):format(cache)
    enabled:insert"writebackup"
    enabled:insert"backup"
    enabled:insert"undofile"
    opt.updatetime = 100 -- Save swap file and trigger CursorHold
    opt.backupext = "~"
    opt.backupskip = {}
  end

  opt.foldcolumn = "0"
  opt.signcolumn = "no" -- "number"
  -- opt.numberwidth = 1
  opt.matchpairs:append"<:>"
  opt.tabstop = 2
  opt.shiftwidth = 2
  opt.backspace = {
    "indent",
    "eol",
    "start",
  }

  opt.whichwrap = T{ ---@diagnostic disable-line
    "b", "s", -- {back,}space
    "<", ">", -- <left>/<right> in N and V
    "[", "]", -- <left>/<right> in I and R
  }:concat()

  opt.history = 10000
  opt.undolevels = 10000
  opt.laststatus = 3
  -- opt.laststatus = 0

  opt.foldmethod = "manual" -- "expr" -- indent, marker, syntax
  -- opt.foldexpr="MyFoldexpr(v:lnum)"
  -- opt.foldtext="MyFoldtext()"

  opt.foldlevel = 99
  opt.foldlevelstart = 99
  opt.foldenable = true

  opt.viewoptions = {
    -- "folds",
    -- "options",
    "cursor",
    "unix",
    "slash",
  }
  opt.virtualedit = "onemore"

  --[=[
  opt.completeopt = {
    "menuone",
    "noselect",
    "noinsert",
  }
  opt.completeopt:remove("preview")
  --]=]
  opt.completeopt = {
    "menu",
    "menuone",
    "noselect",
  } -- recommended by nvim-cmp readme

  -- let @/=''

  opt.sessionoptions:append"tabpages"
  opt.sessionoptions:append"globals"

  -- Just in case it will be needeed whenever:
  -- opt.guicursor = nil

  local term = vim.env.TERM or "none"
  -- TODO: check, maybe not needed anymore!
  -- hack to stop nvim to detect Konsole when in tmux
  -- (I made cursor management stuff in tmux instead).

  --[=[
  if term:match"^tmux*" then
    cmd([[unlet $KONSOLE_DBUS_SERVICE | unlet $KONSOLE_DBUS_SESSION | ]]
      .. [[unlet $KONSOLE_PROFILE_NAME | unlet $KONSOLE_VERSION]])
  end
  ]=]

  if term == "linux" then ---@diagnostic disable-line
    opt.background = "dark"
    -- colorscheme desert
  end
  --[=[
    opt.t_SI = "\<Esc>]12;purple\x7"
    opt.t_EI = "\<Esc>]12;blue\x7"
  --]=]
  for _, e in ipairs(enabled) do
    opt[e] = true
  end
  for _, d in ipairs(disabled) do
    opt[d] = false
  end





  -- opt.clipboard = "unnamedplus" -- Sync with system clipboard
  opt.conceallevel = 3 -- Hide * markup for bold and italic

  opt.grepformat = "%f:%l:%c:%m"
  opt.grepprg = "rg --vimgrep"

  opt.inccommand = "nosplit" -- preview incremental substitute. Empty string to disable.
  -- opt.pumblend = 10          -- Popup blend
  -- opt.pumheight = 10         -- Maximum number of entries in a popup
  opt.scrolloff = 4     -- Lines of context
  opt.sidescrolloff = 8 -- Columns of context
  -- opt.spelllang = { "en", "ru" }
  --opt.splitbelow = true -- Put new windows below current
  --opt.splitright = true -- Put new windows right of current
  opt.timeoutlen = 300
  opt.wildmode = "longest:full,full" -- Command-line completion mode
  opt.winminwidth = 5                -- Minimum window width

  opt.shortmess = "filmnrxoOtTaFWIcqsA"
  opt.shortmess:append{ W = true, I = true, c = true } --- @diagnostic disable-line: undefined-field
  if vim.fn.has("nvim-0.9.0") == 1 then
    --  opt.splitkeep = "screen"
    opt.shortmess:append{ C = true } --- @diagnostic disable-line: undefined-field
  end





  -- Fix markdown indentation settings
  g.markdown_recommended_style = 0

  g.colorcol = 80
  g.python_host_prog = nil
  g.python3_host_prog = "/usr/bin/python3"

  -- I dont't use ruby/perl/js providers
  g.loaded_ruby_provider = 0
  g.loaded_perl_provider = 0
  g.loaded_node_provider = 0

  -- TODO: move to filetype-specific config?
  g.vimpager_disable_x11 = 1
  g.vimpager_passthrough = 1

  -- TODO: move to "plugins settings"?
  g.localvimrc_name = {
    ".local.vimrc",
    ".vimrc"
  }
  g.localvimrc_persistent = 2
  g.localvimrc_persistence_file = ("%s/localvimrc.persistent"):format(data)

  g.vimsyn_embed = "lPr"   -- highlight embedded (l)ua, (P)ython and (r)uby syntax
  g.vimsyn_folding = "afP" -- support (a)utogroups, (f)unctions, (P)ython-scripts folding
  -- g.vimsyn_noerror = 1

  g.netrw_http_cmd = "curl -sLo"
  g.netrw_ftp_cmd = "curl -sLo"
  g.netrw_dav_cmd = "curl -sLo"
  -- g.netrw_sftp_cmd = "curl -sLo" -- NOTE: ignores openssh config (so requires username and port in URL)
  -- g.netrw_ssh_cmd = "curl -sLo"
  -- g.netrw_scp_cmd = "curl -sLo"
  g.netrw_menu = 0
  g.netrw_use_errorwindow = 0
  g.netrw_silent = 1
  g.netrw_bufsettings = 'nohidden noma nomod nonu nowrap ro buflisted'

  -- https://www.reddit.com/r/neovim/comments/107ms1s/comment/j3ngdxr/?utm_source=reddit&utm_medium=web2x&context=3
  -- vim.o.foldcolumn = '1'
  -- vim.o.foldlevel = 99
  -- vim.o.foldlevelstart = 99
  -- vim.o.foldenable = true
  vim.o.fillchars = [[eob: ,fold: ,foldopen:,foldsep: ,foldclose:]]
  -- vim.o.statuscolumn = "%=%l%s%C"
  --

  -- https://github.com/JoosepAlviste/nvim-ts-context-commentstring
  g.skip_ts_context_commentstring_module = true
end

-- vim: ts=2 sw=2 et
