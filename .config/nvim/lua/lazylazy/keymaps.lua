-- luacheck: no max line length
-- luacheck: globals vim
-- luacheck: globals nnoremap map imap vmap noremap inoremap nmap xmap xnoremap tmap tnoremap smap omap _map

-- keep the link to look for smething there: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua

local has = require"lib.lazyutil".has

-- local function lazymap(lhs, rhs, opts)
--   local keys = require"lazy.core.handler".handlers.keys
--   ---@cast keys LazyKeysHandler
--   -- do not create the keymap if a lazy keys handler exists
--   local mode = opts.mode
--   if not keys.active[keys.parse({ lhs, mode = mode }).id] then
--     opts = opts or {}
--     opts.mode = nil
--     opts.silent = opts.silent ~= false
--     _map(mode, lhs, rhs, opts)
--   end
-- end


-- TODO:
-- if has(plugin) then
--   local m = require("keymaps"..plugin)
--   for _,mm in ipairs(m) do lazymap(mm) end
-- end



-- {{{ Global Maps

nnoremap("<space>", "<nop>")
-- nnoremap('<C-/>', '<space>')
-- map('<space>', '<leader>')
--vim.g.mapleader = " "
--vim.g.localmapleader = " "

imap("<c-z>", "<c-o>u")
imap("<c-y>", "<c-o><c-r>")
vmap("<c-z>", "<esc>uv")
vmap("<c-y>", "<esc><c-r>v")
map("<c-z>", "u")
map("<c-y>", "<c-r>")

-- "map <Char-0x1b> z-
-- imap("<c-a>", "<c-o><c-a>")
-- imap("<c-x>", "<c-o><c-x>")

-- " C-c and C-v - Copy/Paste into "global clipboard"
vmap("<c-c>", '"+yi')
imap("<c-v>", '<c-o>"+gP') -- gPa (?)

-- " make shift-insert behave like in Xterm
map("<s-insert>", "<MiddleMouse>")

-- " make <c-l> clear the highlights as well as redraw
noremap("<c-l>", "<cmd>set nohls<bar>redraw<cr>", { silent = true })

noremap("<c-left>", "b")
--noremap('<c-right>', 'w')
noremap("<c-right>", "e<Right>")
inoremap("<c-right>", "<c-o>e<Right>")

-- map('<F9>', '^i#<Esc>j') -- Comment line (poor man's `kommentary`)

-- nnoremap("<c-space>", "<c-w>")
-- nmap("<c-w>", "i<c-w><esc>")

-- map('<c-u>', 'i<c-u><esc>')
map(
  "<PageDown>",
  "<cmd>set scroll=0<CR><cmd>set scroll^=2<CR><cmd>set scroll-=1<CR><C-D><cmd>set scroll=0<CR>",
  { silent = true }
)
map(
  "<PageUp>",
  "<cmd>set scroll=0<CR><cmd>set scroll^=2<CR><cmd>set scroll-=1<CR><C-U><cmd>set scroll=0<CR>",
  { silent = true }
)

local function _copymode()
  if has'mini.indentscope' then
    vim.b.miniindentscope_disable = not (vim.b.miniindentscope_disable)
    local ii = require"mini.indentscope"
    ii.undraw()
    ii.draw()
  elseif has'hlchunk.nvim' then
    for _, mod in ipairs({ "Chunk", "Indent" }) do -- blank, line_num
      if not (vim.b.hlchunk_disabled) then
        vim.cmd(("DisableHL%s"):format(mod))
      else
        vim.cmd(("EnableHL%s"):format(mod))
      end
    end
    vim.b.hlchunk_disabled = not (vim.b.hlchunk_disabled)
  end
  vim.o.list = not (vim.wo.list)
  vim.b.hide_statuscolumn = not (vim.b.hide_statuscolumn)
  vim.o.number = not (vim.o.number)                  -- b is not enough
  vim.o.relativenumber = not (vim.o.relativenumber)  -- same
  vim.o.statuscolumn = nil                           -- HACK:
end

-- nnoremap("<F11>", "<cmd>lua _copymode()<CR>", { silent = true })
-- inoremap("<F11>", "<cmd>lua _copymode()<CR>", { silent = true })
-- nnoremap("<F11>", _copymode, { silent = true })
-- inoremap("<F11>", _copymode, { silent = true })
_map(
  { "n", "i" },
  "<F11>",
  _copymode,
  {
    silent = true,
    remap = false,
  }
)

map("<F12>", "<cmd>set paste!<cr>")
imap("<F12>", "<cmd>set paste!<cr>")


local function _smartquit()
  -- it is better than Sayonara 🤷
  -- TODO: test properly with mans, helps and so on
  -- NB: for i,b in ipairs(vim.api.nvim_list_bufs()) do print(i,vim.fn.buflisted(b)) end
  if #vim.fn.getbufinfo({ buflisted = 1 }) > 1 then
    -- not sure if it is proper algorythm. Maybe the one commented above is better.
    vim.cmd.bwipeout() -- or bdelete?
  else
    --   if #api.nvim_buf_get_name(bufnr) > 0 then
    vim.cmd.q()
  end
end


-- map("q", "<cmd>q<CR>", { silent = true })
map("q", _smartquit, { silent = true })
map("Q", "<cmd>q!<CR>", { silent = true })
map("w", "<cmd>w<CR>", { silent = true })
map("W", "<cmd>w!<CR>", { silent = true })
map("<m-q>", "<cmd>qa!<CR>", { silent = true })



-- https://github.com/mhinz/vim-galore#saner-behavior-of-n-and-n
-- nmap("n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
-- xmap("n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
-- omap("n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
-- nmap("N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
-- xmap("N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })
-- omap("N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })

noremap("n", "'Nn'[v:searchforward]", { expr = true, desc = "Next search result" })
noremap("N", "'nN'[v:searchforward]", { expr = true, desc = "Prev search result" })


-- Temporary replacement for "specs.nvim"
vim.api.nvim_set_hl(0, "CursorLine", { link = "CursorColumn" }) -- CursorLineNr
noremap("<c-k>", function()
  -- TODO: maybe not toggle them separately, but use state of one of them for both? (for consistency) 🤷
  vim.wo.cursorcolumn = not (vim.wo.cursorcolumn)
  vim.wo.cursorline = not (vim.wo.cursorline)
end)

-- Add undo break-points
imap(",", ",<c-g>u")
imap(".", ".<c-g>u")
imap(";", ";<c-g>u")



-- save file
imap("<C-s>", "<cmd>w<cr>", { desc = "Save file" })
vmap("<C-s>", "<cmd>w<cr>", { desc = "Save file" })
nmap("<C-s>", "<cmd>w<cr>", { desc = "Save file" })
smap("<C-s>", "<cmd>w<cr>", { desc = "Save file" })

-- better indenting
vmap("<", "<gv")
vmap(">", ">gv")
nnoremap("<", "<<")
nnoremap(">", ">>")
xnoremap("<", "<gv")
xnoremap(">", ">gv")

-- lazy
nmap("<leader>zl", "<cmd>Lazy<cr>", { desc = "Show Lazy dialog" })

-- new file
nmap("<leader>fn", "<cmd>enew<cr>", { desc = "New File" })

nmap("<leader>xl", "<cmd>lopen<cr>", { desc = "Location List" })
nmap("<leader>xq", "<cmd>copen<cr>", { desc = "Quickfix List" })


-- Clear search, diff update and redraw
-- taken from runtime/lua/_editor.lua
nmap("<leader>ur",
  "<cmd>set nohlsearch<bar>diffupdate<bar>normal! <C-L><cr>",
  { desc = "Redraw / clear hlsearch / diff update" }
)
-- }}}

--  Bufline Mappings {{{
nnoremap("<C-S-Tab>", "<cmd>bp<CR>")
nnoremap("<C-Tab>", "<cmd>bn<CR>")
inoremap("<C-S-Tab>", "<cmd>bp<CR>")
inoremap("<C-Tab>", "<cmd>bn<CR>")

-- TODO: remove <F2>/<F3> when I'll re-learn muscle memory to <C-Tab>/<C-S-Tab>
nnoremap("<F2>", "<cmd>bp<CR>")
nnoremap("<F3>", "<cmd>bn<CR>")
inoremap("<F2>", "<cmd>bp<CR>")
inoremap("<F3>", "<cmd>bn<CR>")

nnoremap("<A-,>", "<cmd>bp<CR>")
nnoremap("<A-.>", "<cmd>bn<CR>")


-- TODO: port below commands to native/custom

-- -- Re-order to previous/next
-- nnoremap("<A-<>", "<cmd>BufferMovePrevious<CR>")
-- nnoremap("<A->>", " <cmd>BufferMoveNext<CR>")
-- -- Goto buffer in position...
-- nnoremap("<A-1>", "<cmd>BufferGoto 1<CR>")
-- nnoremap("<A-2>", "<cmd>BufferGoto 2<CR>")
-- nnoremap("<A-3>", "<cmd>BufferGoto 3<CR>")
-- nnoremap("<A-4>", "<cmd>BufferGoto 4<CR>")
-- nnoremap("<A-5>", "<cmd>BufferGoto 5<CR>")
-- nnoremap("<A-6>", "<cmd>BufferGoto 6<CR>")
-- nnoremap("<A-7>", "<cmd>BufferGoto 7<CR>")
-- nnoremap("<A-8>", "<cmd>BufferGoto 8<CR>")
-- nnoremap("<A-9>", "<cmd>BufferGoto 9<CR>")
-- nnoremap("<A-0>", "<cmd>BufferLast<CR>")
-- -- Close buffer
-- nnoremap("<A-c>", "<cmd>BufferClose<CR>")

-- -- Sort automatically by...
-- nnoremap("<leader>bb", "<cmd>BufferOrderByBufferNumber<CR>")
-- nnoremap("<leader>bd", "<cmd>BufferOrderByDirectory<CR>")
-- nnoremap("<leader>bl", "<cmd>BufferOrderByLanguage<CR>")

-- Wipeout buffer
--                 :BufferWipeout<CR>
-- Close commands
--                 :BufferCloseAllButCurrent<CR>
--                 :BufferCloseBuffersLeft<CR>
--                 :BufferCloseBuffersRight<CR>
-- }}}

-- Ru kb layout
map("й", _smartquit, { silent = true })
map("Й", "<cmd>q!<CR>", { silent = true })
map("ц", "<cmd>w<CR>", { silent = true })
map("Ц", "<cmd>w!<CR>", { silent = true })
map("<m-й>", "<cmd>qa!<CR>", { silent = true })
map("ш", "<cmd>start<CR>")
map("щ", "o")
map("Щ", "O")
map("з", "p")
map("З", "P")
map("Ж", ":")
map("г", "u")
map("псс", "gcc")
map("м", "v")
map("М", "V")
