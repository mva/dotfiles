-- luacheck: globals vim T
require"lazylazy.options"
if vim.fn.argc(-1) == 0 then
  -- autocmds and keymaps can wait to load
  vim.api.nvim_create_autocmd("User", {
    group = vim.api.nvim_create_augroup("LazyLazy", { clear = true }),
    pattern = "VeryLazy",
    callback = function()
      require"lazylazy.autocmds"
      require"lazylazy.keymaps"
    end,
  })
else
  -- load them now so they affect the opened buffers
  require"lazylazy.autocmds"
  require"lazylazy.keymaps"
end
