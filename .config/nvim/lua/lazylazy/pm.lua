-- luacheck: globals vim T

local lazypath = vim.env.LAZY or vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  -- bootstrap lazy.nvim
  -- stylua: ignore
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)
require"cfg.lazy"

if vim.env.firstrun or vim.fn.isdirectory(lazypath) == 0 then
  require"lazy".sync{ wait = true }
  vim.cmd.qa()
  -- os.exit()
end
