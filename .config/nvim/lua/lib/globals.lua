-- luacheck: globals vim f luv api fn opt g o b bo wo req

-- luacheck: globals map noremap
-- luacheck: globals nmap nnoremap
-- luacheck: globals imap inoremap
-- luacheck: globals vmap vnoremap
-- luacheck: globals smap snoremap
-- luacheck: globals xmap xnoremap
-- luacheck: globals omap onoremap
-- luacheck: globals lmap lnoremap
-- luacheck: globals cmap cnoremap
-- luacheck: globals tmap tnoremap
-- luacheck: globals map_ noremap_

-- {{{ Variables
_G.f = require'lib.funcs'

_G.luv = vim.loop

_G.api = vim.api

_G.fn = vim.fn

_G.opt = vim.opt

_G.g = vim.g
_G.o = vim.o
_G.b = vim.b

_G.bo = vim.bo
_G.wo = vim.wo
-- }}}

-- {{{ Functions
function _G.req(mod) -- {{{
  local ok, err = pcall(require, mod)
  if not ok then
    f.eerror(("Caught error while loading '%s' library: %s"):format(mod, err))
    return nil
  else
    return err
  end
end -- }}}

-- {{{ Mapping Functions
local function _map(mode, lhs, rhs, opts)
  local o = opts or {}
  o.silent = o.silent ~= false
  f.map(mode, lhs, rhs, o)
end

_G._map = _map
function _G.map(lhs, rhs, opts) _map('', lhs, rhs, opts) end

function _G.nmap(lhs, rhs, opts) _map('n', lhs, rhs, opts) end

function _G.imap(lhs, rhs, opts) _map('i', lhs, rhs, opts) end

function _G.vmap(lhs, rhs, opts) _map('v', lhs, rhs, opts) end

function _G.smap(lhs, rhs, opts) _map('s', lhs, rhs, opts) end

function _G.xmap(lhs, rhs, opts) _map('x', lhs, rhs, opts) end

function _G.omap(lhs, rhs, opts) _map('o', lhs, rhs, opts) end

function _G.lmap(lhs, rhs, opts) _map('l', lhs, rhs, opts) end

function _G.cmap(lhs, rhs, opts) _map('c', lhs, rhs, opts) end

function _G.tmap(lhs, rhs, opts) _map('t', lhs, rhs, opts) end

function _G.map_(lhs, rhs, opts) _map('!', lhs, rhs, opts) end -- map!

function _G.noremap(lhs, rhs, opts)
  local o = opts or {}; o.remap = false; map(lhs, rhs, o);
end

function _G.nnoremap(lhs, rhs, opts)
  local o = opts or {}; o.remap = false; nmap(lhs, rhs, o);
end

function _G.inoremap(lhs, rhs, opts)
  local o = opts or {}; o.remap = false; imap(lhs, rhs, o);
end

function _G.vnoremap(lhs, rhs, opts)
  local o = opts or {}; o.remap = false; vmap(lhs, rhs, o);
end

function _G.xnoremap(lhs, rhs, opts)
  local o = opts or {}; o.remap = false; xmap(lhs, rhs, o);
end

function _G.tnoremap(lhs, rhs, opts)
  local o = opts or {}; o.remap = false; tmap(lhs, rhs, o);
end

-- TODO: others when needed
-- }}}

--- Read a file's contents to a string.
-- @tparam fname string
-- @treturn content string
-- @treturn err string
function _G._read_file(fname)
  local fl, err = io.open(fname, 'rb')
  if not fl then
    return nil, err
  end

  local content = fl:read('*all')
  fl:close()

  return content
end

--- Decode the contents of a json file.
-- @tparam fname string
-- @return json
-- @treturn err string
function _G._read_json_file(fname)
  local raw, err = _G._read_file(fname)
  if not raw then
    return nil, err
  end

  return fn.json_decode(raw)
end

-- }}
