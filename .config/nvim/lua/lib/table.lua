-- luacheck: globals vim

function _G.table.create(t)
  return setmetatable(t or {}, { __index = table })
end

function _G.table.copy(src)
  local dst = _G.table.create{}
  for k, v in pairs(src) do
    if type(v) == "table" then
      v = _G.table.copy(v)
    end
    dst[k] = v
  end
  return dst
end

_G.table.show = vim.inspect

_G.table.unpack = _G.table.unpack or _G.unpack

return _G.table
