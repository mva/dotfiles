local string = string;
if vim.g.lua_utf8_string_patch then
  string.len = string.utf8_len;
  string.sub = string.utf8_sub;
  string.reverse = string.utf8_reverse;
  string.upper = string.utf8_upper;
  string.lower = string.utf8_lower;
end
string = require"lib.string.extras";
return string
