--- @diagnostic disable: inject-field
local string = string;

function string.insert(where, what, pos)
  where = where or "";
  what = what or "";
  pos = pos or 1;
  local res = "";

  if type(pos) == "number" then
    res = where:sub(1, pos - 1) .. what .. where:sub(pos)
  elseif type(pos) == "table" then
    local res_t = T{};
    local prev_pos = 1;
    for _, p in ipairs(pos) do
      local p_type = type(p)
      assert(p_type == "number", "You've provided table as position argument, but " ..
        "this table contains " .. (p_type) .. " at position " .. (_) .. ", while only numbers are supported");
      res_t:insert(where:sub(prev_pos, p - 1));
      prev_pos = p;
    end
    if #pos == 0 then res_t:insert("") end;
    res_t:insert(where:sub(pos[#pos] or 0, #where));
    res = res_t:concat(what);
  else
    error("Only number or table of numbers supported as position argument")
  end
  return res;
end

function string.symbol(str, sym)
  local s = tostring(str);
  local symbol = tonumber(sym);
  return s:sub(symbol, symbol); --- @diagnostic disable-line: param-type-mismatch
end;

function string.print(str)
  print(str);
end;

function string.word(str, pos)
  local s = tostring(str);
  local p = tonumber(pos) or 1;
  return s:match(("%S+ "):rep(p - 1) .. "(%S+)")
end;

function string.fmt(str, fmt)
  local s = tostring(str);
  local format = tostring(fmt);
  return format:format(s)
end;

-- PHP's explode
function string.explode(str, sep, lim)
  if not str then return false end
  if not sep or sep == "" then return false end
  local limit = lim or math.huge;
  if limit == 0 or limit == 1 then return T{ str } end

  local r = T{}
  local n, init = 0, 1

  while true do
    local s, e = str:find(sep, init, true)
    if not s then break end
    r[#r + 1] = str:sub(init, s - 1)
    init = e + 1
    n = n + 1
    if n == limit - 1 then break end
  end

  if init <= str:len() then
    r:insert(str:sub(init))
  else
    r:insert("")
  end
  n = n + 1

  if limit < 0 then
    for i = n, n + limit + 1, -1 do r[i] = nil end
    n = n + limit
  end

  return r
end

-- Python's split
function string.split(str, sp, lim)
  assert(sp ~= "");
  assert(lim == nil or lim >= 1);
  local sep = sp or " ";
  local limit = lim or math.huge;
  if limit == 0 then return { str } end
  if limit < 0 then return str:explode(sep or " ") end

  return str:explode(sep, limit + 1);
end

function string.fastsplit(str, spr)
  local sep = spr or "\n"
  local result = {}
  local i = 1
  for c in str:gmatch(string.format("([^%s]+)", sep)) do
    result[i] = c
    i = i + 1
  end
  return result
end

-- Numeric Conversions --

function string.to_bin(s)
  s = tonumber(s);
  if not s then return nil; end;
  local out = '';

  while s >= 1 do
    if (s % 2 ~= 0) then
      out = "1" .. out;
    else
      out = "0" .. out;
    end;
    s = math.floor(s / 2);
  end;
  return out;
end;

function string.to_rbin(s)
  s = tonumber(s);
  if not s then return nil; end;
  local out = '';

  while s >= 1 do
    if (s % 2 ~= 0) then
      out = out .. "1"
    else
      out = out .. "0";
    end;
    s = math.floor(s / 2);
  end;
  return out;
end;

function string.to_hex(s)
  s = tonumber(s);
  return ("%2X"):format(s)
end;

return string
