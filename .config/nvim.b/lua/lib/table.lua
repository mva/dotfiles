-- luacheck: globals vim

function _G.table.create(t)
    return setmetatable(t or {}, {__index = table})
end

local T = _G.table.create
_G.table.copy = function (src) -- {{{
  local dst = T{}
  for k, v in pairs(src) do
    if type(v) == "table" then
      v = _G.table.copy(v)
    end
    dst[k] = v
  end
  return dst
end -- }}}

_G.table.show = vim.inspect

return _G.table
