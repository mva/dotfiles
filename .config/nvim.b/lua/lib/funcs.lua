-- luacheck: globals req vim T
-- local g = vim.g

local M = {}

local function send_notification(lvl, msg, title) -- {{{
    vim.schedule_wrap(
      vim.notify(
        msg,
        vim.log.levels[lvl],
        {
          title=title,
        }
      )
    )
end -- }}}

function M.eerror(...) -- {{{
    send_notification('ERROR',...)
end -- }}}

function M.ewarn(...) -- {{{
    send_notification('WARN',...)
end -- }}}

function M.einfo(...) -- {{{
    send_notification('INFO',...)
end -- }}}

function M.map(mode, lhs, rhs, opts) -- probably already upstreamed -- {{{
  local options = {}
  --= {noremap = true, silent = true}
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end
  -- local stat, error = pcall(vim.api.nvim_set_keymap, mode, lhs, rhs, options)
  local stat, error = pcall(vim.keymap.set, mode, lhs, rhs, options)
  if not stat then
    M.eerror(
      ('Caught an error while tryng to map a key. Error:\n%s\n\nparameters:\n\tmode "%s"\nlhs: %s\nrhs: %s\nopts: %s')
        :format(error,mode,lhs,rhs,vim.inspect(options)),
      'keymap'
    )
  end
end -- }}}

function M.add_cmd(cmd, repl, force) -- {{{
  local command
  if force then
    command = "command! "..cmd.." "..repl
  else
    command = "command "..cmd.." "..repl
  end
  local ok, err = pcall(vim.cmd, command)
  if not ok then
    vim.notify("setting cmd: "..cmd.." "..err, vim.log.levels.ERROR, {title='command'})
  end
end -- }}}

M.cmd = M.add_cmd

function M.au(event, pattern, callback, nested, desc) -- {{{
  vim.api.nvim_create_autocmd(event, {
    pattern = pattern,
    callback = callback,
    nested = nested,
    desc = desc,
})
end -- }}}

M.au2 = vim.api.nvim_create_autocmd

function M.usrcmd(name, func, desc) -- {{{
  vim.api.nvim_create_user_command(name, func, {
    nargs = "*",
    desc = desc,
  })
end -- }}}

function M.OS() -- {{{
  local has = vim.fn.has
    if has'macunix' then
      return "mac"
    elseif has'unix' and not has'macunix' and not has'win32unix' then
      return "linux"
    elseif has'win16' or has'win32' or has'win64' then
      return "windows"
    end
end -- }}}


function M.format_w_prg() -- {{{
  if vim.bo.formatprg then -- {{{
    local view = vim.fn.winsaveview()
    vim.cmd[[normal gggqG]]
    vim.fn.winrestview(view)
    return true
  end -- }}}
  return false
end -- }}}

function M.u(code) -- {{{ -- Convert UTF-8 hex code to character
  if type(code) == 'string' then
    code = tonumber('0x' .. code)
  end
  local c = string.char
  if code <= 0x7f then
    return c(code)
  end
  local t = T{}
  if code <= 0x07ff then
    t[1] = c(bit.bor(0xc0, bit.rshift(code, 6)))
    t[2] = c(bit.bor(0x80, bit.band(code, 0x3f)))
  elseif code <= 0xffff then
    t[1] = c(bit.bor(0xe0, bit.rshift(code, 12)))
    t[2] = c(bit.bor(0x80, bit.band(bit.rshift(code, 6), 0x3f)))
    t[3] = c(bit.bor(0x80, bit.band(code, 0x3f)))
  else
    t[1] = c(bit.bor(0xf0, bit.rshift(code, 18)))
    t[2] = c(bit.bor(0x80, bit.band(bit.rshift(code, 12), 0x3f)))
    t[3] = c(bit.bor(0x80, bit.band(bit.rshift(code, 6), 0x3f)))
    t[4] = c(bit.bor(0x80, bit.band(code, 0x3f)))
  end
  return t:concat()
end -- }}}

function _G.dump(...) -- {{{
  local args = {...}
  if #args ~= 1 then
    print(vim.inspect(args)) -- We need to pack it in table before passing to inspect
  else
    print(vim.inspect(args[1])) -- no need in table
  end
end -- }}}

--[[{{{
function M.load(path)
  local ok, mod = pcall(require, path)
  if not ok then
    printf('Error loading module `%s`', path)
    print(mod)
  else
    local loadfunc
    if type(mod) == 'function' then
      loadfunc = mod
    elseif mod.setup ~= nil then
      loadfunc = mod.setup
    end
    local ok, err = pcall(loadfunc)
    if not ok then
      printf('Error loading module `%s`', path)
      print(err)
    end
  end
end
}}}]]


function M.hl_by_name(hl_group) -- {{{ -- Get information about highlight group
  local hl = vim.api.nvim_get_hl_by_name(hl_group, true)
  if hl.foreground ~= nil then
    hl.fg = ('#%x'):format(hl.foreground)
  end
  if hl.background ~= nil then
    hl.bg = ('#%x'):format(hl.background)
  end
  return hl
end -- }}}

function M.id_generator(start) -- {{{
  local cnt = start or 0
  return function()
    local result = cnt
    cnt = cnt + 1
    return result
  end
end -- }}}

-- TODO: M.timer




-- M.au('UIEnter', '*', function() g.ui_entered = true; end)
return M

