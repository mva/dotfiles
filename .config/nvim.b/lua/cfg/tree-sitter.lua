-- luacheck: globals vim req T

local parser_configs = req"nvim-treesitter.parsers".get_parser_configs()
parser_configs.norg = { -- {{{
  install_info = { -- {{{
    url = "https://github.com/nvim-neorg/tree-sitter-norg",
    files = { "src/parser.c", "src/scanner.cc" },
    branch = "main"
  }, -- }}}
} -- }}}
req"nvim-treesitter.configs".setup{ -- {{{
  ensure_installed = { -- {{{
    "c", "cpp",
    "rust", "toml",

    "c",
    "comment",
    "dockerfile",
    -- "help",
    "http",

    "html",
    "javascript",
    "css",

    "bash",
    -- "zsh",
    "comment",
    "lua",
    "go",
    "json",
    "json5",
    "jsonc",

    "yaml",
    "python",
    "vim",

    "norg",
    "org",

    "latex",

    "make",

    "query",
    "toml",
    "regex",

  }, -- }}}
  rainbow = { -- {{{
    enable = false, -- doesn't work anyway 🤷
    extended_mode = true,
    max_file_lines = 1000,
    colors = { '#B52A5B', '#FF4971', '#bd93f9', '#E9729D', '#F18FB0', '#8897F4', '#b488bf' },
  }, -- }}}
  highlight = {
    enable = true,
    disable = function(lang, bufnr)
      local ft_dis = T{
        html = true,
        lua = true,
        xml = true,
        ebuild = true,
        text = true,
      }
      if ft_dis[lang] and vim.api.nvim_buf_line_count(bufnr) > 500 then
        return true
      end
      for _, line in ipairs(vim.api.nvim_buf_get_lines(0, 0, 3, false)) do
        if #line > 200 then
          return true
        end
      end
      return false
    end,
  },
  incremental_selection = { -- {{{
    enable = true,
    keymaps = { -- {{{
      init_selection = "gnn",
      node_incremental = "gnn",
      scope_incremental = "gns",
      node_decremental = "gnp",
    }, -- }}}
  }, -- }}}
  indent = {
    enable = true,
    disable = function(lang, _)
      local ft_dis = T{
        lua = true,
      }
      return not(not(ft_dis[lang]))
    end,
  }, -- problems with indentation in lua. TODO: investigate and try to fix
  query_linter = { -- {{{
    enable = true,
    use_virtual_text = true,
    lint_events = { "BufWrite", "CursorHold" },
  }, -- }}}
  textsubjects = { -- {{{
      enable = true,
      keymaps = { [','] = 'textsubjects-smart', }
  }, -- }}}
  autopairs = {
    enable = false,
    -- disable = function(lang, bufnr) end,
  }, -- adds extra quote on completion inside quotes 🤷 TODO: try to fix
  textobjects = { -- {{{
    select = { -- {{{
      enable = true,
      lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
      keymaps = { -- {{{
        ["af"] = "@function.outer",
        ["if"] = "@function.inner",
        ["il"] = "@loop.outer",
        ["al"] = "@loop.outer",
        ["icd"] = "@conditional.inner",
        ["acd"] = "@conditional.outer",
        ["acm"] = "@comment.outer",
        ["ast"] = "@statement.outer",
        ["isc"] = "@scopename.inner",
        ["iB"] = "@block.inner",
        ["aB"] = "@block.outer",
        ["p"] = "@parameter.inner",
      }, -- }}}
    }, -- }}}
    move = { -- {{{
      enable = true,
      set_jumps = true, -- Whether to set jumps in the jumplist
      goto_next_start = { -- {{{
        ["gnf"] = "@function.outer",
        ["gnif"] = "@function.inner",
        ["gnp"] = "@parameter.inner",
        ["gnc"] = "@call.outer",
        ["gnic"] = "@call.inner",
      }, -- }}}
      goto_next_end = { -- {{{
        ["gnF"] = "@function.outer",
        ["gniF"] = "@function.inner",
        ["gnP"] = "@parameter.inner",
        ["gnC"] = "@call.outer",
        ["gniC"] = "@call.inner",
      }, -- }}}
      goto_previous_start = { -- {{{
        ["gpf"] = "@function.outer",
        ["gpif"] = "@function.inner",
        ["gpp"] = "@parameter.inner",
        ["gpc"] = "@call.outer",
        ["gpic"] = "@call.inner",
      }, -- }}}
      goto_previous_end = { -- {{{
        ["gpF"] = "@function.outer",
        ["gpiF"] = "@function.inner",
        ["gpP"] = "@parameter.inner",
        ["gpC"] = "@call.outer",
        ["gpiC"] = "@call.inner",
      }, -- }}}
    }, -- }}}
    swap = { -- {{{
       enable = true,
       swap_next = {["<leader>xp"] = "@parameter.inner"},
       swap_previous = {["<leader>xP"] = "@parameter.inner"}
    }, -- }}}
    lsp_interop = { -- {{{
       enable = true,
       border = 'none',
       peek_definition_code = {
          ["<leader>pf"] = "@function.outer",
          ["<leader>pc"] = "@class.outer"
       }
    }, -- }}}
  }, -- }}}
  playground = { -- {{{
    enable = true,
    updatetime = 25, -- Debounced time for highlighting nodes in the playground from source code
    persist_queries = false, -- Whether the query persists across vim sessions
    keybindings = { -- {{{
      toggle_query_editor = 'o',
      toggle_hl_groups = 'I',
      toggle_language_display = 'i',
      toggle_injected_languages = 't',
      toggle_anonymous_nodes = 'a',
      focus_language = 'f',
      unfocus_language = 'F',
      update = 'R',
      goto_node = '<cr>',
      show_help = '?',
    }, -- }}}
  }, -- }}}
  tree_docs = { enable = false, }, -- Currently experimental, doesn't work well
  context_commentstring = { -- {{{
    enable = true,
    config = {
      css = '// %s',
     javascript = { -- {{{
      __default = '// %s',
      jsx_element = '{/* %s */}',
      jsx_fragment = '{/* %s */}',
      jsx_attribute = '// %s',
      comment = '// %s'
      }, -- }}}
      crontab = "# %s",
    },
    enable_autocmd = true,
  }, -- }}}
  endwise = { enable = true, },
  autotag = { enable = true, },
  matchup = {
    enable = true,              -- mandatory, false will disable the whole extension
    -- disable = { "c", "ruby" },  -- optional, list of language that will be disabled
    disable_virtual_text = false,
    include_match_words = true,
  },
} -- }}}

req"cfg.tree-sitter.highlights-tmp-fix"
