-- luacheck: globals req vim T

-- [ Colors ] {{{

local hi = vim.api.nvim_set_hl

--[[
" " Meaning of terms:
"
" format: "Buffer" + status + part
"
" status:
"     *Current: current buffer
"     *Visible: visible but not current buffer
"    *Inactive: invisible but not current buffer
"
" part:
"        *Icon: filetype icon
"       *Index: buffer index
"         *Mod: when modified
"        *Sign: the separator between buffers
"      *Target: letter in buffer-picking mode
"
" BufferTabpages: tabpage indicator
" BufferTabpageFill: filler after the buffer section
" BufferOffset: offset section, created with set_offset()
--]]

hi(0, "TabLineFill", {bg = "#1B1D1E", fg = "#808080",})
hi(0, "TabLineSel", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "TabLine", {bg = "#1B1D1E", fg = "#808080", italic=1, })

hi(0, "BufferCurrent", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "BufferCurrentIndex", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "BufferCurrentMod", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })
hi(0, "BufferCurrentSign", {bg = "#1B1D1E", fg = "#559EFF", bold=1, })
hi(0, "BufferCurrentTarget", {bg = "#1B1D1E", fg = "#F8F8F2", bold=1, })

hi(0, "BufferVisible", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })
hi(0, "BufferVisibleIndex", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })
hi(0, "BufferVisibleMod", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })
hi(0, "BufferVisibleSign", {bg = "#1B1D1E", fg = "#808080", })
-- gui=bold,italic
hi(0, "BufferVisibleTarget", {bg = "#1B1D1E", fg = "#808080", bold = 1, italic=1, })

hi(0, "BufferInactive", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveIndex", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveMod", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveSign", {bg = "#1B1D1E", fg = "#808080" })
hi(0, "BufferInactiveTarget", {bg = "#1B1D1E", fg = "#808080", })

hi(0, "BufferTabpages", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferTabpageFill", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferOffset", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferPart", {bg = "#1B1D1E", fg = "#808080", })


hi(0, "BufferVisibleERROR", {bg = "#1B1D1E", fg = "#FF8888", })
hi(0, "BufferInactiveERROR", {bg = "#1B1D1E", fg = "#FF4444", })
hi(0, "BufferCurrentERROR", {bg = "#1B1D1E", fg = "#FF0000", })

hi(0, "BufferVisibleWARN", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveWARN", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferCurrentWARN", {bg = "#1B1D1E", fg = "#808080", })

hi(0, "BufferVisibleHINT", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveHINT", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferCurrentHINT", {bg = "#1B1D1E", fg = "#808080", })

hi(0, "BufferVisibleINFO", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferInactiveINFO", {bg = "#1B1D1E", fg = "#808080", })
hi(0, "BufferCurrentINFO", {bg = "#1B1D1E", fg = "#808080", })

-- }}}
  -- Set barbar's options

req'barbar'.setup { -- {{{
  -- Enable/disable animations
  animation = true,

  -- Enable/disable auto-hiding the tab bar when there is a single buffer
  auto_hide = false,

  -- Enable/disable current/total tabpages indicator (top right corner)
  tabpages = true,

  -- Enables/disable clickable tabs
  --  - left-click: go to buffer
  --  - middle-click: delete buffer
  clickable = false, -- true,

  -- Excludes buffers from the tabline
  -- exclude_ft = {'javascript'},
  -- exclude_name = {'package.json'},

  -- A buffer to this direction will be focused (if it exists) when closing the current buffer.
  -- Valid options are 'left' (the default) and 'right'
  focus_on_close = 'left',

  -- Hide inactive buffers and file extensions. Other options are `alternate`, `current`, and `visible`.
  -- hide = {extensions = true, inactive = true},

  -- Disable highlighting alternate buffers
  highlight_alternate = false,

  -- Disable highlighting file icons in inactive buffers
  highlight_inactive_file_icons = false,

  -- Enable highlighting visible buffers
  highlight_visible = true,

  icons = { -- {{{
    -- Configure the base icons on the bufferline.
    buffer_index = false,
    buffer_number = false,
    -- button = '',
    button = '',
    -- Enables / disables diagnostic symbols
    diagnostics = { -- {{{
      -- [vim.diagnostic.severity.ERROR] = {enabled = true, icon = 'ﬀ'},
      [vim.diagnostic.severity.ERROR] = {
        enabled = false,
        icon = '',
      },
      [vim.diagnostic.severity.WARN] = {
        enabled = false
      },
      [vim.diagnostic.severity.INFO] = {
        enabled = false
      },
      [vim.diagnostic.severity.HINT] = {
        enabled = false
      },
    }, -- }}}
    filetype = { -- {{{
      -- Sets the icon's highlight group.
      -- If false, will use nvim-web-devicons colors
      custom_colors = false,

      -- Requires `nvim-web-devicons` if `true`
      enabled = false, --true,
    }, -- }}}
    separator = { -- {{{
      left = '',
      right = '',
    }, -- }}}

    modified = { -- {{{
      -- Configure the icons on the bufferline when modified or pinned.
      -- Supports all the base icon options.
      button = '',
    -- button = '●',
    }, -- }}}
    pinned = { -- {{{
      button = '車',
    }, -- }}}

    alternate = { -- {{{
    -- Configure the icons on the bufferline based on the visibility of a buffer.
    -- Supports all the base icon options, plus `modified` and `pinned`.
      filetype = { -- {{{
        enabled = false,
      } -- }}}
    }, -- }}}
    current = { -- {{{
      buffer_index = false, --true,
    }, -- }}}
    inactive = { -- {{{
    -- button = '×',
      separator = { -- {{{
        left = '',
        right = '',
      }, -- }}}
    }, -- }}}
    visible = { -- {{{
      modified = { -- {{{
        buffer_number = false,
      }, -- }}}
    }, -- }}}
  }, -- }}}

  -- If true, new buffers will be inserted at the start/end of the list.
  -- Default is to insert after current buffer.
  insert_at_end = false,
  insert_at_start = false,

  -- Sets the maximum padding width with which to surround each tab
  maximum_padding = 1,

  -- Sets the minimum padding width with which to surround each tab
  minimum_padding = 1,

  -- Sets the maximum buffer name length.
  maximum_length = 30,

  -- If set, the letters for each buffer in buffer-pick mode will be
  -- assigned based on their name. Otherwise or in case all letters are
  -- already assigned, the behavior is to assign letters in order of
  -- usability (see order below)
  semantic_letters = true,

  -- New buffer letters are assigned in this order. This order is
  -- optimal for the qwerty keyboard layout but might need adjustement
  -- for other layouts.
  letters = 'asdfjkl;ghnmxcvbziowerutyqpASDFJKLGHNMXCVBZIOWERUTYQP',

  -- Sets the name of unnamed buffers. By default format is "[Buffer X]"
  -- where X is the buffer number. But only a static string is accepted here.
  no_name_title = '',
} -- }}}
