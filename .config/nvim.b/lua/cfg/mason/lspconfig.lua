-- luacheck: globals req

local mason_lsp = req"mason-lspconfig"

local function E(list) -- {{{
  local ret = {}
  local ls_bins = {
    clangd  = "clangd",
    ltex  = "ltex-ls",
    ccls  = "ccls",
    gopls = "gopls",
    pylsp = "pylsp",
    html = "vscode-html-language-server",
    jsonls = "vscode-json-language-server",
    cssls = "vscode-css-language-server",
    eslint = "vscode-eslint-language-server",
    -- markdown = "vscode-markdown-language-server",
  }
  for _,srv in ipairs(list) do
    local ex = io.popen(("which %s 2>/dev/null"):format(ls_bins[srv])):read()
    if ex then
      table.insert(ret, srv)
    end
  end
  return ret
end -- }}}

if mason_lsp and mason_lsp.settings then -- {{{
  mason_lsp.setup {
    -- automatic_installation = true,
    -- automatically detect which servers to install (based on which servers are set up via lspconfig)
    automatic_installation = {
      exclude = E(req"cfg.lsp.servers"),
    },
    ui = {
      icons = { -- TODO: bigger (bold) icons
        server_installed = "✅",
        server_uninstalled = "❌",
        server_pending = "🔄",
      }
    }
  }
end -- }}}


