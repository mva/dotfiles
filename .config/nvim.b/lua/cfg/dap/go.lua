-- luacheck: globals req nmap

local dap_go = req"dap-go"
dap_go.setup()


nmap("<Leader>td",  dap_go.debug_test, { silent = true})
