-- luacheck: globals req nnoremap vnoremap
local dap_python = req"dap-python"

dap_python.setup'~/.local/share/nvim/mason/packages/debugpy/venv/bin/python' -- TODO: move to pipx

nnoremap("<Leader>dn",  dap_python.test_method, { silent = true})
nnoremap("<Leader>df",  dap_python.test_class, { silent = true})
vnoremap("<Leader>ds",  dap_python.debug_selection, { silent = true})
