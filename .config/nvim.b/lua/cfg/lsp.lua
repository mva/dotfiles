-- luacheck: globals req vim T nmap, no max line length
local lsp = req"lspconfig"
local lsp_configs = req"lspconfig.configs"
-- local lsp_util = req"lspconfig.util"

local srv_p = vim.fn.stdpath("data").."/lsp_servers" -- TODO: mason

-- [ Colors ] {{{
local hi = vim.api.nvim_set_hl
local sign_define = vim.fn.sign_define

hi(0, "LSPErrorSign",   { fg="#ee0000" })
hi(0, "LSPWarningSign", { fg="#eeee00" })
hi(0, "LSPInfoSign",    { fg="#2288ee" })
hi(0, "LSPHintSign",    { fg="#33dd99" })

-- FIXME
hi(0, "LspReferenceText",  { bg="#ffff00", fg="#ff0000", bold=1 })
hi(0, "LspReferenceRead",  { bg="#00ffff", fg="#0000ff", bold=1 })
hi(0, "LspReferenceWrite", { bg="#00ff00" })

hi(0, "LspDiagnosticsUnderlineError", {undercurl = 1,})
hi(0, "LspDiagnosticsUnderlineWarning", {undercurl = 1,})
hi(0, "LspDiagnosticsUnderlineInformation", {undercurl = 1,})
hi(0, "LspDiagnosticsUnderlineHint", {undercurl = 1,})

sign_define("DiagnosticSignError", {
  text="",
-- text="✗",
-- text="",
  numhl="LspDiagnosticsSignError",
  texthl="LspDiagnosticsSignError"
-- linehl=
})
sign_define("DiagnosticSignWarn", {
  text="",
-- text=⚠ ",
  numhl="LspDiagnosticsSignWarning",
  texthl="LspDiagnosticsSignWarning"
})
sign_define("DiagnosticSignInfo", {
  text="",
-- text="",
  numhl="LspDiagnosticsSignInformation",
  texthl="LspDiagnosticsSignInformation"
})
sign_define("DiagnosticSignHint", {
  text="",
--  text="",
-- text="",
-- text="",
-- text="➤",
  numhl="LspDiagnosticsSignHint",
  texthl="LspDiagnosticsSignHint"
})

hi(0,"LspDiagnosticsDefaultError", {link = "LSPErrorSign",})
hi(0,"LspDiagnosticsDefaultWarning", {link = "LSPWarningSign",})
hi(0,"LspDiagnosticsDefaultInformation", {link = "LSPInfoSign",})
hi(0,"LspDiagnosticsDefaultHint", {link = "LSPHintSign",})

hi(0,"DiagnosticDefaultError", {link = "LSPErrorSign",})
hi(0,"DiagnosticDefaultWarning", {link = "LSPWarningSign",})
hi(0,"DiagnosticDefaultInformation", {link = "LSPInfoSign",})
hi(0,"DiagnosticDefaultHint", {link = "LSPHintSign",})

hi(0,"LspDiagnosticsVirtualTextError", {link = "LSPErrorSign",})
hi(0,"LspDiagnosticsVirtualTextWarning", {link = "LSPWarningSign",})
hi(0,"LspDiagnosticsVirtualTextInformation", {link = "LSPInfoSign",})
hi(0,"LspDiagnosticsVirtualTextHint", {link = "LSPHintSign",})

hi(0,"DiagnosticVirtualTextError", {link = "LSPErrorSign",})
hi(0,"DiagnosticVirtualTextWarning", {link = "LSPWarningSign",})
hi(0,"DiagnosticVirtualTextInformation", {link = "LSPInfoSign",})
hi(0,"DiagnosticVirtualTextHint", {link = "LSPHintSign",})

-- LspDiagnosticsFloatingError
-- LspDiagnosticsFloatingWarning
-- LspDiagnosticsFloatingInformation
-- LspDiagnosticsFloatingHint

hi(0,"LspDiagnosticsSignError", {link = "LSPErrorSign",})
hi(0,"LspDiagnosticsSignWarning", {link = "LSPWarningSign",})
hi(0,"LspDiagnosticsSignInformation", {link = "LSPInfoSign",})
hi(0,"LspDiagnosticsSignHint", {link = "LSPHintSign",})

hi(0,"DiagnosticSignError", {link = "LSPErrorSign",})
hi(0,"DiagnosticSignWarning", {link = "LSPWarningSign",})
hi(0,"DiagnosticSignInformation", {link = "LSPInfoSign",})
hi(0,"DiagnosticSignHint", {link = "LSPHintSign",})
-- }}}

local servers = {}
local setups = {}

if vim.fn.filereadable(("%s/.load_lspservers"):format(vim.fn.stdpath'data')) == 1 then
  servers = req"cfg.lsp.servers"
  setups  = req"cfg.lsp.setups"
end

local rocks_path = ("%s/packer_hererocks/%s/share/lua/5.1"):format(vim.fn.stdpath'data',jit.version:gsub('LuaJIT ', ''))

-- Custom servers declarations
lsp_configs.lua = { -- {{{ Custom lua-lsp server
  default_config = {
    cmd = {
      [[luajit]], [[-e]],
      table.concat{
        ([[package.path = package.path..";__ROCKS__/?.lua;__ROCKS__/?/init.lua"]]):gsub("__ROCKS__",rocks_path),
        [[require"lua-lsp.loop"()]],
      }
    };
    -- cmd = {"lua-lsp"},
    filetypes = {"lua", "moon"};
    root_dir = function()
      return vim.loop.cwd()
    end
    -- lsp_util.path.dirname
  };
  -- on_new_config = function(new_config) end;
  -- on_attach = function(client, bufnr) end;
} --}}}
lsp_configs.emmet_ls = { -- {{{ Custom LSP server for "emmet" (creates HTML blocks from selector line)
  default_config = {
    cmd = {
      srv_p.."/emmet_ls/node_modules/.bin/"..'emmet-ls',
      '--stdio'
    },
    filetypes = {'html', 'css', 'blade'},
    root_dir = function()
      return vim.loop.cwd()
    end,
    init_options = {
      html = {
        options = {
          -- For possible options, see: https://github.com/emmetio/emmet/blob/master/src/config.ts#L79-L267
          ["bem.enabled"] = true,
        },
      },
    },
    -- settings = {},
  };
} -- }}}

local function goenv() -- {{{
  local env = io.popen("go env");
  local ret = {}
  for line in env:lines() do
    local k, v = line:match([=[^([^=]+)%="(.*)"$]=])
    ret[k]=v
  end
  return ret
end -- }}}
local function gopath() -- luacheck: ignore -- {{{
  local vp = vim.g.gopath;
  if vp then
    return {GOPATH=vp}
  else
    return goenv().GOPATH
  end
end -- }}}

--[[ In case of debug {{{
lsp.util.default_config = vim.tbl_extend(
  "force", lsp.util.default_config, {log_level = vim.lsp.protocol.MessageType.Debug}
)
--]] --}}}

local custom_lsps = { -- For plugins with alternate configurations (instead of lspconfig)
  ltexls = req'ltex-ls'
}

for _, srv in ipairs(servers) do
  local l
  if custom_lsps[srv] then
    l = custom_lsps
  elseif lsp[srv] then
    l = lsp
  else
    l = { setup = function() vim.notify(("Attempted to setup LSP server that has no declarations: %s"):format(srv),"warn") end, }
  end

  l[srv].setup(setups[srv] or setups.default)
end

return {lsp = lsp, defaults = setups.default}
