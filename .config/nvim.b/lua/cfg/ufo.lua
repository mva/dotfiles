-- luacheck: globals req vim

local ufo = req"ufo"

---@param bufnr number
---@return Promise
local function customizeSelector(bufnr)
  local getFolds = ufo.getFolds
  local promise = req"promise"
  local function handleFallbackException(err, providerName)
    if type(err) == 'string' and err:match('UfoFallbackException') then
      return getFolds(bufnr, providerName)
    else
      return promise.reject(err)
    end
  end

  return getFolds(bufnr, 'lsp'):catch(function(err)
    return handleFallbackException(err, 'treesitter')
  end):catch(function(err)
    return handleFallbackException(err, 'indent')
  end)
  -- TODO: `marker` or rewrite to my custom function summarizing folds (with marker)
end

-- lsp->treesitter->indent
local function selectProviderWithChainByDefault(bufnr, filetype, buftype) -- luacheck: no unused args
  local ftMap = {
    -- vim = 'indent',
    -- python = {'indent'},
    git = '',
  }

  return ftMap[filetype] or customizeSelector
end

ufo.setup{
  provider_selector = selectProviderWithChainByDefault,
}


-- TODO: rewrite on Lua
-- {{{
--[=[
vim.cmd[[
function! MyFoldexpr(lnum)
  function! Marker(lnum)
    let l:cur_line = getline(a:lnum)
    let l:next_line = getline(a:lnum + 1)
    let l:marker_start = repeat('{', 3)
    let l:marker_end = repeat('}', 3)
    if l:cur_line =~# l:marker_start
      return "a1"
    elseif l:cur_line =~# l:marker_end
      return "s1"
    else
      return '='
    endif
  endfunction

  let l:ret = Marker(a:lnum)
  return l:ret
endfunction

function! MyFoldtext()
  let l:marker = repeat('{', 3)
  let line = getline(v:foldstart)
  let folded_line_num = v:foldend - v:foldstart
  let line_text = substitute(line, l:marker, '', 'g')
  let fillcharcount = &textwidth - len(line_text) - len(folded_line_num)
  return printf('+%s %d строк: %s%s', repeat('-', 1 + v:foldlevel), folded_line_num, line_text, repeat('·', fillcharcount))
endfunction
]]
--]=]
-- }}}
