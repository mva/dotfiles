-- luacheck: globals req

local i = req"nvim-web-devicons"

i.setup{ -- {{{
  --[[ override {{{
   override = {
     zsh = {
       icon = "",
       color = "#428850",
       name = "Zsh" -- DevIcon will be appended to `name`
       cterm_color = "65",
     }
   };
  --]] -- }}}
  -- globally enable default icons (default to false)
  -- will get overriden by `get_icons` option
  default = true,
  color_icons = true,
} --}}}
-- i.set_default_icon('', '#6d8086')

