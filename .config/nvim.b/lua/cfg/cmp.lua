-- luacheck: globals req vim T luasnip, no max line length

-- local plugins = _G.packer_plugins or {}

local cmp = req"cmp"
local luasnip = req"cfg.luasnip"
local lspkind = req"lspkind"
local cmp_buffer = req"cmp_buffer"
local hi = vim.api.nvim_set_hl

-- [ Colors (higllight) ] {{{
--[[ hi(0, "CmpItemAbbrDeprecated", { bg = "NONE", strikethrough=1, fg = "#808080", })
hi(0, "CmpItemAbbrMatch", { bg = "NONE", fg = "#569CD6", })
hi(0, "CmpItemAbbrMatchFuzzy", { link = "CmpItemAbbrMatch", })
hi(0, "CmpItemKindVariable", { bg = "NONE", fg = "#9CDCFE", })
hi(0, "CmpItemKindInterface", { link = "CmpItemKindVariable", })
hi(0, "CmpItemKindText", { link = "CmpItemKindVariable", })
hi(0, "CmpItemKindFunction", { bg = "NONE", fg = "#C586C0", })
hi(0, "CmpItemKindMethod", { link = "CmpItemKindFunction", })
hi(0, "CmpItemKindKeyword", { bg = "NONE", fg = "#D4D4D4", })
hi(0, "CmpItemKindProperty", { link = "CmpItemKindKeyword", })
hi(0, "CmpItemKindUnit", { link = "CmpItemKindKeyword" }) ]]
local cmp_hls = {
  CmpItemAbbrDeprecated = { fg = "#7E8294", bg = "NONE", strikethrough = 1, },
  CmpItemAbbrMatch = { fg = "#82AAFF", bg = "NONE", bold = 1, },
  CmpItemAbbrMatchFuzzy = { fg = "#82AAFF", bg = "NONE", bold = 1, },
  CmpItemMenu = { fg = "#C792EA", bg = "NONE", italic = 1, bold = 1, },

  CmpItemKindField = { bold = 1, fg = "#B5585F" },
  CmpItemKindProperty = { bold = 1, fg = "#B5585F" },
  CmpItemKindEvent = { bold = 1, fg = "#B5585F" },

  CmpItemKindText = { bold = 1, fg = "#9FBD73" },
  CmpItemKindEnum = { bold = 1, fg = "#9FBD73" },
  CmpItemKindKeyword = { bold = 1, fg = "#9FBD73" },

  CmpItemKindConstant = { bold = 1, fg = "#D4BB6C" },
  CmpItemKindConstructor = { bold = 1, fg = "#D4BB6C" },
  CmpItemKindReference = { bold = 1, fg = "#D4BB6C" },

  CmpItemKindFunction = { bold = 1, fg = "#A377BF" },
  CmpItemKindStruct = { bold = 1, fg = "#A377BF" },
  CmpItemKindClass = { bold = 1, fg = "#A377BF" },
  CmpItemKindModule = { bold = 1, fg = "#A377BF" },
  CmpItemKindOperator = { bold = 1, fg = "#A377BF" },

  CmpItemKindVariable = { bold = 1, fg = "#7E8294" },
  CmpItemKindFile = { bold = 1, fg = "#7E8294" },

  CmpItemKindUnit = { bold = 1, fg = "#D4A959" },
  CmpItemKindSnippet = { bold = 1, fg = "#D4A959" },
  CmpItemKindFolder = { bold = 1, fg = "#D4A959" },

  CmpItemKindMethod = { bold = 1, fg = "#6C8ED4" },
  CmpItemKindValue = { bold = 1, fg = "#6C8ED4" },
  CmpItemKindEnumMember = { bold = 1, fg = "#6C8ED4" },

  CmpItemKindInterface = { bold = 1, fg = "#58B5A8" },
  CmpItemKindColor = { bold = 1, fg = "#58B5A8" },
  CmpItemKindTypeParameter = { bold = 1, fg = "#58B5A8" },
}

for k,v in pairs(cmp_hls) do hi(0, k, v) end

-- }}}

local t = function(str) -- {{{
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end -- }}}

local has_words_before = function() -- {{{
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match('%s') == nil
end -- }}}


--[=[ -- kind icons {{{
local kind_icons = {
  Text = "",
  Method = "",
  Function = "",
  Constructor = "",
  Field = "",
  Variable = "",
  Class = "ﴯ",
  Interface = "",
  Module = "",
  Property = "ﰠ",
  Unit = "",
  Value = "",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = ""
}
local cmp_kinds = { -- vscode-like
  Text = '  ',
  Method = '  ',
  Function = '  ',
  Constructor = '  ',
  Field = '  ',
  Variable = '  ',
  Class = '  ',
  Interface = '  ',
  Module = '  ',
  Property = '  ',
  Unit = '  ',
  Value = '  ',
  Enum = '  ',
  Keyword = '  ',
  Snippet = '  ',
  Color = '  ',
  File = '  ',
  Reference = '  ',
  Folder = '  ',
  EnumMember = '  ',
  Constant = '  ',
  Struct = '  ',
  Event = '  ',
  Operator = '  ',
  TypeParameter = '  ',
}
--]=] -- }}}

  cmp.setup{ -- {{{
    completion = { -- {{{
      -- autocomplete = false,
      -- completeopt = "menu,menuone,noinsert",
    }, --}}}
    experimental = { --{{{
      ghost_text = true,
      custom_menu = false,
      menu = false,
    }, --}}}
    --[[ view = { -- {{{
      -- entries = {name = 'custom', selection_order = 'near_cursor' },
      entries = 'native',
    }, -- }}} ]]
    window = { -- {{{
      documentation = {
        border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
  --      winhighlight = 'FloatBorder:TelescopeBorder',
      },
    }, -- }}}
    formatting = { -- {{{
      border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
--      fields = { "kind", "abbr" },
--      fields = { "abbr", "kind", ... },
      format = lspkind.cmp_format({ -- {{{
--        with_text = false,
        with_text = true,
        maxwidth = 80,
--[=[ -- {{{
        symbol_map = {
          Text = "",
          Method = "",
          Function = "",
          Constructor = "",
          Field = "ﰠ",
          Variable = "",
          Class = "ﴯ",
          Interface = "",
          Module = "",
          Property = "ﰠ",
          Unit = "塞",
          Value = "",
          Enum = "",
          Keyword = "",
          Snippet = "",
          Color = "",
          File = "",
          Reference = "",
          Folder = "",
          EnumMember = "",
          Constant = "",
          Struct = "פּ",
          Event = "",
          Operator = "",
          TypeParameter = "TP"
        },
--]=] -- }}}
        menu = ({ -- {{{
          buffer = "[Buffer]",
          nvim_lsp = "[LSP]",
          luasnip = "[LuaSnip]",
          ultisnips = "[UltiSnips]",
          nvim_lua = "[Lua]",
          latex_symbols = "[LaTeX]",
          path = "[Path]",
          cmdline = "[CmdLine]",
          calc = "[Calc]",
          emoji = "[Emoji]",
          tmux = "[Tmux]",
          rg = "[RipGrep]",
          zsh = "[ZSH]",
          treesitter = "[TS]",
          nvim_lsp_signature_help = "[LSP-H]",
        }), -- }}}
      }), -- }}}
    --[=[ no-lspkind way: -- {{{
      format = function(entry, vim_item)
        -- Kind icons
        vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
        -- Source
        vim_item.menu = ({
          buffer = "[Buffer]",
          nvim_lsp = "[LSP]",
          luasnip = "[LuaSnip]",
          nvim_lua = "[Lua]",
          latex_symbols = "[LaTeX]",
        })[entry.source.name]
        return vim_item
      end
    --]=] -- }}}
    }, -- }}}
    sorting = { -- {{{
      comparators = { -- {{{
        cmp.config.compare.offset,
        cmp.config.compare.exact,
        function(...) return cmp_buffer:compare_locality(...) end,
--        cmp.config.compare.sort_text,
        cmp.config.compare.score,
        req"cmp-under-comparator".under,
        cmp.config.compare.recently_used,
        cmp.config.compare.kind,
        cmp.config.compare.sort_text,
--        cmp.config.compare.recently_used,
        cmp.config.compare.length,
        cmp.config.compare.order,
      }, -- }}}
    }, -- }}}
    snippet = { -- {{{
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        -- vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        luasnip.lsp_expand(args.body) -- For `luasnip` users.
        --vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
        -- require'snippy'.expand_snippet(args.body) -- For `snippy` users.
      end,
    }, -- }}}
    mapping = { -- {{{
      ["<Tab>"] = cmp.mapping({ -- {{{
        c = function() -- {{{
          if cmp.visible() then
            cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
          else
            cmp.complete()
          end
        end, -- }}}
        i = function(fallback) -- {{{
          if cmp.visible() then
            cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
          elseif has_words_before() and luasnip.expand_or_jumpable() then
            vim.api.nvim_feedkeys(t'<Plug>luasnip-expand-or-jump','', true)
         -- elseif vim.fn["UltiSnips#CanJumpForwards"]() == 1 then
         --   vim.api.nvim_feedkeys(t("<Plug>(ultisnips_jump_forward)"), 'm', true)
          else
            fallback()
          end
        end, -- }}}
        s = function(fallback) -- {{{
          if cmp.visible() then
            cmp.select_next_item({ behavior = cmp.SelectBehavior.Insert })
          elseif has_words_before() and luasnip.expand_or_jumpable() then
            vim.api.nvim_feedkeys(t'<Plug>luasnip-expand-or-jump','', true)
         -- elseif vim.fn["UltiSnips#CanJumpForwards"]() == 1 then
         --   vim.api.nvim_feedkeys(t("<Plug>(ultisnips_jump_forward)"), 'm', true)
          else
            fallback()
          end
        end -- }}}
      }), -- }}}
      ["<S-Tab>"] = cmp.mapping({ -- {{{
        c = function() -- {{{
          if cmp.visible() then
            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
          else
            cmp.complete()
          end
        end, -- }}}
        i = function(fallback) -- {{{
          if cmp.visible() then
            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
          elseif luasnip.jumpable(-1) then
            vim.api.nvim_feedkeys(t'<Plug>luasnip-jump-prev', '', true)
          -- elseif vim.fn["UltiSnips#CanJumpBackwards"]() == 1 then
          --   return vim.api.nvim_feedkeys( t("<Plug>(ultisnips_jump_backward)"), 'm', true)
          else
            fallback()
          end
        end, -- }}}
        s = function(fallback) -- {{{
          if cmp.visible() then
            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Insert })
          elseif luasnip.jumpable(-1) then
            vim.api.nvim_feedkeys(t'<Plug>luasnip-jump-prev', '', true)
          -- elseif vim.fn["UltiSnips#CanJumpBackwards"]() == 1 then
          --   return vim.api.nvim_feedkeys( t("<Plug>(ultisnips_jump_backward)"), 'm', true)
          else
            fallback()
          end
        end -- }}}
      }), --}}}
--      ['<Down>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
--      ['<Up>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }), {'i'}),
      ['<Down>'] = cmp.mapping({ -- {{{
        c = function() -- {{{
          if cmp.visible() then
            cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
          else
            vim.api.nvim_feedkeys(t('<Down>'), 'n', true)
          end
        end, -- }}}
        i = function(fallback) -- {{{
          if cmp.visible() then
            cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
          else
            fallback()
          end
        end -- }}}
      }), -- }}}
      ['<Up>'] = cmp.mapping({ -- {{{
        c = function() -- {{{
          if cmp.visible() then
            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
          else
            vim.api.nvim_feedkeys(t('<Up>'), 'n', true)
          end
        end, -- }}}
        i = function(fallback) -- {{{
          if cmp.visible() then
            cmp.select_prev_item({ behavior = cmp.SelectBehavior.Select })
          else
            fallback()
          end
        end -- }}}
      }), -- }}}
      ['<Right>'] = cmp.mapping({ -- {{{
        i = function(fallback) -- {{{
          --[[ if cmp.visible() and has_words_before() and luasnip.expand_or_jumpable() then
            vim.api.nvim_feedkeys(t'<Plug>luasnip-expand-or-jump', '', true)
          else ]]
            fallback()
          -- end
        end, -- }}}
        s = function(fallback) -- {{{
          --if has_words_before() and luasnip.expand_or_jumpable() then
          if luasnip.expand_or_jumpable() then
            vim.api.nvim_feedkeys(t'<Plug>luasnip-expand-or-jump', '', true)
          else
            fallback()
          end
        end, -- }}}
      }), -- }}}
      ['<Left>'] = cmp.mapping({ -- {{{
        i = function(fallback) -- {{{
          --[[ if luasnip.jumpable(-1) then
            vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Plug>luasnip-jump-prev', true, true, true), '', true)
          else ]]
            fallback()
          -- end
        end, -- }}}
        s = function(fallback) -- {{{
          if luasnip.jumpable(-1) then
            vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes('<Plug>luasnip-jump-prev', true, true, true), '', true)
          else
            fallback()
          end
        end, -- }}}
      }), -- }}}

      ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), {'i', 'c'}),
      ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), {'i', 'c'}),
--      ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), {'i', 'c'}),
      ['<C-e>'] = cmp.mapping({ i = cmp.mapping.close(), c = cmp.mapping.close() }),
      ['<Esc>'] = cmp.mapping({ --{{{
        i = function(fallback) -- {{{
          if cmp.visible() then
            cmp.mapping.close()()
          else
            fallback()
          end
        end, -- }}}

        c = function() -- {{{
          if cmp.visible() then
            cmp.mapping.close()()
          else
            vim.api.nvim_feedkeys(t'<C-c>',"",true)
            -- fallback() somewhy executes commandline
          end
        end, -- }}}
      }), -- }}}
      ['<CR>'] = cmp.mapping({ -- {{{
        i = cmp.mapping.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false }),
        c = function(fallback) -- {{{
          if cmp.visible() then
            cmp.confirm({ behavior = cmp.ConfirmBehavior.Replace, select = false })
          else
            fallback()
          end
        end -- }}}
      }), -- }}}
      --[=[ -- {{{
      ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
      ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
      ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
      ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
      ['<C-e>'] = cmp.mapping({
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
      }),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
      --]=] -- }}}
    }, -- }}}
    sources = cmp.config.sources({ -- {{{
      { name = "nvim_lua", dup = 0. },
      { name = 'luasnip', dup = 0, },
      -- { name = 'vsnip' },
      -- { name = 'ultisnips' },
      -- { name = 'snippy' },
      { name = 'treesitter', dup = 0, },
      { name = 'nvim_lsp', dup = 0, },
      { name = "calc", dup = 0, },
      { name = "emoji", -- {{{
        dup = 0,
--        option = { insert = false, },
      }, -- }}}
      { name = "path", dup = 0, },
      { name = "nvim_lsp_signature_help", dup = 0, },
      { name = "rg", -- {{{
        dup = 0,
        option = {},
      }, -- }}}
      { name = "latex_symbols", dup = 0, },
      -- { name = 'zsh' },
      -- { name = 'tmux', -- {{{
          -- dup = 0,
        -- option = { -- {{{
          -- all_panes = false,
          -- label = '[tmux]',
--          trigger_characters = { '.' },
--          trigger_characters_ft = {} -- { filetype = { '.' } }
        -- } -- }}}
      -- } -- }}}
    }, {
      { name = 'buffer',
        dup = 0,
        option = {
          -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%([\-.]\w*\)*\)]],
          -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\S*\%([\-.]\S*\)*\)]],
          -- keyword_pattern = [[\%(-\?\d\+\%(\.\d\+\)\?\|\h\w*\%([\-.]\w*\)*\|[а-яА-ЯеёЕЁ]*\)]], -- ломает ввод в текущем виде
          get_bufnrs = function() -- visible buffers
            local bufs = {}
            for _, win in ipairs(vim.api.nvim_list_wins()) do
              -- local buf = vim.api.nvim_get_current_buf()
              local buf = vim.api.nvim_win_get_buf(win)
              local byte_size = vim.api.nvim_buf_get_offset(buf, vim.api.nvim_buf_line_count(buf))
              if byte_size <= 1024 * 1024 then -- Max: 1 MB (TODO: maybe even lesser?)
                bufs[buf] = true
              end
            end
            return vim.tbl_keys(bufs)
          --[[
            return vim.api.nvim_list_bufs() -- all buffers
          --]]
          end,
        },
      },
    }) --}}}
  } -- }}}

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline('/', { -- {{{
    completion = { autocomplete = false },
    sources = {
      {
        name = 'nvim_lsp_document_symbol',
      }, {
        name = 'buffer',
        option = {
          keyword_pattern = [=[[^\v[:blank:]].*]=], -- "\?v?" - is a hack to care of loupe's hook
          -- "\k\+" -- ?
        }
      }
    }
  }) -- }}}
  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', { -- {{{
    completion = { autocomplete = false },
    sources = cmp.config.sources({
      { name = "nvim_lua" },
      { name = 'path', max_item_count = 20, },
    }, {
      { name = "nvim_lua" },
      { name = 'cmdline', max_item_count = 30, },
    })
  }) -- }}}

 -- DAP
  cmp.setup.filetype({ "dap-repl", "dapui_watches" }, { -- {{{
    sources = {
      { name = "dap" },
    },
  }) -- }}}
