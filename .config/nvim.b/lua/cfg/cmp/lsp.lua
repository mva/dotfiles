-- luacheck: globals req
local capabilities
local cmplsp = req"cmp_nvim_lsp"
cmplsp.setup()
capabilities = cmplsp.default_capabilities(false)
return capabilities
