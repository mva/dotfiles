-- luacheck: globals req

local lsp_status = req"lsp-status"
lsp_status.register_progress()

return lsp_status
