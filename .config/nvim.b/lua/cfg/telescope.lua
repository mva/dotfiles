-- luacheck: globals req map

local telescope = req"telescope"
local telescope_sorters = req"telescope.sorters"
local telescope_previewers = req"telescope.previewers"

local fb_actions = telescope.extensions.file_browser.actions


telescope.setup{ -- {{{
  defaults = { -- {{{
    vimgrep_arguments = { -- {{{
        "rg", "--color=never", "--no-heading", "--with-filename",
        "--line-number", "--column", "--smart-case"
    }, -- }}}
    prompt_prefix = "  ",
    selection_caret = "  ",
    entry_prefix = "  ",
    initial_mode = "insert",
    selection_strategy = "reset",
    sorting_strategy = "descending",
    layout_strategy = "flex",
    layout_config = { -- {{{
      anchor = "N",
      horizontal = { -- {{{
        prompt_position = "top",
        preview_width = 0.55,
      }, -- }}}
      vertical = { -- {{{
        mirror = false,
      }, -- }}}
      width = 0.87,
      height = 0.80,
      preview_cutoff = 120
    }, -- }}}
    file_sorter = telescope_sorters.get_fuzzy_file,
    file_ignore_patterns = {},
    generic_sorter = telescope_sorters.get_generic_fuzzy_sorter,
    path_display = { "absolute" },
    winblend = 0,
    border = {},
    borderchars = {"─", "│", "─", "│", "╭", "╮", "╯", "╰"},
    color_devicons = true,
    use_less = true,
    set_env = {["COLORTERM"] = "truecolor"}, -- default = nil,
    file_previewer = telescope_previewers.vim_buffer_cat.new,
    grep_previewer = telescope_previewers.vim_buffer_vimgrep.new,
    qflist_previewer = telescope_previewers.vim_buffer_qflist.new,
    -- Developer configurations: Not meant for general override
    -- buffer_previewer_maker = telescope_previewers.buffer_previewer_maker
    scroll_strategy = "cycle",
    theme = req"telescope.themes".get_dropdown { layout_config = { prompt_position = 'top' } },
  }, -- }}}
  extensions = { -- {{{
    -- frecency = { workspaces = { name = '', } },
    fzf = { -- {{{
      fuzzy = true, -- false will only do exact matching
      override_generic_sorter = false, -- override the generic sorter
      override_file_sorter = true, -- override the file sorter
      case_mode = "smart_case" -- or "ignore_case" or "respect_case"
      -- the default case_mode is "smart_case"
    }, -- }}}
    ['ui-select'] = {
      req"telescope.themes".get_dropdown { layout_config = { prompt_position = 'top' } },
    },
    heading = { treesitter = true },
    file_browser = { -- {{{
      hijack_netwrw = true,
      hidden = true,
      mappings = { -- {{{
        i = { -- {{{
          ['<c-n>'] = fb_actions.create,
          ['<c-r>'] = fb_actions.rename,
          ['<c-h>'] = fb_actions.toggle_hidden,
          ['<c-x>'] = fb_actions.remove,
          ['<c-p>'] = fb_actions.move,
          ['<c-y>'] = fb_actions.copy,
          ['<c-a>'] = fb_actions.select_all,
        }, -- }}}
      }, -- }}}
    }, -- }}}
  }, -- }}}
  pickers = { -- {{{
    buffers = { -- {{{
      ignore_current_buffer = true,
      -- sort_mru = true,
      sort_lastused = true,
      previewer = false,
    }, -- }}}
  }, -- }}}
} -- }}}



-- telescope.load_extension 'frecency'
telescope.load_extension 'fzf'
telescope.load_extension 'ui-select'
telescope.load_extension 'notify'
telescope.load_extension 'heading'
telescope.load_extension 'file_browser'
