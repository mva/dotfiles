-- luacheck: globals vim req nmap vmap
-- local lsp_signature = req"lsp_signature"
-- local lsp_status = req"lsp-status"
-- local folding = req"folding"

vim.lsp.handlers["textDocument/publishDiagnostics"] = -- {{{
  vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics,
    {
      signs = true,
      underline = true,
      update_in_insert = true -- false -- update diagnostics insert mode
    }
  ) -- }}}

--[[{{{ custom border
local border = {
    {"╭", "FloatBorder"},
    {"─", "FloatBorder"},
    {"╮", "FloatBorder"},
    {"│", "FloatBorder"},
    {"╯", "FloatBorder"},
    {"─", "FloatBorder"},
    {"╰", "FloatBorder"},
    {"│", "FloatBorder"}
}
--}}}]]

--[[ Now, Noice take control over that
vim.lsp.handlers["textDocument/hover"] = -- {{{
  vim.lsp.with(vim.lsp.handlers.hover, {
    border = "single",
    -- focusable = false,
  })
-- }}}
vim.lsp.handlers["textDocument/signatureHelp"] = -- {{{
  vim.lsp.with(vim.lsp.handlers.signature_help, {
    border = "single",
    -- focusable = false,
  })
-- }}}
 ]]
local function goto_definition(split_cmd) -- {{{
  local util = vim.lsp.util
  local log = req"vim.lsp.log"
  local api = vim.api

  -- note, this handler style is for neovim 0.5.1/0.6, if on 0.5, call with function(_, method, result)
  local handler = function(_, result, ctx)
    if result == nil or vim.tbl_isempty(result) then
      local _ = log.info() and log.info(ctx.method, "No location found")
      return nil
    end

    if split_cmd then
      vim.cmd(split_cmd)
    end

    if vim.tbl_islist(result) then
      util.jump_to_location(result[1],"utf-8")

      if #result > 1 then
        util.set_qflist(util.locations_to_items(result))
        api.nvim_command("copen")
        api.nvim_command("wincmd p")
      end
    else
      util.jump_to_location(result,"utf-8")
    end
  end

  return handler
end -- }}}
vim.lsp.handlers["textDocument/definition"] = goto_definition() --'split')


local _M = {}

-- _M.extensions = lsp_status.extensions

-- luacheck: no unused args
function _M.on_attach(client, bufnr) -- {{{
  -- local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end -- buffer-local map
  -- local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end -- buffer-local setopt
  local opts = { remap=false, silent=true, buffer=true, } -- mapping opts

  -- buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
  vim.bo['omnifunc'] = 'v:lua.vim.lsp.omnifunc'

  -- Mappings. -- {{{
  nmap('gD', vim.lsp.buf.declaration, opts)
  nmap('gd', vim.lsp.buf.definition, opts)
  nmap('<leader>lh', vim.lsp.buf.hover, opts) -- <space>h
  nmap('gi', vim.lsp.buf.implementation, opts)
  nmap('<leader>h', vim.lsp.buf.signature_help, opts)
  nmap('<leader>lwa', vim.lsp.buf.add_workspace_folder, opts)
  nmap('<leader>lwr', vim.lsp.buf.remove_workspace_folder, opts)
  nmap('<leader>lwl', function() print(vim.inspect(vim.lsp.buf.list_workspace_folders())) end, opts)
  nmap('<leader>lld', vim.lsp.buf.type_definition, opts)
  nmap('<leader>lrn', vim.lsp.buf.rename, opts)
  nmap('<leader>lca', vim.lsp.buf.code_action, opts)
  nmap('gr', vim.lsp.buf.references, opts)
  nmap('<leader>le', vim.diagnostic.open_float, opts)
  nmap('[d', vim.diagnostic.goto_prev, opts)
  nmap(']d', vim.diagnostic.goto_next, opts)
  nmap('<leader>lq', vim.diagnostic.setloclist, opts)
  --}}}
  -- Set some keybinds conditional on server capabilities
  if client.server_capabilities.document_formatting then -- {{{
    nmap("<leader>lf", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)
  end
  if client.server_capabilities.document_range_formatting then
    vmap("<leader>lf", "<cmd>lua vim.lsp.buf.range_formatting()<CR>", opts)
  end -- }}}

  -- Set autocommands conditional on server_capabilities -- {{{
  if client.server_capabilities.document_highlight then -- TODO: move to aus in customizations
    vim.api.nvim_exec([[
      hi LspReferenceRead  gui=bold,nocombine guifg=#ee9929
      hi LspReferenceText  gui=bold,nocombine guifg=#ee1b29
      hi LspReferenceWrite gui=bold,nocombine guifg=#ee2999
      augroup lsp_document_highlight
        autocmd! * <buffer>
        autocmd CursorHold <buffer> lua vim.lsp.buf.document_highlight()
        autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()
      augroup END
    ]], false)
  end -- }}}
end -- }}}

return _M

