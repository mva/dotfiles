-- luacheck: globals req vim T nmap, no max line length

local handlers = req"cfg.lsp.handlers"
local on_attach = handlers.on_attach

local capabilities = req"cfg.lsp.caps"

local setups = {}
setups.default = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  flags = {
    debounce_text_changes = 150 -- ?
  },
  --    root_dir = vim.loop.cwd,
} -- }}}

setups.gopls = { -- {{{
--{{{   cmd = {
--      --("%s/bin/gopls"):format(goenv().GOROOT),
--      ("%s/bin/gopls"):format(gopath()),
--      "serve",
--}}}   },
  settings = {
    gopls = {
      analyses = {
        unusedresults = true,
        unusedparams = true,
        composites = true,
      },
      staticcheck = true,
    },
  },
 capabilities = capabilities,
 on_attach = on_attach,
} -- }}}
setups.ccls = { -- {{{
  init_options = {
    highlight = {
      lsRanges = true,
    },
  },
  on_attach = on_attach,
  capabilities = capabilities,
  } -- }}}

setups.clangd = {
  -- handlers = ext.clangd.setup(),
  init_options = {
    clangdFileStatus = true
  },
  on_attach = on_attach,
  capabilities = capabilities,
}

-- Schemastore {{{
local schemastore_schemas;

local plugins = _G.packer_plugins or {}
if plugins['schemastore.nvim'] then
  schemastore_schemas = req'schemastore'.json.schemas()
end
-- }}}

setups.jsonls = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    json = {
      schemas = schemastore_schemas,
      validate = { enable = true },
    },
  },
} -- }}}

setups.yamlls = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    yaml = {
      schemas = schemastore_schemas,
      validate = { enable = true },
    },
  },
} -- }}}

setups.ansiblels = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    ansible = {
      ansible = {
        useFullyQualifiedCollectionNames = true,
      }
    }
  }
} -- }}}

setups.pylsp = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    pylsp = {
      configurationSources = { "pyflake8" },
      plugins = {
        flake8 = {
          enabled = true,
          exclude = { '.git', '__pycache__' },
          -- ignore = {"E501"},
          maxComplexity = 10,
        },
        pycodestyle = { enabled = false, },
        pyflakes = { enabled = false, },
        mccabe = { enabled = false, },
      },
    },
  },
} -- }}}

setups.ltexls = { -- {{{
  on_attach = on_attach,
  capabilities = capabilities,
  use_spellfile = false,
  filetypes = { "latex", "tex", "bib", "markdown", "gitcommit", "text", },
  settings = {
    ltex = {
      enabled = { "latex", "tex", "bib", "markdown", "gitcommit", "text", },
      language = "auto",
      diagnosticSeverity = "hint",
      sentenceCacheSize = 2000,
      additionalRules = {
        enablePickyRules = true,
        motherTongue = "ru",
      },
      disabledRules = {
 --       fr = { "APOS_TYP", "FRENCH_WHITESPACE" } -- example
      },
      dictionary = (function()
        -- For dictionary, search for files in the runtime to have
        -- and include them as externals the format for them is
        -- dict/{LANG}.txt
        --
        -- Also add dict/default.txt to all of them
        local files = {}
        for _, file in ipairs(vim.api.nvim_get_runtime_file("dict/*", true)) do
          local lang = vim.fn.fnamemodify(file, ":t:r")
          local fullpath = vim.fs.normalize(file, ":p")
          files[lang] = { ":" .. fullpath }
        end

        if files.default then
          for lang, _ in pairs(files) do
            if lang ~= "default" then
              vim.list_extend(files[lang], files.default)
            end
          end
          files.default = nil
        end
        return files
      end)(),
    },
  },
} -- }}}


return setups
