-- luacheck: globals vim req T
-- {{{ capabilities
local def_caps = vim.lsp.protocol.make_client_capabilities()
local cmplsp_caps = req"cfg.cmp.lsp"

local capabilities = vim.tbl_deep_extend(
  'force',
  def_caps or {},
  cmplsp_caps
)
-- capabilities = vim.tbl_extend('keep', capabilities or {}, lsp_status.capabilities)

capabilities.textDocument.foldingRange = { -- {{{
    dynamicRegistration = false,
    lineFoldingOnly = true,
} -- }}}

capabilities.textDocument.completion.completionItem.documentationFormat = { -- {{{
  "markdown",
  "plaintext"
} -- }}}
-- }}}

capabilities.textDocument.completion.completionItem.snippetSupport = true

return capabilities
