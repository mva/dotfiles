-- luacheck: globals req vim T, no max line length

local _M = T{}

req"lsp_lines".setup()

vim.diagnostic.config {
  virtual_text = false,
  virtual_lines = true,
}

function _M.toggle()
  local c = vim.diagnostic.config()
  local vl = not(c.virtual_lines)
  local vt = not(c.virtual_text)

  if vt == true then
    vt = {
      prefix = "",
      spacing = 1 -- 0
    }
  end

  -- req"lsp_lines".toggle()
  vim.diagnostic.config {
    virtual_lines=vl,
    virtual_text=vt,
  }
end

return _M
