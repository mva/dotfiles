-- luacheck: globals req
req"mason".setup({
  ui = {
    icons = {
      server_installed = "✅",
      server_uninstalled = "❌",
      server_pending = "🔄",
    },
  keymaps = { apply_language_filter = "<C-f>", },
  border = "double",
  },
})
