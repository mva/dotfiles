-- luacheck: no max line length
-- luacheck: globals vim req nmap
-- luacheck: globals nnoremap

local dap = req"dap"
--[[
]]
local api = vim.api
local keymap_restore = {}
dap.listeners.after['event_initialized']['me'] = function()
  for _, buf in pairs(api.nvim_list_bufs()) do
    local keymaps = api.nvim_buf_get_keymap(buf, 'n')
    for _, keymap in pairs(keymaps) do
      if keymap.lhs == "K" then
        table.insert(keymap_restore, keymap)
        api.nvim_buf_del_keymap(buf, 'n', 'K')
      end
    end
  end
  nmap('K', req"dap.ui.widgets".hover(), { silent = true })
end

dap.listeners.after['event_terminated']['me'] = function()
  for _, keymap in pairs(keymap_restore) do
    api.nvim_buf_set_keymap(
      keymap.buffer,
      keymap.mode,
      keymap.lhs,
      keymap.rhs,
      { silent = keymap.silent == 1 }
    )
  end
  keymap_restore = {}
end

dap.configurations.cpp = {
    {
      -- If you get an "Operation not permitted" error using this, try disabling YAMA:
      --  echo 0 | sudo tee /proc/sys/kernel/yama/ptrace_scope
      name = "Attach to process",
      type = 'cpp',  -- Adjust this to match your adapter name (`dap.adapters.<name>`)
      request = 'attach',
      pid = require('dap.utils').pick_process,
      args = {},
    },
}
--[[
dap.configurations.python = {
  {
    type = 'python';
    request = 'launch';
    name = "Launch file";
    program = "${file}";
    pythonPath = function()
      return '/usr/bin/python'
    end;
  },
} ]]

nnoremap("<Leader>cc",  dap.continue, { silent = true})
nnoremap("<Leader>sv",  dap.step_over, { silent = true})
nnoremap("<Leader>si",  dap.step_into, { silent = true})
nnoremap("<Leader>su",  dap.step_out, { silent = true})
nnoremap("<Leader>b",   dap.toggle_breakpoint, { silent = true})
nnoremap("<Leader>B",   function() dap.set_breakpoint(vim.fn.input('Breakpoint condition: ')) end, { silent = true})
nnoremap("<Leader>lp",  function() dap.set_breakpoint(nil, nil, vim.fn.input('Log point message: ')) end, { silent = true})
nnoremap("<Leader>dr",  dap.repl.open, { silent = true})
nnoremap("<Leader>dl",  dap.run_last, { silent = true})
