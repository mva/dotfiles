-- luacheck: globals req vim nnoremap T

-- Unless you are still migrating, remove the deprecated commands from v1.x
vim.g.neo_tree_remove_legacy_commands=1

local ntree = req"neo-tree"


local my_components = T{ -- {{{
  symlink_target = function(config, node, _) -- state -- {{{
    local highlights = req"neo-tree.ui.highlights"
    if node.is_link then -- {{{
      return { -- {{{
        text = string.format(" %s", node.link_to),
        highlight = config.highlight or highlights.SYMBOLIC_LINK_TARGET,
      } -- }}}
    else
      return {}
    end -- }}}
  end, -- }}}
  --[[ icon = function(config, node, state) -- {{{
    local highlights = req"neo-tree.ui.highlights"
    local icon = config.default or " "
    local highlight = config.highlight or highlights.FILE_ICON
    if node.type == "directory" then
      highlight = highlights.DIRECTORY_ICON
      if node.loaded and not node:has_children() then
        icon = config.folder_empty or config.folder_open or "-"
      elseif node:is_expanded() then
        icon = config.folder_open or "-"
      else
        icon = config.folder_closed or "+"
      end
    elseif node.type == "file" then
      local success, web_devicons = pcall(require, "nvim-web-devicons")
      if success then
        local devicon, hl = web_devicons.get_icon(node.name, node.ext)
        icon = devicon or icon
        highlight = hl or highlight
      end
    end

    local filtered_by = req"neo-tree.sources.common.components".filtered_by(config, node, state)

    return {
      text = icon .. " ",
      highlight = filtered_by.highlight or highlight,
    }
  end,
-- }}}]]
} -- }}}


ntree.setup{ -- {{{
  close_if_last_window = true, -- Close Neo-tree if it is the last window left in the tab
  -- close_if_last_window = false, -- Close Neo-tree if it is the last window left in the tab
  popup_border_style = "rounded",
  enable_git_status = true,
  enable_diagnostics = true,
  sort_case_insensitive = false, -- used when sorting files and directories in the tree
  sort_function = nil , -- use a custom function for sorting files and directories in the tree
  -- sort_function = function (a,b)
  --       if a.type == b.type then
  --           return a.path > b.path
  --       else
  --           return a.type > b.type
  --       end
  --   end , -- this sorts files and directories descendantly
  default_component_configs = { -- {{{
    container = { -- {{{
      enable_character_fade = true
     }, -- }}}
    indent = { -- {{{
      indent_size = 2,
      padding = 1, -- extra padding on left hand side
      -- indent guides
      with_markers = true,
      indent_marker = "│",
      last_indent_marker = "└",
      highlight = "NeoTreeIndentMarker",
      -- expander config, needed for nesting files
      with_expanders = nil, -- if nil and file nesting is enabled, will enable expanders
      expander_collapsed = "",
      expander_expanded = "",
      expander_highlight = "NeoTreeExpander",
    }, -- }}}
    icon = { -- {{{
      folder_closed = "",
      -- folder_closed = "📁",
      folder_open = "",
      -- folder_open = "📂",
      folder_empty = "",
      -- folder_empty = "ﰊ",
      -- folder_empty = "",
      -- The next two settings are only a fallback, if you use nvim-web-devicons and configure default icons there
      -- then these will never be used.
      default = "*",
      highlight = "NeoTreeFileIcon"
    }, -- }}}
    modified = { -- {{{
      symbol = "[+]",
      highlight = "NeoTreeModified",
    }, -- }}}
    name = { -- {{{
      trailing_slash = false,
      use_git_status_colors = true,
      highlight = "NeoTreeFileName",
    }, -- }}}
    git_status = { -- {{{
      symbols = { -- {{{
        -- Change type
        added     = "", -- or "✚", but this is redundant info if you use git_status_colors on the name
        modified  = "", -- or "", but this is redundant info if you use git_status_colors on the name
        deleted   = "✖",-- this can only be used in the git_status source
        renamed   = "",-- this can only be used in the git_status source
        -- Status type
        untracked = "",
        ignored   = "",
        unstaged  = "",
        staged    = "",
        conflict  = "",
      }, -- }}}
      align = "right",
    }, -- }}}
  }, -- }}}
  window = { -- {{{
    position = "left",
    width = 40,
    mapping_options = { -- {{{
      noremap = true,
      nowait = true,
    }, -- }}}
    mappings = { -- {{{
      ["<space>"] = { -- {{{
          "toggle_node",
          nowait = false, -- disable `nowait` if you have existing combos starting with this char that you want to use
      }, -- }}}
      ["<2-LeftMouse>"] = "open",
      ["<cr>"] = "open",
      ["<esc>"] = "revert_preview",
      ["P"] = { "toggle_preview", config = { use_float = true } },
      ["S"] = "open_split",
      ["s"] = "open_vsplit",
      -- ["S"] = "split_with_window_picker",
      -- ["s"] = "vsplit_with_window_picker",
      ["t"] = "open_tabnew",
      -- ["<cr>"] = "open_drop",
      -- ["t"] = "open_tab_drop",
      ["w"] = "open_with_window_picker",
      --["P"] = "toggle_preview", -- enter preview mode, which shows the current node without focusing
      ["C"] = "close_node",
      ["z"] = "close_all_nodes",
      --["Z"] = "expand_all_nodes",
      ["a"] = { -- {{{
        "add",
        -- some commands may take optional config options, see `:h neo-tree-mappings` for details
        config = { -- {{{
          show_path = "none" -- "none", "relative", "absolute"
        } -- }}}
      }, -- }}}
      ["A"] = "add_directory", -- also accepts the optional config.show_path option like "add".
      ["d"] = "delete",
      ["r"] = "rename",
      ["y"] = "copy_to_clipboard",
      ["x"] = "cut_to_clipboard",
      ["p"] = "paste_from_clipboard",
      ["c"] = "copy", -- takes text input for destination, also accepts the optional config.show_path option like "add":
      -- ["c"] = { -- {{{
      --  "copy",
      --  config = { -- {{{
      --    show_path = "none" -- "none", "relative", "absolute"
      --  } -- }}}
      --} -- }}}
      ["m"] = "move", -- takes text input for destination, also accepts the optional config.show_path option like "add".
      ["q"] = "close_window",
      ["R"] = "refresh",
      ["?"] = "show_help",
      ["<"] = "prev_source",
      [">"] = "next_source",
    } -- }}}
  }, -- }}}
  renderers = { -- {{{
    directory = { -- {{{
      { "indent" },
      { "icon" },
      { "current_filter" },
      { "container", -- {{{
        content = { -- {{{
          { "name", zindex = 10, },
          { "symlink_target", -- {{{
            zindex = 10,
            highlight = "NeoTreeDirectoryName",
          }, -- }}}
          { "clipboard", zindex = 10 },
          { "diagnostics", errors_only = true, zindex = 20, align = "right" },
          { "git_status", zindex = 20, align = "right" },
        }, -- }}}
      }, -- }}}
    }, -- }}}
    file = { -- {{{
      { "indent" },
      { "icon" },
      { "container", -- {{{
        content = { -- {{{
          { "name", zindex = 10, },
          { "symlink_target",
            zindex = 10,
            highlight = "NeoTreeFileName",
          },
          { "clipboard", zindex = 10 },
          { "bufnr", zindex = 10 },
          { "modified", zindex = 20, align = "right" },
          { "diagnostics",  zindex = 20, align = "right" },
          { "git_status", zindex = 20, align = "right" },
        }, -- }}}
      }, -- }}}
    }, -- }}}
    message = { -- {{{
      { "indent", with_markers = true },
      { "name", highlight = "NeoTreeMessage" },
    }, -- }}}
    terminal = { -- {{{
      { "indent" },
      { "icon" },
      { "name" },
      { "bufnr" }
    } -- }}}
  }, -- }}}
  nesting_rules = {},
  filesystem = { -- {{{
    filtered_items = { -- {{{
      visible = true, -- when true, they will just be displayed differently than normal items
      hide_dotfiles = true,
      hide_gitignored = true,
      hide_hidden = true, -- only works on Windows for hidden files/directories
      hide_by_name = { -- {{{
        --"node_modules"
      }, -- }}}
      hide_by_pattern = { -- uses glob style patterns -- {{{
        --"*.meta",
        --"*/src/*/tsconfig.json",
      }, -- }}}
      always_show = { -- remains visible even if other settings would normally hide it -- {{{
        --".gitignored",
      }, -- }}}
      never_show = { -- remains hidden even if visible is toggled to true, this overrides always_show -- {{{
        --".DS_Store",
        --"thumbs.db"
      }, -- }}}
      never_show_by_pattern = { -- uses glob style patterns -- {{{
        --".null-ls_*",
      }, -- }}}
    }, -- }}}
    follow_current_file = false, -- This will find and focus the file in the active buffer every
                                 -- time the current file is changed while the tree is open.
    group_empty_dirs = false, -- when true, empty folders will be grouped together
    hijack_netrw_behavior = "open_default", -- netrw disabled, opening a directory opens neo-tree
                                            -- in whatever position is specified in window.position
                          -- "open_current",  -- netrw disabled, opening a directory opens within the
                                            -- window like netrw would, regardless of window.position
                          -- "disabled",    -- netrw left alone, neo-tree does not handle opening dirs
    use_libuv_file_watcher = false, -- This will use the OS level file watchers to detect changes
                                    -- instead of relying on nvim autocmd events.
    window = { -- {{{
      position = "float",
      mappings = { -- {{{
        ["<bs>"] = "navigate_up",
        ["."] = "set_root",
        ["H"] = "toggle_hidden",
        ["/"] = "fuzzy_finder",
        ["D"] = "fuzzy_finder_directory",
        ["f"] = "filter_on_submit",
        ["<c-x>"] = "clear_filter",
        ["[g"] = "prev_git_modified",
        ["]g"] = "next_git_modified",
      } -- }}}
    }, -- }}}
    components = my_components,
  }, -- }}}
  buffers = { -- {{{
    follow_current_file = true, -- This will find and focus the file in the active buffer every
                                 -- time the current file is changed while the tree is open.
    group_empty_dirs = true, -- when true, empty folders will be grouped together
    show_unloaded = true,
    window = { -- {{{
      position = "float",
      mappings = { -- {{{
        ["bd"] = "buffer_delete",
        ["<bs>"] = "navigate_up",
        ["."] = "set_root",
      } -- }}}
    }, -- }}}
    components = my_components,
  }, -- }}}
  git_status = { -- {{{
    window = { -- {{{
      position = "float",
      mappings = { -- {{{
        ["A"]  = "git_add_all",
        ["gu"] = "git_unstage_file",
        ["ga"] = "git_add_file",
        ["gr"] = "git_revert_file",
        ["gc"] = "git_commit",
        ["gp"] = "git_push",
        ["gg"] = "git_commit_and_push",
      } -- }}}
    }, -- }}}
    components = my_components,
  }, -- }}}
  event_handlers = { -- {{{
    { -- {{{
      event = 'after_render',
      handler = function (state)
        -- local state = req"neo-tree.sources.manager".get_state"filesystem"
        if not req"neo-tree.sources.common.preview".is_active() then
          -- print(vim.inspect(state.config)) --.config)
          -- state.config = { use_float = true } -- or whatever your config is
          state.commands.toggle_preview(state)
        end
      end
    }, -- }}}
  }, -- }}}
} -- }}}

vim.api.nvim_set_hl(0,"NeoTreeTitleBar", { bg = "NONE" })

-- auto_close {{{
-- req"lib.funcs".au("BufLeave", "neo-tree*", function() ntree.close_all() end)
-- }}}
