-- luacheck: globals vim

local has = vim.fn.has
local empty = vim.fn.empty

-- Encoding/Multibyte
if has('multi_byte') then -- If multibyte support is available and
  if vim.o.enc ~= "utf-8" then -- the current encoding is not Unicode,
    if empty(vim.o.tenc) > 0 then -- default to
      vim.o.tenc = vim.o.enc -- using the current encoding for terminal output
    end -- unless another terminal encoding was already set
    vim.o.enc="utf-8" -- but use utf-8 for all data internally
  end
end
