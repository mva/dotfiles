-- luacheck: globals req vim T
-- TODO: rewrite to normal way

local luakai_path = ("%s/%s"):format(vim.fn.stdpath('data'),"site/pack/packer/start/luakai/colors/luakai.lua")
if vim.fn.filereadable(luakai_path) == 1 then
  vim.cmd[[
    colorscheme luakai
  ]]
end
-- colorscheme pmenu
-- colorscheme statusline
