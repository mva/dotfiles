-- luacheck: globals req vim T
local opt = vim.opt
local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local go = vim.go
-- local f = req'lib.funcs'

local data  = fn.stdpath("data")
local cache = fn.stdpath("cache")

local enabled = T{
  "modeline",
  "modifiable",
  "ttyfast",
  "hidden",
  "showmatch",
  "ignorecase",
  "smartcase",
  "hlsearch",
  "incsearch",
  "showmatch",
  "relativenumber",
  "number",
  -- "ruler",
  -- "title",
  -- "cursorline",
  -- "cursorcolumn",
  "visualbell",
  "showfulltag",
  "writeany",
  "termguicolors",
}

local disabled = T{
  "compatible",
  "showmode",
  -- "showcmd",
  "autoindent",
  "smartindent",
  "cindent",
  -- "wrapscan",
  "wrap",
}

cmd[[filetype on]]
cmd[[filetype plugin on]]
cmd[[filetype indent on]]
cmd[[syntax on]]

local enc = 'utf-8'
opt.encoding = enc
-- opt.termencoding = enc -- removed
opt.fileencoding = enc
opt.fileencodings = {
  "utf-8",
  "cp1251",
  "koi8-r",
  "cp866",
  "latin1"
}

opt.mouse = {}--"a" -- nv
-- opt.scrolljump = 5
-- opt.scrolloff = 3
opt.matchtime = 2

opt.formatoptions = "qn1crjol"
--[=[ ^^^^^^
  q = Format text with gq, but don't format as I type.
  n = gq recognizes numbered lists, and will try to
  1 = break before, not after, a 1 letter word
  c = auto-wrap comments using textwidth, inserting comment leader automatically
  r = Automatically insert the current comment leader after hitting <Enter> in Insert mode
  t = auto-wrap using textwidth
  j = Where it makes sense, remove a comment leader when joining lines.

  see :help fo-table

  default = "tcqj"
--]=]
opt.nrformats = {
  "bin",
  "hex",
  "alpha",
  "octal"
} -- maybe disable alpha? or even hex? (and I also never used octal in real life, IIRC)

opt.shada:append"r/tmp" -- don't save backups and swaps for stuff in /tmp

if os.getenv"VIM_NOBACKUP" then
  opt.shada:append(("n/tmp/.%s.shada"):format(os.getenv"USER"))
  disabled:insert"backup"
  disabled:insert"swapfile"
  disabled:insert"undofile"
else
  local backupdir = ("%s/backup"):format(cache)
  if not fn.isdirectory(backupdir) then
    fn.mkdir(backupdir,"p")
  end
  opt.backupdir = backupdir
  opt.directory = ("%s/swap"):format(data)
  -- opt.undodir = ("%s/undo"):format(cache)
  enabled:insert"writebackup"
  enabled:insert"backup"
  enabled:insert"undofile"
  opt.updatetime = 100
  opt.backupext = "~"
  opt.backupskip = {}
end

opt.signcolumn = "number"
opt.numberwidth = 1
opt.matchpairs:append"<:>"
opt.tabstop = 2
opt.shiftwidth = 2
opt.backspace = {
  "indent",
  "eol",
  "start",
}

opt.whichwrap = T{
  "b", "s", -- {back,}space
  "<", ">", -- <left>/<right> in N and V
  "[", "]", -- <left>/<right> in I and R
}:concat()

opt.history = 10000
opt.undolevels = 5000
opt.laststatus = 3

opt.foldmethod = "manual" -- "expr" -- indent, marker, syntax
-- opt.foldexpr="MyFoldexpr(v:lnum)"
-- opt.foldtext="MyFoldtext()"

opt.foldlevel = 99
opt.foldlevelstart = 99
opt.foldenable = true

opt.viewoptions = {
  "folds",
  "options",
  "cursor",
  "unix",
  "slash",
}
opt.virtualedit = "onemore"

--[=[
opt.completeopt = {
  "menuone",
  "noselect",
  "noinsert",
}
opt.completeopt:remove("preview")
--]=]

opt.completeopt = {
  "menu",
  "menuone",
  "noselect",
} -- recommended by nvim-cmp readme

-- let @/=''

opt.shortmess="filmnrxoOtTaFWIcqsA"

opt.sessionoptions:append"tabpages"
opt.sessionoptions:append"globals"

-- Just in case it will be needeed whenever:
-- opt.guicursor = nil

-- TODO: check, maybe not needed anymore!
-- hack to stop nvim to detect Konsole when in tmux
-- (I made cursor management stuff in tmux instead).
if os.getenv("TERM"):match"^tmux*" then
  cmd([[let $KONSOLE_DBUS_SERVICE="" | let $KONSOLE_DBUS_SESSION="" | ]]
    ..[[let $KONSOLE_PROFILE_NAME="" | let $KONSOLE_VERSION=""]])
end

if go.term == "linux" then
  opt.background="dark"
  -- colorscheme desert
end

opt.pastetoggle = "<F12>"

--[=[
  opt.t_SI = "\<Esc>]12;purple\x7"
  opt.t_EI = "\<Esc>]12;blue\x7"
--]=]


for _,e in ipairs(enabled) do
  opt[e] = true
end
for _,d in ipairs(disabled) do
  opt[d] = false
end

g.colorcol = 80
g.python_host_prog = nil
g.python3_host_prog = "/usr/bin/python3"

-- I dont't use ruby/perl/js providers
g.loaded_ruby_provider = 0
g.loaded_perl_provider = 0
g.loaded_node_provider = 0

-- TODO: move to filetype-specific config?
g.vimpager_disable_x11 = 1
g.vimpager_passthrough = 1

-- TODO: move to "plugins settings"?
g.localvimrc_name = {
  ".local.vimrc",
  ".vimrc"
}
g.localvimrc_persistent = 2
g.localvimrc_persistence_file = ("%s/localvimrc.persistent"):format(data)

g.vimsyn_embed = "lPr" -- highlight embedded (l)ua, (P)ython and (r)uby syntax
g.vimsyn_folding = "afP" -- support (a)utogroups, (f)unctions, (P)ython-scripts folding
-- g.vimsyn_noerror = 1

g.netrw_http_cmd="curl -sLo"
g.netrw_dav_cmd="curl -sLo"
--g.netrw_menu=0
--g.netrw_use_errorwindow=0
g.netrw_silent=1
g.netrw_bufsettings = 'nohidden noma nomod nonu nowrap ro buflisted'


-- vim: ts=2 sw=2 et
