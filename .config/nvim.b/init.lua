-- luacheck: globals req vim T

-- [[ Starting from stuff that shouldn't cast any notification (mangling over variables) ]]
local g = vim.g
local o = vim.opt
local cmd = vim.cmd

-- Set mapleader here, before ay plugins, to not miss any leader mappings because of switching
g.mapleader = [[ ]]
g.maplocalleader = [[,]]

-- Skip some remote provider loading
g.loaded_python3_provider = 0
g.loaded_node_provider = 0
g.loaded_perl_provider = 0
g.loaded_ruby_provider = 0


-- Do not source the default filetype.vim as NVim had migrated to filetype.lua,
-- and also we'll load filetype.nvim later (and it's much more faster)
-- and also we'll load polyglot then, which is slow as fuck anyway.
g.do_filetype_lua = 1
g.did_load_filetypes = 0

o.termguicolors = true -- kludge for packer_compiled and early-preloading of nvim-notify



-- [[ Below is the stuff that can cause notifications ]]

--pcall(require,"impatient") -- allow to fail, if it is first run

--local notify_ok, notify = pcall(require,"notify")
--if notify_ok then
--  notify.setup{background_colour="#000000"} -- initial pre-setup to avoid warning
--  vim.notify = notify
--end

require"lib.globals"

_G.table = req"lib.table" or table -- luacheck: ignore
_G.T = table.create or function(t) return t end -- luacheck: ignore

local packer = req'lib.packer'

if packer then packer:load() end

-- Re-cache all the documentation from all the plugins
-- XXX: (slows down the startup. Maybe somehow rewrite with async/subprocesses?)
-- cmd[[helptags ALL]]

-- cmd[[start]] -- auto-enter Insert mode (actually, mostly useless)
