-- luacheck: no max line length
-- luacheck: globals req vim
-- luacheck: globals nnoremap map imap vmap noremap inoremap nmap xmap xnoremap tmap tnoremap

local plugins = _G.packer_plugins or {}

-- {{{ Global Maps

nnoremap('<space>', '<nop>')
-- nnoremap('<C-/>', '<space>')
-- map('<space>', '<leader>')
vim.g.mapleader = " "

imap('<c-z>', '<c-o>u')
imap('<c-y>', '<c-o><c-r>')
vmap('<c-z>', '<esc>uv')
vmap('<c-y>', '<esc><c-r>v')
map('<c-z>', 'u')
map('<c-y>', '<c-r>')

-- "map <Char-0x1b> z-
imap('<c-a>', '<esc><c-a>i') -- c-o?
imap('<c-x>', '<esc><c-x>i') -- c-o?

-- " C-c and C-v - Copy/Paste into "global clipboard"
vmap('<c-c>', '"+yi')
imap('<c-v>', '<c-o>"+gPa')

-- " make shift-insert behave like in Xterm
map('<s-insert>', '<MiddleMouse>')

-- " make <c-l> clear the highlights as well as redraw
noremap('<c-l>', '<cmd>nohls<cr><cmd>nohls<cr><cmd>redraw<cr>', {silent=true})

noremap('<c-left>', 'b')
--noremap('<c-right>', 'w')
noremap('<c-right>', 'e<Right>')
inoremap('<c-right>', '<c-o>e<Right>')

-- map('<F9>', '^i#<Esc>j') -- Comment line (poor man's `kommentary`)
nnoremap('<c-space>', '<c-w>')
nmap('<c-w>', 'i<c-w><esc>')
-- map('<c-u>', 'i<c-u><esc>')
map('<PageDown>', '<cmd>set scroll=0<CR><cmd>set scroll^=2<CR><cmd>set scroll-=1<CR><C-D><cmd>set scroll=0<CR>', {silent=true})
map('<PageUp>', '<cmd>set scroll=0<CR><cmd>set scroll^=2<CR><cmd>set scroll-=1<CR><C-U><cmd>set scroll=0<CR>', {silent=true})

function _G._copymode() -- {{{
  if vim.wo.signcolumn == vim.o.signcolumn then
    vim.wo.signcolumn = "no"
    vim.wo.list = not(vim.wo.list)
    vim.wo.number = not(vim.wo.number)
    req'indent_blankline.commands'.toggle()
    if vim.fn.mode() ~= "i" then
      vim.wo.relativenumber = not(vim.wo.relativenumber)
    end
  else
    vim.wo.signcolumn = vim.o.signcolumn
    vim.wo.list = vim.o.list
    vim.wo.number = vim.o.number
    if vim.fn.mode() ~= "i" then
      vim.wo.relativenumber = vim.o.relativenumber
    end
  end
end
-- }}}
nnoremap('<F11>', '<cmd>lua _copymode()<CR>',{ silent=true })
inoremap('<F11>', '<cmd>lua _copymode()<CR>',{ silent=true })

if os.getenv("MANOPT") then
  map("K","<cmd>Man<cr>",{ silent = true })
  map("<enter>","<cmd>Man<cr>",{ silent = true })
end

map('q', '<cmd>Sayonara<CR>', { silent = true })
-- map('q', '<cmd>q<CR>', { silent = true })
map('Q', '<cmd>q!<CR>', { silent = true })
map('w', '<cmd>w<CR>', { silent = true })
map('W', '<cmd>w!<CR>', { silent = true })
map('<m-q>', '<cmd>qa!<CR>', { silent = true })

nnoremap("<", "<<")
nnoremap(">", ">>")
xnoremap("<", "<gv")
xnoremap(">", ">gv")


-- }}}

-- {{{ EasyAlign
local easyalign = plugins['vim-easy-align']
if easyalign then
  -- vmap('<leader>e', ':EasyAlign<CR>')
  -- Start interactive EasyAlign in visual mode (e.g. vipga)
  xmap("ga", "<Plug>(EasyAlign)")
  -- Start interactive EasyAlign for a motion/text object (e.g. gaip)
  nmap("ga", "<Plug>(EasyAlign)")
end -- }}}

-- {{{ AnyJump
local anyjump = plugins['any-jump.vim']
if anyjump then
  nnoremap('<leader>j', '<cmd>AnyJump<CR>')
  xnoremap('<leader>j', '<cmd>AnyJumpVisual<CR>')
  nnoremap('<leader>ab', '<cmd>AnyJumpBack<CR>')
  nnoremap('<leader>al', '<cmd>AnyJumpLastResults<CR>')
end -- }}}

--[[ {{{ NvimTree
local nvimtree = plugins['nvim-tree.lua']
if nvimtree then
  map('<leader>tt', '<cmd>NvimTreeToggle<CR>')
end -- }}} ]]

-- {{{ NvimTree
local nvimtree = plugins['neo-tree.nvim']
if nvimtree then
  map('<leader>tt', '<cmd>Neotree toggle<CR>')
end -- }}}

-- {{{
local trouble = plugins['trouble.nvim']

if trouble then
  map('<leader>tr', '<cmd>TroubleToggle<CR>')
end
-- }}}



local focus = plugins['focus.nvim']
if focus then
  nmap("<C-Space><left>", "<cmd>FocusSplitLeft<CR>")
  nmap("<C-Space><right>", "<cmd>FocusSplitRight<CR>")
  nmap("<C-Space><down>", "<cmd>FocusSplitDown<CR>")
  nmap("<C-Space><up>", "<cmd>FocusSplitUp<CR>")
  nmap("<C-Space>s", "<cmd>FocusSplitNicely<CR>")
end

-- {{{ FTerm
local fterm = plugins['FTerm.nvim']
if fterm then
  nmap("<leader>lg","<cmd>LazygitToggle<cr>")
  nmap('<c-\\>', '<cmd>FTermToggle<CR>')
  tnoremap('<c-\\>', '<c-\\><c-n><cmd>FTermToggle<CR>')
  tnoremap('<c-n>', '<c-\\><c-n>')
end -- }}}

-- {{{ Telecscope
local telescope = plugins['telescope.nvim']
if telescope then
  nmap("<leader>tf","<cmd>lua req'telescope.builtin'.find_files{}<cr>")
  nmap("<leader>tg","<cmd>lua req'telescope.builtin'.live_grep{}<cr>")

  local silent = { silent = true, noremap = true }
  -- Navigate buffers and repos
  nmap('<leader>tb', [[<cmd>Telescope buffers show_all_buffers=true theme=get_dropdown<cr>]], silent)
  nmap('<leader>tc', [[<cmd>Telescope commands theme=get_dropdown<cr>]], silent)
  -- nmap('<c-e>', [[<cmd>Telescope frecency theme=get_dropdown<cr>]], silent)
  nmap('<leader>tee', [[<cmd>Telescope git_files theme=get_dropdown<cr>]], silent)
  nmap('<leader>tef', [[<cmd>Telescope find_files theme=get_dropdown<cr>]], silent)
  nmap('<leader>teg', [[<cmd>Telescope live_grep theme=get_dropdown<cr>]], silent)
end -- }}}

-- {{{ neogit
local neogit = plugins['neogit']
if neogit then
  nmap("<leader>ng","<cmd>Neogit<cr>")
end -- }}}


--  BarBar Mappings {{{
--if vim.fn.exists(':BufferNext') then
local barbar = plugins['barbar.nvim']
if barbar then
  -- Move to previous/next
  nnoremap('<F2>', '<cmd>BufferPrevious<CR>')
  nnoremap('<F3>', '<cmd>BufferNext<CR>')
  inoremap('<F2>', '<cmd>BufferPrevious<CR>')
  inoremap('<F3>', '<cmd>BufferNext<CR>')
  nnoremap('<A-,>', '<cmd>BufferPrevious<CR>')
  nnoremap('<A-.>', '<cmd>BufferNext<CR>')
  -- Re-order to previous/next
  nnoremap('<A-<>', '<cmd>BufferMovePrevious<CR>')
  nnoremap('<A->>', ' <cmd>BufferMoveNext<CR>')
  -- Goto buffer in position...
  nnoremap('<A-1>', '<cmd>BufferGoto 1<CR>')
  nnoremap('<A-2>', '<cmd>BufferGoto 2<CR>')
  nnoremap('<A-3>', '<cmd>BufferGoto 3<CR>')
  nnoremap('<A-4>', '<cmd>BufferGoto 4<CR>')
  nnoremap('<A-5>', '<cmd>BufferGoto 5<CR>')
  nnoremap('<A-6>', '<cmd>BufferGoto 6<CR>')
  nnoremap('<A-7>', '<cmd>BufferGoto 7<CR>')
  nnoremap('<A-8>', '<cmd>BufferGoto 8<CR>')
  nnoremap('<A-9>', '<cmd>BufferGoto 9<CR>')
  nnoremap('<A-0>', '<cmd>BufferLast<CR>')
  -- Close buffer
  nnoremap('<A-c>', '<cmd>BufferClose<CR>')

  -- Magic buffer-picking mode
  nnoremap('<C-p>', '<cmd>BufferPick<CR>')
  -- Sort automatically by...
  nnoremap('<leader>bb', '<cmd>BufferOrderByBufferNumber<CR>')
  nnoremap('<leader>bd', '<cmd>BufferOrderByDirectory<CR>')
  nnoremap('<leader>bl', '<cmd>BufferOrderByLanguage<CR>')

  -- Wipeout buffer
  --                 :BufferWipeout<CR>
  -- Close commands
  --                 :BufferCloseAllButCurrent<CR>
  --                 :BufferCloseBuffersLeft<CR>
  --                 :BufferCloseBuffersRight<CR>
  -- Other:
  -- :BarbarEnable - enables barbar (enabled by default)
  -- :BarbarDisable - very bad command, should never be usedlocal map = vim.api.nvim_set_keymap
end
-- }}}

-- {{{ Specs (cursor highlighter)
local specs = plugins['specs.nvim']
if specs then
-- map ... <cmd>lua require"cfg.specs".toggle()
  nnoremap('<c-b>', '<cmd>lua s = req"cfg.specs"; s.show_specs(); s.hl_blink()<cr>')
  nnoremap('n', 'n<cmd>lua req"cfg.specs".show_specs()<cr>')
  nnoremap('N', 'N<cmd>lua req"cfg.specs".show_specs()<cr>')
end
-- }}}

-- {{{ LSP Lines
local lspl = plugins['lsp_lines.nvim']
if lspl then
  map(
    "<leader>ll",
    req"cfg.lsp.lines".toggle,
    { desc = "Toggle lsp_lines" }
  )
end
-- }}}


local ufo_l = plugins['nvim-ufo']
if ufo_l then
  local ufo = req"ufo"
  nmap('zR', ufo.openAllFolds)
  nmap('zM', ufo.closeAllFolds)
end


--[=[ -- {{{

" TABS

"nnoremap <silent> <F1> :ba<CR>
""nnoremap <silent> <F2> <cmd>bp<CR>
""nnoremap <silent> <F3> <cmd>bn<CR>

"nnoremap <silent> <F1> :tab new<CR>
"nnoremap <silent> <F2> gT
"nnoremap <silent> <F3> gt

"set switchbuf=uselast,newtab
"nnoremap <silent> <F1> :tabnew<CR>
"inoremap <silent> <F1> <esc>:tabnew<CR>i
"nnoremap <silent> <F2> :tabprevious<CR>
"inoremap <silent> <F2> <esc>:tabprevious<CR>i
"nnoremap <silent> <F3> :tabnext<CR>
"inoremap <silent> <F3> <esc>:tabnext<CR>i


"nnoremap <silent> <F5> :so $MYVIMRC<CR>
"vnoremap <silent> <F5> <esc>:so $MYVIMRC<CR>v
"inoremap <silent> <F5> <esc>:so $MYVIMRC<CR>i

"nnoremap <silent> <F9> :w<CR>:make<CR>
"inoremap <silent> <F9> <esc>:w<CR>:make<CR>i


" Will move to vim-cmdline in files and local per-project .vimrcs
"nnoremap <silent> <F11> :call <SID>_colorcol()<CR>
"inoremap <silent> <F11> <esc>:call <SID>_colorcol()<CR>i
" fun! s:_colorcol()
"   if &colorcolumn > 0
"     set colorcolumn=0
"   else
"     exe "set colorcolumn=".g:colorcol
"   endif
" endfun

"nnoremap <silent> <F12> :!ctags -R<CR>

" F11 toggles line numbering
"inoremap <silent> <F12> <esc>:set rnu! <bar> set rnu?<CR>i
"nnoremap <silent> <F12> :set rnu! <bar> set rnu?<CR>


"cabbrev W w
"cabbrev Ц W
"cabbrev ц w
"cabbrev Q q
"cabbrev Й Q
"cabbrev й q

"cabbrev man tab help

--]=] --}}}
