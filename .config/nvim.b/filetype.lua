-- luacheck: globals vim
--[=[
vim.filetype.add({
    extension = {
        ebuild = "ebuild",
    },
    filename = {
        ["Foofile"] = "fooscript",
    },
    pattern = {
        ["~/%.config/foo/.*"] = "fooscript",
    }
})
--]=]
