## TODO:

# NVim:

1) Indent-Blankline draws line over first character entered in noice's cmdline when LSP status messages active
(btw, interesting fact that adding **FILE**type `noice` to Indent-Blankline exclusion helps with removing lines from those LSP Status messages. Not sure, why.)
Anyway, it looks like the way to fix it — is to find (and/or set) filetype/buftype of noice's cmdline, and add it to exclusion.

SOLUTION: currently disaled Indent-Blankline, using 'mini.indentscope'


2) `q` binded to Sayonara quits entire nvim, irrelevant if there are other buffers opened when it is any noice/notify message active. Whether it is a message, or lspstatus 🤷
Solution is, probably, to get rid of sayonara and make a function that will use `bwipeout` until there are no more, and then call `quit`.
And bind it to `q`.

SOLUTION: kicked off Sayonara, using current function with bwipeout.
It has some issues sometimes, but it would easier to fix them, than having a sex with Sayonara.


3) Sometimes somewhy nvim throws an error on quit:
```
nvim: src/unix/core.c:116: uv_close: Assertion `!uv__is_closing(handle)' failed.
```
And exits with 134.
Among other things, it brakes git commit behaviour.


Seems like it was fixed by commenting out `opt.spelling = { "en", "ru" }` in options. But needs more tests.


(Sometimes happens anyway, but much rarely)


4) Something in lazylazy/options.lua conflicts with modifiable=no in Lazy dialog, so loading of the options brakes.

(seems like kinda fixed, TODO: test)


5) think about disabling `lsp_lines` for lazy popup

SOLUTION: https://github.com/folke/lazy.nvim/issues/796
TEMP. SOLUTION: lazylazy/autocommands => cfg.lsp.ui.reload


6) noice.lua: if nvim was started with empty buffer (no files), or with non-existing file as argument, it behaves strange: it DO loads
all respective plugins, but don't apply colorscheme (luakai) to cmdline popups,
nvim-cmp-lua: and don't use nvim_lua completion on commandline 

// fixed in luakai theme
9) sometimes (can't figure out conditions) `normal '"` restores not a real "last line where cursor was before closing",
but some kind of pre-saved.
TL;DR: nvim stopping to save the line where cursor was before closing, and always return constant value 
(kinda fixed by mkview+loadview)



===== Not fixed yet =====


7) treesitter-cmp: problems with surroundings:
a) when it suggests completions with quotes in it, cmp inserts that complerion ignoring already-existing closing quote on the end of completed word.

i.e.

```"smth|" # (| == cursor)```
with selected completion ```"something completed"```
transforms
```"something completed""```

b) same for left side surroundings sometimes:
```-- lua<tab>```
with selected completion ```"-- luacheck: globals ....."```
transforms
```-- -- luacheck: globals .....```

8) Folding:
a) (fold-cycle.nvim) cycles folds located deeply inside even when cursor on top-level (maybe related to foldlevel=99 for ufo), or maybe just as planned
b) (neovim) navigating inside closed fold in Insert mode doesn't open it (works fine in Normal)
c) fold column/heirline fold column: see "b" in treesitter

10) treesitter:
a) (lua) folds includes closing block (while indent-powered folds - doesn't)
b) (lua) no fold markers on fold column (and maybe not even folds themself) on similar following blocks after foldable, if code doesn't separated by empty line (at least with lua) 

11) 
………

# ZSH:

1) Suggestions (shadowed) from history (TODO: search for name of a plugin, which provide that thing)

………

