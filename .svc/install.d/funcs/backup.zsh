backup() {
	dbg ext "[BACKUP] [INIT] ARGS: '${@}'"

	local p=${1}
	if [[ -n ${DO_NOT_TOUCH} ]] {
		for n ("${(@)DO_NOT_TOUCH}") {
			if [[ "${n}" == "${p}" ]] {
				wrn "[BACKUP] Skipping '${p}' because it listed in DO_NOT_TOUCH"
				return 1
			}
		}
	}
	dbg ext "[BACKUP] Processing: ${p}"
	local f="${H}/${p}" zf="${Z}/${p}";
	local b="${H}/.config/backups/${TS}/${p}";
	local fp="$(linkpath ${f})" zfp="$(linkpath ${zf})";

	if [[ -e "${f}" ]] {
		dbg ext "[BACKUP] ${f} exists;"
		if [[ -h "${f}" ]] {
			dbg ext "[BACKUP] ${f} is symlink"
			if [[ "${fp}" == "${zfp}" ]] {
				dbg inf "[BACKUP] [OK] ${f} is okay (linked to ${zfp}). Skipping it."
				return 1
			}
			dbg ext "[BACKUP] ${f} is not ours ('${fp}' vs '${zfp}') symlink"
		}
		[[ -e "${zfp}" ]] || Restore+=( "${p}" )
		${noop} "${(z)MKD}" "$(dirname ${b})/" &&
		${noop} "${(z)MV}" "${f}" "${b}"
		if [[ -e "${f}" ]] {
			err "[BACKUP] [!!!] Failed to move ${p} to backup. Probably conflict. Try to restart as 'DBG=1 $0 | grep ${p}'"
			dbg ext "[BACKUP] f: $(ls -ld ${f})"
			dbg ext "[BACKUP] b: $(ls -ld ${b})"
		} else {
			wrn "[BACKUP] [MOVE] ${p} moved to backup ('${b}')"
		}
	} else {
		if [[ -h "${f}" ]] {
			${noop} "${(z)MKD}" "$(dirname ${b})/" &&
			${noop} "${(z)MV}" "${f}" "${b}"
			dbg inf "[BACKUP] [MOVE] File '${p}' is broken symlinking. Moving to backup."
		} else {
			dbg inf "[BACKUP] [OK] File '${p}' is missing in the home dir. Continuing."
		}
	}
}

